# Disclaimer
**This is not a product! Is's a prototype.** You should not assume that this prototype will work as expected in your particular setting. Carefully check your results if you use this for an ETL-job with real data. Use at own risk ;-) 

# Setting up the project for development
1. Clone repository
2. Disable tracking of changes to Log.txt ``add git update-index --assume-unchanged ETLBuilder_Forms/ETLBuilder_Forms/Log/Log.txt`` (this step is not necessary to run the project, but you should not commit your logs to the repository)
1. IDE used: *Visual Studio Community 2017* (free for students, open-source and single developers)
2. open ``..\HAMSTRETLBuilder\ETLBuilder_Forms\ETLBuilder_Forms.sln``
3. Select *x64* or *x86* instead of *Any CPU*
3. Projektmappe neu erstellen (*Erstellen*, *Projektmappe neu erstellen*)
5. Now you shuold be able to run the project

All other configurations, references and packages should be loaded automatically.
- Zielframework:.NET Framework 4.5.2
- Assembly name: HaMSTR_ETLBuilder_Forms
- Standardnamespace: HaMSTR_ETLBuilder_Forms
- Ausgabetyp: Windows-Anwendung

## Quick orientation
- *program.cs* is the main entry point
- *ETLBuilder_Initial.cs* and *classes.cs* contain most of the functionalities implemented in c#
- Mapping functionalities in folder *HTML*

# Using the tool
1. Run *HaMSTR_ETLBuilder_Forms.exe* in ``..\ETLBuilder_Forms\ETLBuilder_Forms\bin\ ``
2. Specify datasource
	1. Select connection type (checkboxes)
	2. Specify *connection string* to database or flat file.  (connection string for flat file = Path to file)
	3. Open connection
3. Enter and execute SQL query to retrieve data to load from datasource
	1. This query has to retrieve all the data in one result table.
	2. Note: You can specify a SQL query on an Excel file looking like this `` SELECT * FROM [sheet1$] s1 LEFT JOIN [sheet2$] s2 ON s1.[column_holding_id] = s2.[column_holding_id]``
3. Use data preview to check the query results
4. Switch to tab *Instructions & Contributions*
5. Enter information for openEHR repository
	1. Base address of your openEHR repositories REST-API, e.g. ``http://1.1.1.1:8081/rest/v1/``
		(Note: For ehrbase REST-API set e.g. ``https://1.1.1.1:8081/ehrbase/rest/v1/``)
	2. TemplateId
	3. Username and password of openEHR repository user to commit the data
	4. Namespace for external subject id (for each subjectId/namespace pair an EHR will be created in the openEHR repository if it doesn't already exist)
6. Now you can switch to tab *Mapping Options* to create a mapping from your relational dataset to an openEHR contribution or load an existing mapping from file. If you create a new mapping, the process works as follows:
	1.	Imagine to iterate through the relational dataset from top left to buttom right. For each column specify what action (e.g. creation of new contribution, creation of an entry) shall be invoked when a condition (e.g. value for column changed from previous row) is met .
	2. Usually this is an iterative process where you switch between tab *Mapping Options* to modify your mapping and tab *Instructions & Contributions* to check the preview of how a final contribution will look like. 
7. Once the mapping is done in tab *Instructions & Contributions* the button *Transmit Contributions* starts the ETL-process. Testmode will save contributions in folder ``..\Log\ `` instead of sending them to the openEHR repository.
8. The application will prompt success and logged events when done. Logged errors are displayed in tab *DB Connection*

#region Namespaces
using System;
using System.Data;
using Microsoft.SqlServer.Dts.Runtime;
using System.Windows.Forms;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Linq;
using System.IO;
using System.Web.Script.Serialization;
using System.Dynamic;
using System.Collections.Concurrent;
using System.Threading;
using System.Text.RegularExpressions;
#endregion

namespace ST_7cee71c791424fc1b245bcb7296f97e5
{
    /// <summary>
    /// ScriptMain is the entry point class of the script.  Do not change the name, attributes,
    /// or parent of this class.
    /// </summary>
    [Microsoft.SqlServer.Dts.Tasks.ScriptTask.SSISScriptTaskEntryPointAttribute]
    public partial class ScriptMain : Microsoft.SqlServer.Dts.Tasks.ScriptTask.VSTARTScriptObjectModelBase
    {
        #region constants and variables
        const string versionTag = "2017-05-09-001"; //version of this component
        const string suppressedBadEntriesTag = "suppressedBadEntries"; //Tag to add to feeder_audit version_id to indicate that bad entries where removed
        const string suppressedBadEntriesPathTag = "report/_feeder_audit/originating_system_audit"; //path where to add this info
        const double worthCleansingLevel = 0.1;//level where a composition is deemed to be worth to get cleaned from bad entries and resubmitted

        const string sqlConnectionName = "CCDB_DWH_ADO"; //ADO-connection to DB for error table (SQL)
        const string thinkHTTPConnectionName = "ThinkRestHTTP"; //Http-connection to Think!EHR
        const int maxOpenRequests = 40; //maximum number of open requests - also defines DefaultConnectionLimit 

        const string recordSetToPostTag = "RecordSetToPost"; //input recordset to generate compositions from

        const string testmodeTag = "testmode"; //variable indicating if current run is a test, what kind etc.
        const string conditionsAndActionsTag = "conditionsAndActions"; //JSON-object describing how to react to the fields and its values from recordSetToPost
        const string errorTableTag = "errorTable"; //Table in which rejected compositions are stored for error investigation
        const string templateIdTag = "templateId"; //name of the template which is target for this process
        const string namespaceTag = "namespace"; //namespace in think!EHR platform to which data shall be committed
        
        const string componentNameUsedForLogging = "ThinkRestContributions";

        const string logToPath = "C:\\SSIS_Solutions\\MLIFE\\Beatmung_mlife_think\\Log\\"; 
        const int debug = 3; // 0 = log nothing from debug mode, 10 = log everything you can imagine 

        bool fireAgain = true;
        #endregion

        #region classes for handling instructions
        public class InstructionsHandler
        {
            bool fireAgain = true;
            public Microsoft.SqlServer.Dts.Tasks.ScriptTask.ScriptObjectModel Dts { get; set; }
            public IContributionsHandler contributionsHandler { get; set; }

            public ColumnInstructions[] columnInstructions { get; set; }
            public IDictionary<string, int> columnInstructionsKeyToIndex { get; set; }
            private Dictionary<string, string> lastValues { get; set; }

            public InstructionsHandler(string instructionsString, Microsoft.SqlServer.Dts.Tasks.ScriptTask.ScriptObjectModel Dts, IContributionsHandler contributionsHandler)
            {
                this.Dts = Dts;
                this.contributionsHandler = contributionsHandler;

                this.lastValues = new Dictionary<string, string>();
                /* deserialze instructions (given as JSON-String) on what to do for which columnName and values */
                this.columnInstructionsKeyToIndex = new Dictionary<string, int>();
                try
                {
                    if (instructionsString == "")
                    {
                        instructionsString = "[]";
                    }
                    this.columnInstructions = new JavaScriptSerializer().Deserialize<ColumnInstructions[]>(instructionsString);
                    //fill lookup dictionary for indexes of columns in columnInstructions - so later one can lookup instructions by columnName without iterating
                    for (int i = 0, le = this.columnInstructions.Length; i < le; i++)
                    {
                        this.columnInstructionsKeyToIndex[this.columnInstructions[i].colName] = i;
                    }
                }
                catch (Exception ex)
                {
                    Dts.Events.FireError(0, "InstructionsHandler" + "INSTRUCTIONS", "Processing of given instructions failed: " + ex.Message, string.Empty, 0);
                    Dts.TaskResult = (int)ScriptResults.Failure;
                }
            }

            public void runOn(DataRow row, DataColumn col)
            {
                if ((row == null) && (col == null)) //just to finish last contribution
                {
                    this.contributionsHandler.newContribution(null, null, null, null, null, null);
                    return;
                }

                if (this.columnInstructionsKeyToIndex.ContainsKey(col.ColumnName)) //if collumn has no instruction it will simply be ignored
                {
                    //iterate through all conditions for current collumn
                    foreach (CAndA cAndA in this.columnInstructions[this.columnInstructionsKeyToIndex[col.ColumnName]].cAndAs)
                    {
                        this.runActions(col.ColumnName, row, cAndA);//outsourced that just to fit into my mind
                    }
                }
                this.lastValues[col.ColumnName] = row[col.ColumnName].ToString();
            }

            /* runs actions depending on the current input value and the given instructions */
            private void runActions(string columnName, DataRow row, CAndA cAndA)
            {
                Action[] actionsToTake = null;
                //conditions can be concatenated by || because this is handy sometimes
                string[] separator = new string[] { "||" };
                string[] tmpConditions = cAndA.condition.Split(separator, StringSplitOptions.None);
                
                foreach (string s in tmpConditions)
                {
                    if (s == "changed")
                    {
                        if ((!this.lastValues.ContainsKey(columnName)) || (this.lastValues[columnName] != row[columnName].ToString()))
                        {
                            actionsToTake = cAndA.actions;
                            break;
                        }
                    }
                    else if (s == "true")
                    {

                        actionsToTake = cAndA.actions;
                        break;
                    }
                    else if (s.Contains("="))
                    {
                        string[] components = s.Split('=');
                        if (row[components[0]].ToString() == components[1])
                        {
                            actionsToTake = cAndA.actions;
                            break;
                        }
                    }
                    else
                    {
                        Dts.Events.FireError(0, "InstructionsHandler" + "INSTRUCTIONS", "Invalid condition: " + cAndA.condition, string.Empty, 0);
                        Dts.TaskResult = (int)ScriptResults.Failure;
                        actionsToTake = null;
                    }
                }

                //run the actions
                if (actionsToTake != null)
                {
                    foreach (Action a in actionsToTake)
                    {
                        if (a.method == "newContribution")
                        {
                            this.lastValues = new Dictionary<string, string>();
                            string subjectId = row[a.parameters[2]].ToString();
                            this.contributionsHandler.newContribution(a.parameters[0], subjectId, a.parameters[1], columnName, a.parameters[3], a.parameters[4]);
                        }
                        else if (a.method == "newSubtree")
                        {
                            this.contributionsHandler.newSubtree(a.parameters[0]);
                        }
                        else if (a.method == "newEntry")
                        {
                            string castInstruction = "noCast";
                            if (a.parameters.Length > 1)
                            {
                                castInstruction = a.parameters[1];
                            }
                            string sourceColname = columnName;
                            if ((a.parameters.Length > 2) && (a.parameters[2] != ""))
                            {
                                sourceColname = a.parameters[2];
                            }
                            this.contributionsHandler.newEntry(a.parameters[0], row[sourceColname].ToString(), castInstruction);
                        }
                    }
                }
            }
        }

        public class ColumnInstructions
        {
            public string colName { get; set; }
            public CAndA[] cAndAs { get; set; }
        }

        public class CAndA //conditions And Actions
        {
            public string condition { get; set; }
            public Action[] actions { get; set; }
        }

        public class Action
        {
            public string method { get; set; }
            public string[] parameters { get; set; }
        }
        #endregion

        #region class for summary statistics
        public class SummaryStatistics
        {
            public bool fireAgain = true;
            public DateTime starttime { get; set; }

            public ThinkHandle thinkHandle { get; set; }
            public ServerResponseHandling serverResponseHandle { get; set; }
            public ContributionsHandler contributionsHandler { get; set; }

            public int rowCounter { get; set; }

            public SummaryStatistics(ThinkHandle thinkHandle, ServerResponseHandling serverResponseHandle)
            {
                this.starttime = DateTime.UtcNow;
                this.rowCounter = 0;
                this.thinkHandle = thinkHandle;
                this.serverResponseHandle = serverResponseHandle;
            }

            public void log(string logMessage)
            {
                if (debug > 0)
                {
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + logMessage, String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Time passed: " + DateTime.UtcNow.Subtract(starttime).TotalSeconds + " seconds", String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Database records(rows) processed: " + this.rowCounter, String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Posted requests: " + this.thinkHandle.requestCounter, String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Contributions: " + this.contributionsHandler.contributionsCounter, String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Entries: " + this.contributionsHandler.entryCounter, String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Invalid contributions: " + this.serverResponseHandle.storedInvalidCounter, String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Modified contributions (reposted): " + this.serverResponseHandle.modifiedCounter, String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Valid contributions (confirmed from Think!platform): " + this.serverResponseHandle.validCounter, String.Empty, 0, ref fireAgain);
                    thinkHandle.Dts.Events.FireInformation(0, "SummaryStatistics", "Contribution check: Valid + Invalid = " + (this.serverResponseHandle.validCounter + this.serverResponseHandle.storedInvalidCounter), String.Empty, 0, ref fireAgain);
                }
            }
        }
        #endregion

        #region interface and classes for building contributions

        public interface IContributionsHandler
        {
            /*  Starts a new composition */
            void newContribution(string action, string subjectId, string templateId, string colName, string language, string territory);
            /* updates index-counts for subtree specified by given path - clear deprecated entries and update index and lastValue for current*/
            void newSubtree(string path);
            /*  Adds an entry for one attribute of composition in FLAT Format and appends it as new line to StringBuilder sb */
            void newEntry(string path, string valueToAdd, string castInstruction);
        }

        public class ContributionsHandler : IContributionsHandler
        {
            public bool fireAgain = true;
            public ThinkHandle thinkHandle { get; set; }
            public int contributionsCounter { get; set; }
            public int entryCounter { get; set; }
            private Contribution contributionUnderConstruction { get; set; }
            public IDictionary<string, string> subjectIdsToEhrIds { get; set; }
            public ConcurrentDictionary<Contribution, bool> waitingForCommit { get; set; }
            public bool checkWaitingContributionsRunning { get; set; }
            public IDictionary<string, string> atCodes { get; set; }
            public IDictionary<string, string> stringsToReplace { get; set; }

            public ContributionsHandler(ThinkHandle thinkHandle)
            {
                this.contributionsCounter = 0;
                this.entryCounter = 0;
                this.contributionUnderConstruction = null;
                this.thinkHandle = thinkHandle;
                this.subjectIdsToEhrIds = new Dictionary<string, string>();
                this.waitingForCommit = new ConcurrentDictionary<Contribution, bool>();
                this.checkWaitingContributionsRunning = false;
            }

            void IContributionsHandler.newContribution(string action, string subjectId, string templateId, string colName, string language, string territory)
            {
                //if necessary, commit last contribution
                if (this.contributionUnderConstruction != null)
                {
                    this.waitingForCommit[this.contributionUnderConstruction] = true;
                    //this.checkWaitingContributions();
                }
                //if this method was just invoked to finish last contribution
                if (subjectId == null)
                {
                    this.contributionUnderConstruction = null;
                    return;
                }
                //if not, trigger ehrId detection
                else
                {
                    if (!subjectIdsToEhrIds.ContainsKey(subjectId))
                    {
                        if ((thinkHandle.Dts.Variables.Contains(testmodeTag) == false) || (thinkHandle.Dts.Variables[testmodeTag].Value.ToString() == "no"))
                        {
                             subjectIdsToEhrIds[subjectId] = "requestEHRId";
                        }
                        else //das hier ist nur Testmode Kram
                        {
                            subjectIdsToEhrIds[subjectId] = subjectId;
                            this.checkWaitingContributions();
                        }
                    }
                }
                //create contribution
                this.contributionsCounter++;
                this.contributionUnderConstruction = new Contribution(action, subjectId, templateId, language, territory);
            }

            void IContributionsHandler.newSubtree(string path)
            {
                this.contributionUnderConstruction.newSubtree(path);
            }

            void IContributionsHandler.newEntry(string path, string valueToAdd, string castInstruction)
            {
                object finalValue = valueToAdd;
                    
                //treat value if necessary
                String[] castInstructions = castInstruction.Split(new string[] { "," }, StringSplitOptions.None);
                for (int i = 0; i < castInstructions.Length; i++)
                {
                    if (castInstructions[i] == "replaceDotWithComma")
                    {
                        finalValue = finalValue.ToString().Replace('.', ',');
                    }
                    if (castInstructions[i] == "replaceCommaWithDot")
                    {
                        finalValue = finalValue.ToString().Replace(',', '.');
                    }
                    if (castInstructions[i] == "constantValue")
                    { //constantValue,Test
                        finalValue = castInstructions[i + 1];
                    }
                    if (castInstructions[i] == "split")
                    { //split,:,0
                        String[] tmpComponents = finalValue.ToString().Split(new string[] { castInstructions[i + 1] }, StringSplitOptions.None);
                        int index = Int32.Parse(castInstructions[i + 2]);
                        if (tmpComponents.Length > index)
                        {
                            finalValue = tmpComponents[index];
                        }
                        else
                        {
                            thinkHandle.Dts.Events.FireWarning(0, "Split string", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Given index out of range: " + finalValue + ", " + castInstruction, String.Empty, 0);
                        }
                    }
                    if (castInstructions[i] == "asInt")
                    {
                        finalValue = Int32.Parse(finalValue.ToString());
                    }
                    if (castInstructions[i] == "asDouble")
                    {
                        finalValue = Convert.ToDouble(finalValue.ToString());
                    }
                    if (Array.IndexOf(castInstructions, "asISO8601") > -1)
                    {
                        finalValue = Convert.ToDateTime(valueToAdd).ToString("s", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    if (castInstructions[i] == "noEmptyEntry")
                    {
                        if (finalValue.ToString() == "")
                        {
                            return;
                        }
                    }
                    if (castInstructions[i] == "castUsingDictionary")
                    {
                        if (this.stringsToReplace.ContainsKey(finalValue.ToString()))
                        {
                            finalValue = this.stringsToReplace[finalValue.ToString()];
                        }
                        else
                        {
                            thinkHandle.Dts.Events.FireWarning(0, "Replace string using dictionary", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " No dictionary entry given for value: " + valueToAdd, String.Empty, 0);
                        }
                    }
                    if (castInstructions[i] == "castIntoAtCode")
                    {
                        if (this.atCodes.ContainsKey(finalValue.ToString()))
                        {
                            finalValue = this.atCodes[finalValue.ToString()];
                        }
                        else
                        {
                            thinkHandle.Dts.Events.FireWarning(0, "Cast At Codes", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " No atCode given for value: " + valueToAdd, String.Empty, 0);
                        }
                    }
                }
                this.entryCounter++;
                this.contributionUnderConstruction.newEntry(path, finalValue);
            }

            public void assignEhrId(string subjectId, string ehrId) {
                if ((this.subjectIdsToEhrIds.ContainsKey(subjectId)) && ((this.subjectIdsToEhrIds[subjectId] == "requested") || (this.subjectIdsToEhrIds[subjectId] == "requestEHRId")))
                {
					this.subjectIdsToEhrIds[subjectId] = ehrId;
				}
				else {
                    thinkHandle.Dts.Events.FireWarning(0, "ServerResponseHandling", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Unexpected dictionary-state for subjectId: " + subjectId, String.Empty, 0);
				}
			}

            public void checkWaitingContributions()
            {
                //sicherstellen das checkWaitingContributions nur einmal l�uft
                if (this.checkWaitingContributionsRunning)
                {
                    thinkHandle.Dts.Events.FireWarning(0, "checkWaitingContributions", "i am not supposed to be running in more than one thread!", string.Empty, 0);
                    return;
                }
                else
                {
                    this.checkWaitingContributionsRunning = true;
                }
                
                List<Contribution> toRemove = new List<Contribution>();
                foreach (KeyValuePair<Contribution, bool> con in this.waitingForCommit)
                {
                    //um die Anzahl an requests im Zaum zu halten
                    if (thinkHandle.openRequests.Count > maxOpenRequests)
                    {
                        break;
                    }
                    else if (subjectIdsToEhrIds[con.Key.subjectId] == "requestEHRId") //ehrId has to be set
                    {
                        subjectIdsToEhrIds[con.Key.subjectId] = "requested";
                        thinkHandle.httpRequest("ehr?subjectId=" + con.Key.subjectId + "&subjectNamespace=" + thinkHandle.Dts.Variables[namespaceTag].Value.ToString(), true, "", "application/json");
                    }
                    else if (subjectIdsToEhrIds[con.Key.subjectId] != "requested") //ehrId is already set. Thus, send composition
                    {
                        con.Key.ehrId = subjectIdsToEhrIds[con.Key.subjectId];
                        //in testmode just log the JSON-object, in normal mode post it to think!API
                        if ((thinkHandle.Dts.Variables.Contains(testmodeTag) == false) || (thinkHandle.Dts.Variables[testmodeTag].Value.ToString() == "no"))
                        {
                            //TODO falls man den Bedarf sieht, kann man evtl. noch was optimieren, 
                            //z.B. wenn man hier mehrere Contributions zu einem Commit zusammenfasst
                            thinkHandle.httpRequest("composition/contribution/", true, "[" + con.Key.asJSON() + "]", "application/json");
                            if (debug > 4) { System.IO.File.AppendAllText(logToPath + con.Key.ehrId + "_" + con.Key.asJSON().GetHashCode() + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", ""); }
                            string tmpStr = "[" + con.Key.asJSON() + "]";
                            if (debug > 3) { System.IO.File.AppendAllText(logToPath + "CompositionsData" + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss.fff tt") + tmpStr.GetHashCode() + "," + con.Key.asJSON().Length + ", " + con.Key.asJSON().Count(x => x == ',') + Environment.NewLine); }
                        }
                        else if (thinkHandle.Dts.Variables[testmodeTag].Value.ToString() == "logToFile")
                        {
                            System.IO.File.AppendAllText(logToPath + con.Key.ehrId + "_" + con.Key.asJSON().GetHashCode() + ".txt", "[" + con.Key.asJSON() + "]" + Environment.NewLine);    
                        }
                        else
                        {
                            thinkHandle.Dts.Events.FireInformation(0, "ContributionsHandler - commitContributions: ", "[" + con.Key.asJSON() + "]", String.Empty, 0, ref fireAgain);
                        }
                        toRemove.Add(con.Key); //remember which contributions to remove from list
                    }
                }
                //remove contributions from waiting list
                foreach (Contribution con in toRemove)
                {
                    bool useless; this.waitingForCommit.TryRemove(con, out useless);
                    if (!useless) thinkHandle.Dts.Events.FireWarning(0, "ContributionsHandler", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "Removing contribution (type 3) failed", string.Empty, 0);
                }

                this.checkWaitingContributionsRunning = false;
            }

            public void setAtCodes(ColumnInstructions atCodesAsInstructions)
            {
                this.atCodes = new Dictionary<string, string>();
                if (atCodesAsInstructions.cAndAs.Length>0) {
                    foreach (Action a in atCodesAsInstructions.cAndAs[0].actions)
                    {
                        this.atCodes[a.parameters[0]] = a.method;
                        
                    }
                }
            }

            public void setStringsToReplace(ColumnInstructions stringsToReplaceAsInstructions)
            {
                this.stringsToReplace = new Dictionary<string, string>();
                if (stringsToReplaceAsInstructions.cAndAs.Length > 0)
                {
                    foreach (Action a in stringsToReplaceAsInstructions.cAndAs[0].actions)
                    {
                        this.stringsToReplace[a.method] = a.parameters[0];

                        if (debug > 5) System.IO.File.AppendAllText(logToPath + "Log.txt", "Building dictionary: " + a.method + " = " + a.parameters[0] + Environment.NewLine);
                    }
                }
            }
        }

        public class Contribution
        {
            [ScriptIgnore]
            private IDictionary<string, int> cardinalityCounts { get; set; } //to support cardinality in templates, count how many instances of a path already exist
            [ScriptIgnore]
            public string subjectId { get; set; }

            public string action { get; set; }
            public string ehrId { get; set; }
            public string format { get; set; }
            public string templateId { get; set; }
            public IDictionary<string, object> composition { get; set; }

            public Contribution(string action, string subjectId, string templateId, string language, string territory)
            {
                this.action = action;
                this.ehrId = null;
                this.format = "FLAT";
                this.templateId = templateId;
                this.composition = new Dictionary<string, object>();
                this.newEntry("ctx/language", language);
                this.newEntry("ctx/territory", territory);
                this.subjectId = subjectId;
                this.cardinalityCounts = new Dictionary<string, int>(); //reset cardinality counters
            }

            public string asJSON()
            {                                                                        
                JavaScriptSerializer s = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
                return s.Serialize(this);
            }

            /* updates index-counts for subtree specified by given path - clear deprecated entries and update index and lastValue for current*/
            public void newSubtree(String path)
            {
                this.deleteAllIndexesContainingPath(path); //delete all index-entries descendants of given path, because it's a new subtree
                this.cardinalityCounts[path] = this.getIndex(path) + 1; //make entry for new index of this subtree
            }

            public void newEntry(string path, object valueToAdd)
            {
                const string counterMarkup = "<<index>>";
                //replace <<index>> tags with real counters for that path
                String pathEntry = "";
                String[] subpaths = path.Split(new string[] { counterMarkup }, StringSplitOptions.None); //split by <<index>>
                for (int i = 0, le = subpaths.Count(); i < le - 1; i++) //for each subpath 
                {
                    string subpath = subpaths[0];
                    for (int j = 1; j <= i; j++)
                    {
                        subpath += counterMarkup + subpaths[j]; //build subpath for index retrieval
                    }
                    pathEntry += subpaths[i] + this.getIndex(subpath); //and append new part of subpath and its index to the entry string
                }
                pathEntry += subpaths[subpaths.Count() - 1]; //append last bit of path (or whole path if there where no <<index>>-markers) 
                this.composition[pathEntry] = valueToAdd;
            }

            #region helping methods
            /*  returns index(int) for given path - returns index -1 if no index entry in cardinalityCounts exists*/
            private int getIndex(String path)
            {
                if (!this.cardinalityCounts.ContainsKey(path))
                {
                    return -1;
                }
                else
                {
                    return this.cardinalityCounts[path];
                }
            }

            /*  deletes all indexes containing the given path */
            private void deleteAllIndexesContainingPath(String path)
            {
                List<string> removals = new List<string>();
                foreach (KeyValuePair<string, int> kv in this.cardinalityCounts)
                {
                    //if (kv.Key.Contains(path))
                    if ((kv.Key.Contains(path)) && (kv.Key != path))
                    {
                        removals.Add(kv.Key);
                    }
                }
                foreach (string key in removals)
                {
                    this.cardinalityCounts.Remove(key);
                }
            }
            #endregion
        }
        #endregion

        #region class for assembling ETL-Builder-Tool input
        public class EtlBuilderInfo
        {
            public ColumnInstructions[] columnInstructions { get; set; }
            public List<Dictionary<string, object>> exampleData { get; set; }
            public object templatePathes { get; set; }
            public object templateOPT { get; set; }

            /* Pfade und OPT f�r �bergebene templateId von Think-Plattform abfragen und setzen. */
            public void getPathsAndOPT(string templateId, ThinkHandle thinkHandle)
            {
                try
                {
                    //deserializing because strings would get an extra escape on serialization later 
                    this.templateOPT = new JavaScriptSerializer().DeserializeObject(thinkHandle.httpRequest("template/" + templateId + "/", false, null, null));
                    this.templatePathes = new JavaScriptSerializer().DeserializeObject(thinkHandle.httpRequest("template/" + templateId + "/example", false, null, null));
                }
                catch (Exception ex)
                {
                    thinkHandle.Dts.Events.FireError(0, "EtlBuilderInfo", " failed while trying to get the templates paths and OPT: " + ex.Message, string.Empty, 0);
                    thinkHandle.Dts.TaskResult = (int)ScriptResults.Failure;
                }
            }

            public void setExampleDataFromDataTable(DataTable dt)
            {
                exampleData = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                int rowCounter = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    exampleData.Add(row);
                    rowCounter++;
                    if (rowCounter > 99)
                    {
                        break;
                    }
                }
            }

            /* Instanz als JSON serialisiert zur�ckgeben */
            public string asJSON()
            {
                JavaScriptSerializer s = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
                return s.Serialize(this);
            }
        }
        #endregion

        #region interface and class implementation for handling of server-responses
        public interface IServerResponseHandling
        {
            /* handling of positive server responses for async-get-requests */
            void handleGetResponse(string resultString);
            /* handling of positive server responses for async-post-requests */
            void handlePostResponse(string response, string requestBody);
            /* handling of negative server responses for async-post-requests */
            void handlePostError(string response, string requestBody);
        }

        public class ServerResponseHandling : IServerResponseHandling
        {
            private bool fireAgain = true;
            public string sqlConnectionName { get; set; }
            public int invalidContributionsCounter { get; set; }
            public int retriedCompositionsCounter { get; set; }
            public int storedInvalidCounter { get; set; }
            public int modifiedCounter { get; set; }
            public int validCounter { get; set; }
            public ThinkHandle thinkHandle { get; set; }
            public ContributionsHandler contributionsHandler { get; set; }
            public ConcurrentDictionary<string, string[]> invalidCompositionsPaths { get; set; }

            public ServerResponseHandling(string sqlConnectionName)
            {
                this.sqlConnectionName = sqlConnectionName;

                this.storedInvalidCounter = 0;
                this.modifiedCounter = 0;
                this.invalidContributionsCounter = 0;
                this.retriedCompositionsCounter = 0;
                this.validCounter = 0;

                this.invalidCompositionsPaths = new ConcurrentDictionary<string, string[]>();
            }

            /* handling of positive server responses for async-post-requests */
            void IServerResponseHandling.handlePostResponse(string response, string requestBody)
            {
                /*
                //ehr-creation positive response body
                {
                  "meta": {
                    "href": "http://172.24.8.56:8081/rest/v1/ehr/0a9e39a9-7a30-4128-ab4b-7fb98e3d2986"
                  },
                  "action": "CREATE",
                  "ehrId": "0a9e39a9-7a30-4128-ab4b-7fb98e3d2986"
                }

                //contribution positive response body
                {
                    "commitData": [
                        {
                            "href": "http://localhost:9003/rest/v1/composition/07cea606-3b9b-492b-a3fc-ecc31dc8493b::default::1",
                            "id": "07cea606-3b9b-492b-a3fc-ecc31dc8493b::default::1",
                            "action": "CREATE"
                        }
                    ]
                }
                */

                Dictionary<string, object> responseDict = new JavaScriptSerializer().DeserializeObject(response) as Dictionary<string, object>;
                if (!responseDict.ContainsKey("commitData"))
                { //is no response for contribution-request, so its a response for ehr-creation-request
                    if (responseDict.ContainsKey("ehrId"))
                    {
                        string subjectId = this.substrBetween("subjectId=", "&", requestBody); //extract subjectId from requestBody which isn't a body but a URL-string in that case
                        contributionsHandler.assignEhrId(subjectId, responseDict["ehrId"].ToString());
                    }
                    else
                    {
                        thinkHandle.Dts.Events.FireError(0, "ServerResponseHandling", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Unexpected response: " + response + " on request: " + requestBody, String.Empty, 0);
                    }
                }
                else
                {
                    this.validCounter++;
                }
            }

            /* returns substring from sourceString between delimiterStart and delimiterEnd*/
            private string substrBetween(string delimiterStart, string delimiterEnd, string sourceString)
            {
                int pFrom = sourceString.IndexOf(delimiterStart) + delimiterStart.Length;
                string tmpStr = sourceString.Substring(pFrom);
                int pTo = tmpStr.IndexOf(delimiterEnd);
                return tmpStr.Substring(0, pTo);
            }

            /* handling of negative server responses for async-post-requests */
            void IServerResponseHandling.handlePostError(string response, string requestBody)
            {
                /* 
				//ehr-creation negative response body
                {
                  "status": 400,
                  "code": "EHR-2124",
                  "userMessage": "The EHR for this subject already exists.",
                  "developerMessage": "The EHR with this subject ID and namespace combination already exists.",
                  "exceptionMessage": "EHR with this subject ID and subject namespace already exists!",
                  "moreInfo": "https://confluence.ehrvendor.com/display/ehrrest/EHR-2124",
                  "requestHref": "http://172.24.8.56:8081/rest/v1/ehr?subjectId=1&subjectNamespace=Test"
                }
				
                //contribution negative response bodys
                { //1
                  "status": 400,
                  "code": "COMP-1041",
                  "userMessage": "Could not parse submitted composition.",
                  "developerMessage": "Cannot parse the submitted composition JSON. Is it in the right format?",
                  "exceptionMessage": "Expected a JSON array!",
                  "moreInfo": "https://confluence.ehrvendor.com/display/ehrrest/COMP-1041",
                  "requestHref": "http://172.24.8.56:8081/rest/v1/composition/contribution"
                }
                { //2
                  "status": 400,
                  "code": "COMP-1081",
                  "userMessage": "Composition validation failed (there are missing or invalid values).",
                  "developerMessage": "Composition validation failed (there are missing or invalid values).",
                  "exceptionMessage": "There were errors in validation: Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:41.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #3']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #10']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #15']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #15']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #18']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #18']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #18']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #18']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #20']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #30']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #30']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #30']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #30']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #32']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #33']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #33']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #33']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #33']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #35']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #38']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #38']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #38']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #38']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #40']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #40']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #40']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #40']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #45']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #45']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #49']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #52']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #52']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #52']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #52']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #54']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #57']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #57']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #57']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #57']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:41.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #59']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #59']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #59']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #59']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #61']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #65']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #68']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:41.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #72']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #72']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #72']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #72']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #81']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #81']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #82']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #83']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #83']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #83']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #83']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #85']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #86']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #90']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #91']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #93']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #97']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #100']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #100']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #100']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #100']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #101']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #112']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #112']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #112']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #112']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #115']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #117']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #119']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #120']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #122']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #125']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #128']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #128']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #128']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #128']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #132']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #137']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #137']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #137']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #137']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #139']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #146']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #146']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #146']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #146']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #148']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #153']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #155']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #155']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #155']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #155']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #158']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #158']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #158']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #158']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #160']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #165']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #169']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #173']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #173']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #173']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #173']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #176']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #180']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #180']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #180']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #180']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #181']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #188']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #193']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #193']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #193']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #193']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #196']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #201']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #201']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #201']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #201']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #205']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #210']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #211']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #211']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #211']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #211']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #213']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #214']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #215']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #218']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #220']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #220']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #220']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #220']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #222']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #222']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #222']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #222']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #227']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #230']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #230']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #230']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #230']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #234']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #237']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #239']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #240']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #242']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #242']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #242']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #242']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #243']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #245']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #245']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #245']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #245']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #246']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #246']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #246']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #246']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #247']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #248']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #248']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #248']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #248']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #251']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #251']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #251']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #251']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #257']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #260']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #260']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #260']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #260']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #262']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #265']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #266']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #266']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #266']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #266']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #269']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #277']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #281']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #281']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #281']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #281']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #282']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #282']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #282']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #282']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #285']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #289']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #293']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #293']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #293']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #293']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #296']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #300']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #300']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #300']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #300']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #302']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #302']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #302']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #302']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #304']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #304']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:165.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #307']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:18.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #307']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #309']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:52.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #311']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #311']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #311']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #311']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #312']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:50.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #314']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:50.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #315']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #315']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #315']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #315']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #317']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #317']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #317']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #317']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #320']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:18.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #321']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #325']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #325']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #325']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #325']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:52.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #327']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:50.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #328']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:18.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #331']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #336']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #337']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #340']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #340']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #340']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #340']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #342']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #345']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #345']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #345']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #345']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #346']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #346']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #346']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #346']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:18.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #347']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #347']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #347']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #347']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #349']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #350']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #354']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #354']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #354']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #354']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #357']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #357']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #357']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #357']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #361']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #364']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #364']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #366']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #368']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #368']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #368']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #368']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #370']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #371']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #373']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #377']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #378']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #378']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #378']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #378']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #379']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #381']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #381']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #381']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #381']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #383']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #383']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #383']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #383']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #385']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #387']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #387']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #387']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #387']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #390']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #390']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #390']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #390']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #392']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #396']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #398']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #398']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #398']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #398']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #400']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #400']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #400']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #400']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #401']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #404']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #405']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #407']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #409']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #410']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #411']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #414']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #418']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #421']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #426']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #428']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #430']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #433']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #433']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #433']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #433']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #436']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #440']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #443']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #446']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #451']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #451']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #451']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #451']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #453']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #453']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #453']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #453']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #457']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #460']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #461']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #465']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #472']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #474']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #477']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #477']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #477']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #477']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #481']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #485']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #488']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #488']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #488']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #488']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #490']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #490']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #490']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #490']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #495']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #499']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:59.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #503']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #504']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #506']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #506']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #506']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #506']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #509']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #511']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #514']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #517']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #517']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #517']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #517']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #518']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #518']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #518']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #518']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #526']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #528']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #528']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #528']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #528']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #531']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #535']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #536']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #537']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #537']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #537']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #537']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #539']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #539']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #539']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #539']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #541']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #543']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #544']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #546']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #547']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #549']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #549']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #549']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #549']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #551']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #552']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #555']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #555']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #555']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #555']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #559']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #559']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #559']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #559']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #562']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #563']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #565']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #567']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #569']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #572']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #575']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #575']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #575']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #575']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #579']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #580']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #580']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #580']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #580']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:0.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #581']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #595']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #595']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #595']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #595']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:0.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #596']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #601']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #601']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #601']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #601']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #603']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:0.0, U:l/Min",
                  "moreInfo": "https://confluence.ehrvendor.com/display/ehrrest/COMP-1081",
                  "requestHref": "http://172.24.8.56:8081/rest/v1/composition/contribution"
                }
             
				*/

                //handle if there was no response - probably because connection was aborted for some reason
                if (response == "no")
                {
                    if (!requestBody.Contains(suppressedBadEntriesTag)) this.storedInvalidCounter++;
                    this.storeProblematicRequests(requestBody, response);

                    if (this.invalidCompositionsPaths.ContainsKey("No response"))
                    {
                        int tmp = Int32.Parse(this.invalidCompositionsPaths["No response"][0]);
                        this.invalidCompositionsPaths["No response"][0] = "" + (tmp + 1);
                    }
                    else
                    {
                        this.invalidCompositionsPaths["No response"] = new string[] { "1", "(usually connection timeout because validation took to long)", "0" };
                    }
                }
                else
                {
                    Dictionary<string, object> responseDict = new JavaScriptSerializer().DeserializeObject(response) as Dictionary<string, object>;
                    if (responseDict.ContainsKey("code"))
                    {
                        if (responseDict["code"].ToString().StartsWith("EHR") == true)
                        {
                            if (responseDict["code"].ToString() == "EHR-2124")
                            { //ehr for subjectId in namespace already exists
                                //get the ehrId from Think!-platform
                                //TODO it doesn't check how much requests are open, this could be handled in httpRequst function not it checkWaitingContributions 
                                thinkHandle.httpRequest("ehr?" + responseDict["requestHref"].ToString().Split(new[] { "ehr?" }, StringSplitOptions.None)[1].Replace("\\u0026", "&"), true, null, "application/json");
                            }
                            else
                            {
                                if (debug > 2) { System.IO.File.AppendAllText(logToPath + "Log.txt", "ThinkHandle - response: " + string.Join(";", responseDict.Select(x => x.Key + "=" + x.Value).ToArray()) + Environment.NewLine); }

                                thinkHandle.Dts.Events.FireError(0, "ServerResponseHandling", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Unexpected response on EHR-creation attempt: " + response, String.Empty, 0);
                            }
                        }
                        else if (responseDict["code"].ToString().StartsWith("COMP") == true)
                        {
                            string errorDescription = response;
                            Regex rgx = new Regex("");
                            int suppressBadValuesState = 0;
                            string newRequestBody = requestBody;

                            //try to produce helpful statistics about invalid contributions
                            try {
                                string aTmpStr = responseDict["exceptionMessage"].ToString().Replace("There were errors in validation:", "");
                                string[] compositionErrors = aTmpStr.Split(new string[] { "; " }, StringSplitOptions.None);

                                if (debug > 3) System.IO.File.AppendAllText(logToPath + "Log" + "_ErrorCounts_" + Thread.CurrentThread.ManagedThreadId + ".txt", "Errors in Composition: " + compositionErrors.Count() + Environment.NewLine);

                                // state = 0 -> just count distinct  problems, 
                                // state = 1 -> try to suppress problematic entries but nothing suppressed yet 
                                // state = 2 -> try to suppress problematic entries at least one bad entry suppressed already
                                if ((compositionErrors.Count() / Regex.Matches(newRequestBody, "\\\",").Count < worthCleansingLevel) && (!requestBody.Contains(suppressedBadEntriesTag)))
                                {
                                    suppressBadValuesState = 1;
                                }

                                foreach (string compositionError in compositionErrors)
                                {
                                    //wenn Anzahl Fehler < x,x% der Composition Eintr�ge, entsprechende Eintr�ge rausschmei�en
                                    if (suppressBadValuesState > 0)
                                    {
                                        try
                                        {

                                            if (Regex.Matches(compositionError, ".+\\(path:").Count > 0)
                                            {
                                                //"Invalid decimal value: 3,7 (path: report/ventilator_observations:1/ventilator_settings_findings2/oxygen_delivery/ambient_oxygen_en/percent_o2_en|numerator)",
                                                string tmpPath = compositionError.Trim();
                                                string[][] requestPatterns = new string[][] { new string[] {" #\\d+", ""}, 
                                        new string[]{".+\\(path:", ""},
                                        new string[]{"\\)", ""}, 
                                        new string[]{"\\/", "\\/"}, 
                                        new string[]{"\\[", "\\["}, 
                                        new string[]{"\\]", "\\]"},
                                        new string[]{"\\|", "\\|"} };
                                                foreach (string[] pattern in requestPatterns)
                                                {
                                                    rgx = new Regex(pattern[0]);
                                                    tmpPath = rgx.Replace(tmpPath, pattern[1]);
                                                }

                                                rgx = new Regex("\\\"" + tmpPath.Trim() + "\\\":(\\\"[^\\\"]*\\\",|[^\\\"]*,)?");
                                                newRequestBody = rgx.Replace(newRequestBody, "", 1);

                                                suppressBadValuesState = 2;
                                                this.logProblematicEntry(compositionError, true);
                                            }
                                            else if (Regex.Matches(compositionError, "actual:").Count > 0)
                                            {
                                                //Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #372']/data/events[at0002,'Any event']/data/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Positive End Expiratory Pressure (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:413.0, P:1, U:mbar";
                                                string tmpError = compositionError.Trim();
                                                //get entry number from error description - result z.B. 372
                                                string tmpN = "";
                                                rgx = new Regex("#[0-9]+']");
                                                Match match = rgx.Match(tmpError);
                                                if (match.Success)
                                                {
                                                    tmpN = match.Value;
                                                    rgx = new Regex("[0-9]+");
                                                    match = rgx.Match(tmpN);
                                                    if (match.Success)
                                                    {
                                                        tmpN = match.Value;
                                                    }
                                                }
                                                else //bei der ersten instanz geben sie keine #1 an, sondern einfach keine Nummer
                                                {
                                                    tmpN = "1";
                                                }
                                                int tmpInt = Int32.Parse(tmpN);
                                                tmpN = "" + (tmpInt - 1); //die Nummer im Webtemplate-Path ist eins niedriger als die in der Fehlermeldung
                                                //get value from error description - result z.B. 4130
                                                string tmpM = "";
                                                rgx = new Regex("actual:[^;]+"); //z.B. actual: quantity M:413.0, P:1, U:mbar"
                                                match = rgx.Match(tmpError);
                                                if (match.Success)
                                                {
                                                    tmpM = match.Value;
                                                    rgx = new Regex("M:.+?, "); // z.B. M:4130, 
                                                    match = rgx.Match(tmpM);
                                                    if (match.Success)
                                                    {
                                                        tmpM = match.Value;
                                                        rgx = new Regex(", ");
                                                        tmpM = rgx.Replace(tmpM, "");
                                                        rgx = new Regex("M:");
                                                        tmpM = rgx.Replace(tmpM, "");
                                                        tmpM = tmpM.Split('.')[0];
                                                    }
                                                }
                                                rgx = new Regex("\\\"[^,]+?:" + tmpN + "[^\"]+\":\\s?(\\\"" + tmpM + "\\\",|" + tmpM + ",)");
                                                //get path so other pathes like e.g. unit can get removed too
                                                string tmpP = "wuseldusel";
                                                match = rgx.Match(newRequestBody);
                                                if (match.Success)
                                                {
                                                    tmpP = match.Value;
                                                    tmpP = tmpP.Split('|')[0];
                                                }
                                                rgx = new Regex(tmpP + "\\|[^\"]+\\\":\\s?(\\\"[^\"]*\\\",|[^\"]*,)?");
                                                newRequestBody = rgx.Replace(newRequestBody, "");

                                                suppressBadValuesState = 2;
                                                this.logProblematicEntry(compositionError, true);
                                            }
                                            else // auch Fehler merken, falls ich den nicht kannte und also auch nicht aufbereiten kann
                                            {
                                                this.logProblematicEntry(compositionError);
                                            }
                                            //in feeder_audit sollten ja sowieso noch Original-Werte hinzugef�gt werden (TODO) deswegen hier nicht zwingend n�tig die irgendwo zu speichern
                                        }
                                        catch (Exception ex)
                                        {
                                            thinkHandle.Dts.Events.FireWarning(0, "handlePostError", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Error while trying to parse server's hint's line on invalid contribution: " + ex.ToString(), string.Empty, 0);
                                            thinkHandle.Dts.Events.FireWarning(0, "handlePostError", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Resonse: " + response, string.Empty, 0);
                                            thinkHandle.Dts.Events.FireWarning(0, "handlePostError", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Request " + requestBody, string.Empty, 0);
                                        }
                                    }
                                    else //sonst z�hlen
                                    {
                                        // if this composition is not a modified one, log the problematic entries to help while data cleansing/debugging of mapping
                                        if (!requestBody.Contains(suppressedBadEntriesTag)) this.logProblematicEntry(compositionError);

                                        if (requestBody.Contains(suppressedBadEntriesTag)) {
                                            thinkHandle.Dts.Events.FireWarning(0, "ServerResponseHandling", "ok, i noticed the modification marker and the contribution still sucks. There is a special place in hell for that one:", string.Empty, 0);
                                            if (debug > 2) System.IO.File.AppendAllText(logToPath + "Log" + Thread.CurrentThread.ManagedThreadId + ".txt", "modified request which is still invalid - " + requestBody + Environment.NewLine);
                                        }

                                        this.storedInvalidCounter++;
                                    }
                                }

                                
                                if (suppressBadValuesState > 0)
                                {
                                    string entryPoint = "composition\": {";
                                    rgx = new Regex("composition.:.*?{");
                                    newRequestBody = rgx.Replace(newRequestBody, entryPoint + "\"" + suppressedBadEntriesPathTag + "|system_id\": \"SSIS-generic-think-feeder - " + versionTag + "\",");
                                    newRequestBody = rgx.Replace(newRequestBody, entryPoint + "\"" + suppressedBadEntriesPathTag + "|version_id\": \"SSIS-generic-think-feeder - " + suppressedBadEntriesTag + "\",");

                                    if (suppressBadValuesState == 1) 
                                    {
                                        this.storedInvalidCounter++;
                                    }
                                    else //Composition changed, send it again
                                    {
                                        if (debug > 4) System.IO.File.AppendAllText(logToPath + "Log" + Thread.CurrentThread.ManagedThreadId + ".txt", "original request - " + requestBody + Environment.NewLine);
                                        if (debug > 4) System.IO.File.AppendAllText(logToPath + "Log" + Thread.CurrentThread.ManagedThreadId + ".txt", "modified request - " + newRequestBody + Environment.NewLine);

                                        //TODO it doesn't check how much requests are open, this could be handled in httpRequst function not it checkWaitingContributions
                                        thinkHandle.httpRequest("composition/contribution/", true, newRequestBody, "application/json");
                                        this.modifiedCounter++;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                thinkHandle.Dts.Events.FireError(0, "handlePostError", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Error while trying to parse servers hint on invalid contribution: " + ex.ToString(), string.Empty, 0);
                                thinkHandle.Dts.Events.FireWarning(0, "handlePostError", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Resonse: " + response, string.Empty, 0);
                                thinkHandle.Dts.Events.FireWarning(0, "handlePostError", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Request " + requestBody, string.Empty, 0);
                            }

                            if (suppressBadValuesState != 1) //store it with error details in error table
                            {
                                this.storeProblematicRequests(requestBody, "error handling state: " + suppressBadValuesState + ", response: " +  errorDescription);
                            }
                        }
                        else
                        {
                            thinkHandle.Dts.Events.FireError(0, "ServerResponseHandling", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Pretty unexpected response: " + response, String.Empty, 0);
                        }
                    }
                    else
                    {
                        thinkHandle.Dts.Events.FireError(0, "ServerResponseHandling", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Completly unexpected response: " + response, String.Empty, 0);
                    }
                }
            }

            private void logProblematicEntry(string compositionError, bool suppressed = false)
            {
                Regex rgx = new Regex("");
                string tmpStr = compositionError.Trim();

                //extract some extra info from the error instance which shall be logged individually
                string extraInfo = "";
                if (compositionError.Contains("actual:"))
                {
                    rgx = new Regex("actual:.+");
                    Match match = rgx.Match(compositionError);
                    if (match.Success)
                    {
                        extraInfo = match.Value;
                    }
                }
                else if (compositionError.Contains("value:"))
                {
                    rgx = new Regex("value:.+\\(");
                    Match match = rgx.Match(compositionError);
                    if (match.Success)
                    {
                        extraInfo = match.Value;
                    }
                }

                //extract the error path which can occur in multiple error-instances but shall be displayed only once
                string[][] patterns = new string[][] { new string[] {" #\\d+", ""}, 
                                        new string[]{":\\d+\\/", ":0/"},
                                        new string[]{":\\d+\\|", ":0|"}, 
                                        new string[]{"actual:.+", "actual:aValue"}, 
                                        new string[]{"value:.+\\(", "value: aValue ("} };
                foreach (string[] pattern in patterns)
                {
                    rgx = new Regex(pattern[0]);
                    tmpStr = rgx.Replace(tmpStr, pattern[1]);
                }
                tmpStr = tmpStr.Trim();

                int counterIndex = suppressed ? 2 : 0;
                //enter the entry in dict for problematic entries
                if (!this.invalidCompositionsPaths.ContainsKey(tmpStr))
                {
                    this.invalidCompositionsPaths[tmpStr] = new string[] { "0", extraInfo, "0" };
                }
                int tmp = Int32.Parse(this.invalidCompositionsPaths[tmpStr][counterIndex]);
                this.invalidCompositionsPaths[tmpStr][counterIndex] = "" + (tmp + 1);
                this.invalidCompositionsPaths[tmpStr][1] += " --- " + extraInfo;
            }

            public void logInvalidCompositionsPaths()
            {
                System.IO.File.AppendAllText(logToPath + "Log.txt", "ThinkHandle - Logging InvalidCompositionsPaths: " + Environment.NewLine);
                System.IO.File.AppendAllText(logToPath + "Log.txt", Environment.NewLine);
                foreach (KeyValuePair<string, string[]> entry in this.invalidCompositionsPaths)
                {
                    System.IO.File.AppendAllText(logToPath + "Log.txt", entry.Value[0] + "/" + entry.Value[2] + " x " + entry.Key + Environment.NewLine);
                    System.IO.File.AppendAllText(logToPath + "Log.txt", "Extra Infos: " + entry.Value[1] + Environment.NewLine);
                    System.IO.File.AppendAllText(logToPath + "Log.txt", Environment.NewLine);
                }
            }

            public void storeProblematicRequests(string requestBody, string response)
            {
                string tmp = "INSERT INTO " + thinkHandle.Dts.Variables[errorTableTag].Value + " (composition, error_description) VALUES (@compostition, @error_desc);";
                try
                {
                    ConnectionManager cm = thinkHandle.Dts.Connections[sqlConnectionName];
                    System.Data.SqlClient.SqlConnection con = (System.Data.SqlClient.SqlConnection)cm.AcquireConnection(thinkHandle.Dts.Transaction);
                    System.Data.SqlClient.SqlCommand sql = new System.Data.SqlClient.SqlCommand(tmp, con);
                    sql.Parameters.Add("@compostition", SqlDbType.Text);
                    sql.Parameters.Add("@error_desc", SqlDbType.VarChar);
                    sql.Parameters["@compostition"].Value = requestBody;
                    sql.Parameters["@error_desc"].Value = response;
                    sql.ExecuteNonQuery();
                    cm.ReleaseConnection(con);
                }
                catch
                {
                    thinkHandle.Dts.Events.FireError(0, "ServerResponseHandling", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Error while storing invalid composition. SQL: " + tmp, string.Empty, 0);
                }
            }

            /* handling of positive server responses for async-get-requests */
            void IServerResponseHandling.handleGetResponse(string response)
            {
                /*
                //ehr-retrieval positive response body
                {
                  "meta": {
                    "href": "http://localhost:8082/rest/v1/ehr/f77f9b4a-cfda-414d-aa6c-4f78bcac7601"
                  },
                  "action": "RETRIEVE",
                  "ehrStatus": {
                    "subjectId": "90470912",
                    "subjectNamespace": "ExternalDB",
                    "queryable": true,
                    "modifiable": true
                  },
                  "ehrId": "f77f9b4a-cfda-414d-aa6c-4f78bcac7601"
                }
                */

                Dictionary<string, object> responseDict = new JavaScriptSerializer().DeserializeObject(response) as Dictionary<string, object>;
                if (responseDict.ContainsKey("ehrId"))
                {
                    Dictionary<string, object> ehrStatus = responseDict["ehrStatus"] as Dictionary<string, object>;
                    string subjectId = ehrStatus["subjectId"].ToString(); //extract subjectId from response
                    contributionsHandler.assignEhrId(subjectId, responseDict["ehrId"].ToString());
                }
                else
                {
                    thinkHandle.Dts.Events.FireError(0, "ServerResponseHandling", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Unexpected response: " + response, String.Empty, 0);
                }

            }
        }
        #endregion

        #region class for interactions with REST-API of Think!EHR platform
        public class ThinkHandle
        {
            public Microsoft.SqlServer.Dts.Tasks.ScriptTask.ScriptObjectModel Dts { get; set; }
            public string thinkHTTPConnection { get; set; }
            public ConcurrentDictionary<string, bool> openRequests { get; set; }
            public DateTime lastResponse { get; set; }
            public int requestCounter { get; set; }
            public IServerResponseHandling responseHandle { get; set; }

            public ThinkHandle(Microsoft.SqlServer.Dts.Tasks.ScriptTask.ScriptObjectModel Dts, string thinkHTTPConnectionName, IServerResponseHandling responseHandle)
            {
                this.Dts = Dts;
                this.thinkHTTPConnection = thinkHTTPConnectionName;
                this.responseHandle = responseHandle;

                this.openRequests = new ConcurrentDictionary<string, bool>();
                this.lastResponse = DateTime.UtcNow;
                this.requestCounter = 0;
            }

            /* 	make a http-request to Think-REST-API
            @param urlAppendix specifies to which part of the API e.g. http://www.thinkehraddress.de/composition/contribution would use appendix "composition/contribution" (yes, without first slash)
            @param async request synchronous or asynchronous
            @param requestBody if value is given, this will be the post-requests body. If no value is given there will be a get-request.
            @param contentType if value is given, contentType-header will be set with that value
            */
            public string httpRequest(string urlAppendix, bool async, string requestBody, string contentType)
            {
                if (this.Dts.Connections.Contains(this.thinkHTTPConnection) == false)
                {
                    this.Dts.Events.FireError(0, "ThinkHandle", "Expected variable missing: " + this.thinkHTTPConnection + "(Connection)", string.Empty, 0);
                    this.Dts.TaskResult = (int)ScriptResults.Failure;
                }
                try
                {
                    // Create a webclient
                    Uri address = new Uri(this.Dts.Connections[this.thinkHTTPConnection].ConnectionString + urlAppendix);
                    WebClient myWebClient = new WebClient();
                    myWebClient.Encoding = Encoding.UTF8;
                    HttpClientConnection con = new HttpClientConnection(this.Dts.Connections[this.thinkHTTPConnection].AcquireConnection(null));
                    myWebClient.Credentials = new System.Net.NetworkCredential(con.ServerUserName, con.GetServerPassword());
                    myWebClient.BaseAddress = address.AbsoluteUri;
                    if (debug > 5) { System.IO.File.AppendAllText(logToPath + "Log.txt", "ThinkHandle - Request - URL: " + address.ToString() + " body: " + requestBody + Environment.NewLine); }
                    
                    this.requestCounter++;
                    if (contentType != null)
                    {
                        myWebClient.Headers.Add(HttpRequestHeader.ContentType, contentType);
                    }
                    //send request
                    if ((requestBody == null) && (!async))
                    {
                        return myWebClient.DownloadString(address);
                    }
                    else if ((requestBody == null) && (async))
                    {
                        myWebClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(getCallback);
                        myWebClient.DownloadStringAsync(address);
                        this.openRequests[address.ToString()] = true;
                        return "async request";
                    }
                    else
                    {
                        if (async)
                        {
                            myWebClient.UploadStringCompleted += (sender, e) => postCallback(sender, e, requestBody);
                            myWebClient.UploadStringAsync(address, requestBody);
                            if (requestBody == "")
                            {
                                this.openRequests[address.ToString()] = true;
                            }
                            else
                            {
                                this.openRequests[requestBody] = true;
                            }
                            return "async request";
                        }
                        else
                        {
                            return myWebClient.UploadString(address, requestBody);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.Dts.Events.FireError(0, "ThinkHandle", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " HTTP-POST-Request failed: " + ex.Message, string.Empty, 0);
                    this.Dts.TaskResult = (int)ScriptResults.Failure;
                    return "Error on http-request";
                }
            }

            /* callback for async-get-requests to Think-REST-API */
            private void getCallback(object sender, DownloadStringCompletedEventArgs e)
            {
                this.lastResponse = DateTime.UtcNow;
                try
                {
                    WebClient tmpWC = (WebClient)sender;
                    
                    bool notUseless = false;
                    int aCounter = 0;
                    while((!notUseless) && (aCounter<10)) {
                        this.openRequests.TryRemove(tmpWC.BaseAddress, out notUseless);
                        if (!notUseless) System.Threading.Thread.Sleep(50);
                        aCounter++;
                    }
                    if (!notUseless) this.Dts.Events.FireWarning(0, "ThinkHandle", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "Removing request type 1 failed", string.Empty, 0);

                    this.responseHandle.handleGetResponse(e.Result);
                }
                catch (Exception ex)
                {
                    this.Dts.Events.FireError(0, "ThinkHandle", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " HTTP-GET-Request failed: " + ex.Message, string.Empty, 0);
                }
            }

            /* callback for async-post-requests to Think-REST-API */
            private void postCallback(Object sender, UploadStringCompletedEventArgs e, string requestBody)
            {
                int state = 0;
                this.lastResponse = DateTime.UtcNow;
                Exception error = e.Error;
                try
                {
                    if (requestBody == "")
                    { //if there is no requestBody given, adress is used to identify requests
                        WebClient tmpWC = (WebClient)sender;
                        requestBody = tmpWC.BaseAddress;
                        state = 1;
                    }

                    bool notUseless = false;
                    int aCounter = 0;
                    while ((!notUseless) && (aCounter < 10))
                    {
                        state = 2;
                        this.openRequests.TryRemove(requestBody, out notUseless);
                        if (!notUseless) System.Threading.Thread.Sleep(50);
                        aCounter++;
                    }
                    if (!notUseless) this.Dts.Events.FireWarning(0, "ThinkHandle", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "Removing request type 2 failed", string.Empty, 0);

                    state = 3;
                    if (error != null)
                    {
                        state = 4;
                        if (debug > 3) System.IO.File.AppendAllText(logToPath + "Log" + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", "ThinkHandle - error: " + error.ToString() + Environment.NewLine);
                        WebException webEx = (WebException)error;
                        state = 5;
                        HttpWebResponse webResp = (HttpWebResponse)webEx.Response;
                        state = 6;
                        if (webEx.Response != null)
                        {
                            state = 7;
                            string response = new StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                            state = 8;
                            this.responseHandle.handlePostError(response, requestBody);
                            state = 9;
                        }
                        else
                        {
                            state = 10;
                            if (debug > 4) System.IO.File.AppendAllText(logToPath + "Log" + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", "ThinkHandle" + requestBody + Environment.NewLine);
                            state = 11;
                            if (debug > 3) System.IO.File.AppendAllText(logToPath + "SpecialRequest" + "_" + requestBody.GetHashCode() + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", requestBody);
                            if (debug > 3) { System.IO.File.AppendAllText(logToPath + "Log" + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", requestBody.GetHashCode() + "," + requestBody.Length + ", " + requestBody.Count(x => x == ',') + Environment.NewLine); }
                            this.responseHandle.handlePostError("no", requestBody);
                            state = 12;
                        }
                    }
                    else
                    {
                        state = 13;
                        this.responseHandle.handlePostResponse(e.Result, requestBody);
                    }
                }
                catch (Exception ex)
                {
                    this.Dts.Events.FireError(0, "ThinkHandle", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Post-request failed for unknown reason! State:"+state+", Exception Details: " + ex.ToString(), string.Empty, 0);
                    this.Dts.Events.FireWarning(0, "ThinkHandle", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "Request Body: " + requestBody, string.Empty, 0);
                    this.Dts.Events.FireWarning(0, "ThinkHandle", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "ThinkHandle - error: " + error.ToString(), string.Empty, 0);
                }
            }
        }
        #endregion

        /// <summary>
        /// This method is called when this script task executes in the control flow.
        /// Before returning from this method, set the value of Dts.TaskResult to indicate success or failure.
        /// To open Help, press F1.
        /// </summary>
        public void Main()
        {
            try
            {
                System.Net.ServicePointManager.DefaultConnectionLimit = maxOpenRequests + (maxOpenRequests / 5);

                #region preparations - initial checks, setting variables, logging infos, ...
                if (debug > 1) System.IO.File.AppendAllText(logToPath + "Log.txt", DateTime.Now.ToString("dd/MM/yyyy h:mm tt") + " - New Iteration" + Environment.NewLine);
                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " hooking up basic actors", String.Empty, 0, ref fireAgain);
                //hook up basic actors
                EtlBuilderInfo etlBuilderInfo = new EtlBuilderInfo();
                ServerResponseHandling responseHandle = new ServerResponseHandling(sqlConnectionName);
                ThinkHandle thinkHandle = new ThinkHandle(Dts, thinkHTTPConnectionName, responseHandle);
                responseHandle.thinkHandle = thinkHandle;
                SummaryStatistics summaryStatistics = new SummaryStatistics(thinkHandle, responseHandle);

                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " create and fill DataTable with RecordSet which holds the data to be send", String.Empty, 0, ref fireAgain);
                //create and fill DataTable with RecordSet which holds the data to be send
                OleDbDataAdapter oleDA = new OleDbDataAdapter();
                DataTable dt = new DataTable();
                oleDA.Fill(dt, Dts.Variables[recordSetToPostTag].Value);

                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, "reading in instructions", String.Empty, 0, ref fireAgain);
                //read in instructions
                ContributionsHandler contributionsHandler = new ContributionsHandler(thinkHandle);
                summaryStatistics.contributionsHandler = contributionsHandler;
                responseHandle.contributionsHandler = contributionsHandler;
                InstructionsHandler instructionsHandler = new InstructionsHandler(Dts.Variables[conditionsAndActionsTag].Value.ToString(), Dts, contributionsHandler);

                //forward at codes to contributionsHandler .ContainsKey(key)
                if (instructionsHandler.columnInstructionsKeyToIndex.ContainsKey("NoCollumnJustAdditionalInformationAboutAtCodes"))
                {
                    contributionsHandler.setAtCodes(instructionsHandler.columnInstructions[instructionsHandler.columnInstructionsKeyToIndex["NoCollumnJustAdditionalInformationAboutAtCodes"]]);
                }
                if (instructionsHandler.columnInstructionsKeyToIndex.ContainsKey("NoCollumnJustADictionaryWithStringsToReplace"))
                {
                    contributionsHandler.setStringsToReplace(instructionsHandler.columnInstructions[instructionsHandler.columnInstructionsKeyToIndex["NoCollumnJustADictionaryWithStringsToReplace"]]);
                }

                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, "assembling infos for ETL-Builder-Tool and log them as JSON-String", String.Empty, 0, ref fireAgain);
                //assemble infos for ETL-Builder-Tool and log them as JSON-String
                etlBuilderInfo.setExampleDataFromDataTable(dt);
                etlBuilderInfo.columnInstructions = instructionsHandler.columnInstructions;
                etlBuilderInfo.getPathsAndOPT(Dts.Variables[templateIdTag].Value.ToString(), thinkHandle);
                Dts.Events.FireInformation(0, componentNameUsedForLogging, etlBuilderInfo.asJSON(), String.Empty, 0, ref fireAgain);


                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, "checking if there is data to transmit", String.Empty, 0, ref fireAgain);
                //check if there is data to transmit
                if (dt.Rows.Count == 0)
                {
                    Dts.Events.FireWarning(0, componentNameUsedForLogging, "PLEASE VERIFY: record set for storage was empty.", String.Empty, 0);
                    return;
                };
                #endregion

                #region create body as JSON-Array of contributions - compositions are built in FLAT Format
                //API-details: www.ehrscape.com/api-explorer.html and www.ehrscape.com/reference.html#_composition

                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " start iterating through all cells of dt", String.Empty, 0, ref fireAgain);
                //iterate through all cells of dt
                foreach (DataRow row in dt.Rows)
                {
                    summaryStatistics.rowCounter++;
                    foreach (DataColumn col in dt.Columns)
                    {
                        instructionsHandler.runOn(row, col);
                    }
                    if (summaryStatistics.rowCounter % 10000 == 0)
                    {
                        contributionsHandler.checkWaitingContributions(); // start firing requests earlier to be done faster
                        if ((debug > 2) && (summaryStatistics.rowCounter % 100000 == 0)) { Dts.Events.FireInformation(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " - Open requests: " + thinkHandle.openRequests.Count + ", Waiting Compositions: " + contributionsHandler.waitingForCommit.Count, String.Empty, 0, ref fireAgain); }
                    }
                }
                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " iterating through all cells done", String.Empty, 0, ref fireAgain);
                instructionsHandler.runOn(null, null); //to finish last contribution

                System.Threading.Thread.Sleep(100);

                if (debug > 2) summaryStatistics.log("Converted all relational data to compositions");

                #endregion

                #region wait while there are unanswered requests
                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, "waiting while there are compositions not sent or unanswered requests", String.Empty, 0, ref fireAgain);
                thinkHandle.lastResponse = DateTime.UtcNow;
                DateTime lastLog = DateTime.UtcNow;
                int reduceOutputBy = 50;
                while ((thinkHandle.openRequests.Count > 0) || contributionsHandler.waitingForCommit.Count > 0)
                {
                    if (DateTime.UtcNow.Subtract(thinkHandle.lastResponse).TotalSeconds > 120)
                    {
                        Dts.Events.FireWarning(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Last Response more than 2 minutes ago... stopping the job.", String.Empty, 0);
                        foreach (KeyValuePair<string, bool> tmp in thinkHandle.openRequests)
                        {
                            if (debug > 2) { System.IO.File.AppendAllText(logToPath + "Log.txt", "No response for: " + tmp + Environment.NewLine); }
                            responseHandle.storeProblematicRequests(tmp.Key, "no response");
                        }
                        //TODO store waiting contributions
                        thinkHandle.openRequests = new ConcurrentDictionary<string, bool>();
                        contributionsHandler.waitingForCommit = new ConcurrentDictionary<Contribution, bool>();
                    }
                    contributionsHandler.checkWaitingContributions();
                    System.Threading.Thread.Sleep(100);
                    if ((debug > 2) && (DateTime.UtcNow.Subtract(lastLog).TotalSeconds > 60)) {
                        lastLog = DateTime.UtcNow;
                        Dts.Events.FireInformation(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " - Open requests: " + thinkHandle.openRequests.Count + ", Waiting Compositions: " + contributionsHandler.waitingForCommit.Count, String.Empty, 0, ref fireAgain); 
                    }
                }

                System.Threading.Thread.Sleep(90000);

                if (debug > 1) responseHandle.logInvalidCompositionsPaths();

                #endregion

                if (contributionsHandler.waitingForCommit.Count > 0)
                {
                    Dts.Events.FireError(0, componentNameUsedForLogging, "Es sind noch nicht versendete Contributions vorhanden! (" + contributionsHandler.waitingForCommit.Count + ")", string.Empty, 0);
                }

                if (debug > 2) { Dts.Events.FireInformation(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " - Open requests (should be 0 now): " + thinkHandle.openRequests.Count + ", Waiting Compositions(should be 0 now): " + contributionsHandler.waitingForCommit.Count, String.Empty, 0, ref fireAgain); }

                #region some aftermath
                if (debug > 1) Dts.Events.FireInformation(0, componentNameUsedForLogging, "aftermath", String.Empty, 0, ref fireAgain);
                summaryStatistics.log("Finished waiting for responses");
                Dts.TaskResult = (int)ScriptResults.Success;// set result of Script Task to succesful
                dt.Clear();//clear DataTable dt
                #endregion

            }
            catch (Exception ex)
            {
                Dts.Events.FireError(0, componentNameUsedForLogging, DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Build/Commit of contributions failed: " + ex.Message, string.Empty, 0);
                Dts.TaskResult = (int)ScriptResults.Failure;
            }
        }

        #region ScriptResults declaration
        /// <summary>
        /// This enum provides a convenient shorthand within the scope of this class for setting the
        /// result of the script.
        /// 
        /// This code was generated automatically.
        /// </summary>
        enum ScriptResults
        {
            Success = Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Success,
            Failure = Microsoft.SqlServer.Dts.Runtime.DTSExecResult.Failure
        };
        #endregion

    }
}
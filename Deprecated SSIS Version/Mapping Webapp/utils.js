var Utils = Utils || {};

Utils.getUnique = function(myArray, onAttribute) {
    if (typeof onAttribute === 'undefined') {
        onAttribute = false;
    }
    var u = [];
    if (onAttribute) {
        for (var i = 0, le = myArray.length; i < le; ++i) {
            if ($.inArray(myArray[i][onAttribute], u) != -1) {
                continue;
            }
            else {
                u.push(myArray[i][onAttribute]);
            }
        }
    }
    else {
        for (var i = 0, le = myArray.length; i < le; ++i) {
            if ($.inArray(myArray[i], u) != -1) {
                continue;
            }
            else {
                u.push(myArray[i]);
            }
        }
    }
    return u;
};

/* simply iteratres recursively through a nested structure of objects and arrays calls the given callback function on each element */
Utils.iterateNestedStructure = function(structure, callback) {
    callback(structure);
    if (typeof structure === "object") {
        if (Array.isArray(structure)) {
            for (var i = 0, le = structure.length; i<le; i++) {
                Utils.iterateNestedStructure(structure[i], callback);
            }
        }
        else {
            for (var key in structure) {
                Utils.iterateNestedStructure(structure[key], callback);
            }
        }
    }
};

Utils.saveTextAsFile = function(textToWrite, fileNameToSaveAs) {
    var textFileAsBlob = new Blob([textToWrite], {type: 'text/plain'});

    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    if (window.URL != null)
    {
        // Chrome allows the link to be clicked
        // without actually adding it to the DOM.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
    }
    else
    {
        // Firefox requires the link to be added to the DOM
        // before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
    }

    downloadLink.click();
};
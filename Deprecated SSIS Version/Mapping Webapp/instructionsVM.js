function InstructionsVM() {
    var self = this;
    self.myJSON = ko.observable(false);
    self.myCommit = ko.observable(false);
    self.uname = ko.observable("");
    self.pw = ko.observable("");
    self.instructionsRaw = ko.observable();
    self.columns = ko.observableArray();
    self.paths = ko.observableArray();
    self.probableParams = ko.observableArray();
    self.exampleDataHeader = ko.observableArray();
    self.exampleData = ko.observableArray();
    self.currentParameter = false;
    
    self.addRawInstructions = function() {
        var parts = self.instructionsRaw().replace("<BEGIN_ETLBUILDER_INPUT>", "").replace("<END_ETLBUILDER_INPUT>", "").split("<DELIMITER>");
        iVM.addExampleData(JSON.parse(parts[1]));
        iVM.addInstructions(JSON.parse(parts[0]));
        iVM.addOPT(JSON.parse(parts[2]));
        iVM.addPaths(JSON.parse(parts[3]));
    };

    self.addExampleData = function(iExampleData) {
        for (var i = 0, le = iExampleData.length; i < le; i++) {
            if (i == 0) {
                self.exampleDataHeader(iExampleData[i]);
            }
            else {
                self.exampleData.push({row: iExampleData[i]});
            }
        }
    };
    
    self.addOPT = function(iOPT) {
        //F�gt zur Zeit nur f�r alle Elemente vom rmType OBSERVATION den Path "time" und "history_origin" an 
        var pathList = {};
        for (var i = 0, le = self.paths().length; i < le; i++) {
            var tmpPath = self.paths()[i];
            if (tmpPath.indexOf("ctx") > -1)
                continue;
            //tmpPath = tmpPath.replace(/\"/g, "").replace(/:<<index>>/g, "").replace(/: <<value>>/g, "");
            var components = tmpPath.split("/");
            var currentObject = iOPT.webTemplate.tree;
            var tmpSubpath = components[0];
            for (var j = 1, le2 = components.length; j < le2; j++) { //bei 1 beginnen weil 0 das bereits gew�hlte Element ist
                for (var k = 0, le3 = currentObject.children.length; k < le3; k++) {
                    if (currentObject.children[k].id == components[j].replace(/:<<index>>/g, "")) {
                        currentObject = currentObject.children[k];
                        tmpSubpath += "/" + components[j];
                        break;
                    }
                    if ((currentObject.rmType == "OBSERVATION")) { //so kann man ggf. sp�ter noch andere rmTypes hinzuf�gen
                        pathList[tmpSubpath] = "OBSERVATION";
                    }
                }
            }
        }

        for (var key in pathList) {
            if (pathList[key] == "OBSERVATION") {
                self.paths.push(key + "/time\": \"<<value>>\"");
                self.paths.push(key + "/history_origin\": \"<<value>>\"");
            }
        }
    };
    
    self.addPaths = function(iPaths) {
        //falls aus Textdatei, dann in brauchbares JSON umwandeln und einlesen
        if (typeof iPaths === 'string') {
            iPaths = JSON.parse(iPaths);
        }
        //jetzt pfade aufbereiten und auslesen
        var tmpPaths = [];
        for (var key in iPaths) {
            var currentLine = "\"" + key + "\": \"<<value>>\"";
            currentLine = currentLine.replace(/:0/g, ":<<index>>"); // alle Indexe (Nullen) in Beispielpfaden durch index-Platzhalter ersetzen
            var components = currentLine.split("<<index>>");
            //erstmal stumpf alle Subpfade bilden
            var tmpStr = components[0];
            tmpPaths.push(tmpStr);
            for (var j = 1, le2 = components.length; j < le2; j++) {
                tmpStr += "<<index>>" + components[j];
                tmpPaths.push(tmpStr);
            }
        }
        tmpPaths = Utils.getUnique(tmpPaths);
        for (var i = 0, le = tmpPaths.length; i < le; i++) {
            self.paths.push(tmpPaths[i]);
        }
    };
    
    self.addInstructions = function(iColumns) {
        for (var i = 0, le = iColumns.length; i < le; i++) {
            self.addColumn(null, null, iColumns[i], true);
        }
        self.updateCommit();
    };
    
    self.addColumn = function(sender, event, iColumn, push) {
        if (typeof push === 'undefined') {
            push = false;
        }
        ;
        if (typeof iColumn === 'undefined') {
            iColumn = {
                colName: "",
                cAndAs: []
            };
        }
        var col = {colName: iColumn.colName, cAndAs: ko.observableArray()};
        for (var i = 0, le = iColumn.cAndAs.length; i < le; i++) {
            self.addCondition(col, null, iColumn.cAndAs[i]);
        }

        if (push) {
            self.columns.push(col);
        }
        else
        {
            self.columns.unshift(col);
        }
    };
    
    self.addCondition = function(column, event, iCondition) {
        if (typeof iCondition === 'undefined') {
            iCondition = {
                condition: "",
                actions: []
            };
        }

        var cAndA = {condition: iCondition.condition, actions: ko.observableArray()};
        for (var i = 0, le = iCondition.actions.length; i < le; i++) {
            self.addAction(cAndA, null, iCondition.actions[i]);
        }

        column.cAndAs.push(cAndA);
    };
    
    self.addAction = function(cAndA, event, iAction) {
        if (typeof iAction === 'undefined') {
            iAction = {
                method: "",
                parameters: []
            };
        }

        var action = {method: iAction.method, parameters: ko.observableArray()};
        for (var i = 0, le = iAction.parameters.length; i < le; i++) {
            self.addParameter(action, null, iAction.parameters[i]);
        }

        cAndA.actions.push(action);
    };
    
    self.addParameter = function(action, event, iParameter) {
        //get static parameter infos
        var parameterObject = {parameterName: "", parameter: ko.observable("")};
        var infos = self.getInfosForMethod(action.method);
        if ((infos != null) && (action.parameters().length < infos.parameters.length)) {
            parameterObject = jQuery.extend(true, {}, infos.parameters[action.parameters().length]);
            if (typeof parameterObject.defaultValue !== 'undefined') {
                parameterObject.parameter = ko.observable(parameterObject.defaultValue);
            }
            else {
                parameterObject.parameter = ko.observable("");
            }
        }
        //set value if one was given
        if (typeof iParameter !== 'undefined') {
            parameterObject.parameter = ko.observable(iParameter);
        }

        action.parameters.push(ko.observable(parameterObject));
    };
    
    self.deleteItem = function(parent, collection) {
        var deletingItem = this; // $data, passed when binding from view
        if (collection == "parameters") {
            parent[collection].remove(function(item) {
                return item().parameter == deletingItem.parameter;
            });
        }
        else {
            parent[collection].remove(deletingItem);
        }
    };
    
    //wenn action ge�ndert wurde, schauen ob methode bekannt, falls ja, dann entsprechende parameterfelder setzen
    self.actionChanged = function() {
        var action = this;
        var infos = self.getInfosForMethod(action.method);
        if (infos == null)
            return;
        action.parameters.removeAll();
        for (var i = action.parameters().length, le = infos.parameters.length; i < le; i++) {
            self.addParameter(action, null);
        }
    };
    
    self.getInfosForMethod = function(method) {
        for (var i = 0, le = actionsData.length; i < le; i++) {
            if (actionsData[i].method == method) {
                return actionsData[i];
            }
        }
        return null;
    };
    
    self.editParameter = function(action) {
        var parameter = this; // $data, passed when binding from view
        var tmpParams = [];
        self.probableParams.removeAll();
        if (parameter.parameterName == "path") {
            tmpParams = self.paths();
            if (action.method == "newSubtree") {
                tmpParams = tmpParams.filter(self.isSubtreePath(true));
            }
            else {
                tmpParams = tmpParams.filter(self.isSubtreePath(false));
            }
        }
        else if (typeof parameter.exampleValues !== 'undefined') {
            tmpParams = parameter.exampleValues;
        }
        for (var i = 0, le = tmpParams.length; i < le; i++) {
            self.probableParams.push(tmpParams[i]);
        }

        self.currentParameter = parameter;
    };
    
    self.isSubtreePath = function(invert) {
        return function(path) {
            return invert == (path.indexOf("<<value>>") === -1);
        };
    };
    
    self.toJSON = function(returnTheString) {
        if (returnTheString != true)
            returnTheString = false;
        var tmp = ko.toJS(self.columns);
        var templateId = {templateId: "Export"};
        Utils.iterateNestedStructure(tmp, self.getTemplateId(templateId)); //ich bin ein computerhacker!
        Utils.iterateNestedStructure(tmp, self.cleanParams); //parameter objekte zu einem einzelnen string der nur den Wert des parameters enth�lt umwandeln
        if (returnTheString) {
            return JSON.stringify(tmp, null, 2);
        }
        else {
            self.myJSON(JSON.stringify(tmp, null, 2));
            $(".taJSON").focus();
            $(".taJSON").select();
        }
    };
    
    self.clearJSON = function() {
        self.myJSON(false);
    };
    
    self.getTemplateId = function(templateId) {
        return function(obj) {
            if ((obj.hasOwnProperty("parameterName")) && (obj.parameterName == "templateId")) {
                templateId.templateId = obj.parameter;
            }
        };
    };
    
    self.cleanParams = function(obj) {
        if (obj.hasOwnProperty("parameters")) {
            var myParameters = obj.parameters;
            for (var i = 0, le = obj.parameters.length; i < le; i++) {
                obj.parameters[i] = obj.parameters[i].parameter;
            }
        }
    };
    
    self.updateCommit = function() {
        var cardinalityCounts = {}; //IDictionary<string, int> cardinalityCounts = new Dictionary<string, int>(); //to support cardinality in templates, count how many instances of a path already exist
        var lastValues = {}; //IDictionary<string, PathAndValue> lastValues = new Dictionary<string, PathAndValue>(); //collumns can be marked to trigger action only on new value e.g. only new composition on changed ehrId
        var compositionToFinish = false;
        //make exactly the same instructions the c# component gets
        var SSISInstructions = JSON.parse(self.toJSON(true));
        var colnameToIndex = {};
        for (var i=0, le=self.exampleDataHeader().length; i<le; i++) {
            colnameToIndex[self.exampleDataHeader()[i]] = i;
        }

        var tmpStr = "";
        //iterate through all cells of exampleData
        for (var i = 0, le = self.exampleData().length; i < le; i++) {
            for (var j = 0, le2 = self.exampleData()[i].row.length; j < le2; j++) {
                //Anweisungen f�r die Spalte holen
                var col = SSISInstructions.filter(function(obj) {
                    return obj.colName == self.exampleDataHeader()[j];
                });
                if (col.length == 0) { //f�r die Spalte gibt es keine Anweisungen, also weiter zur n�chsten
                    continue;
                }
                else {
                    col = col[0];
                }
                //conditions And Actions f�r Spalte durchgehen und ausf�hren
                for (var k = 0, le3 = col.cAndAs.length; k < le3; k++) {
                    var actionsToTake = null;
                    if (col.cAndAs[k].condition == "changed")
                    {
                        if (valueChanged(col.colName, self.exampleData()[i].row[j]))
                        {
                            actionsToTake = col.cAndAs[k].actions;
                        }
                    }
                    else if (col.cAndAs[k].condition == "true")
                    {

                        actionsToTake = col.cAndAs[k].actions;
                    }
                    else if (col.cAndAs[k].condition.indexOf("=") > -1)
                    {
                        var components = col.cAndAs[k].condition.split('=');
                        if (self.exampleData()[i].row[colnameToIndex[components[0]]] == components[1])
                        {
                            actionsToTake = col.cAndAs[k].actions;
                        }
                    }
                    else
                    {
                        console.log("Invalid condition: " + cAndA.condition);
                        actionsToTake = null;
                    }

                    //run the actions
                    if (actionsToTake != null)
                    {
                        for (var l = 0, le4 = actionsToTake.length; l < le4; l++)
                        {
                            if (actionsToTake[l].method == "newComposition")
                            {
                                if (compositionToFinish == true) { //nach der ersten compostition aufh�ren
                                    finishComposition();
                                    self.myCommit(tmpStr);
                                    $(".exampleCommit").fadeIn(100).fadeOut(100).fadeIn(100);
                                    return;
                                }
                                var ehrId = self.exampleData()[i].row[colnameToIndex[actionsToTake[l].parameters[1]]];
                                startNewComposition(ehrId, actionsToTake[l].parameters[0], col.colName, actionsToTake[l].parameters[2], actionsToTake[l].parameters[3]);
                            }
                            else if (actionsToTake[l].method == "newSubtree")
                            {
                                newSubtree(actionsToTake[l].parameters[0], col.colName, self.exampleData()[i].row[colnameToIndex[col.colName]]);
                            }
                            else if (actionsToTake[l].method == "newEntry")
                            {
                                var castInstruction = "noCast";
                                if (actionsToTake[l].parameters.length > 1)
                                {
                                    castInstruction = actionsToTake[l].parameters[1];
                                }
                                var sourceColname = col.colName;
                                if (actionsToTake[l].parameters.length > 2)
                                {
                                    sourceColname = actionsToTake[l].parameters[2];
                                }
                                newEntry(actionsToTake[l].parameters[0], col.colName, self.exampleData()[i].row[colnameToIndex[col.colName]], self.exampleData()[i].row[colnameToIndex[sourceColname]], castInstruction);
                            }
                        }
                    }
                }
            }
        }
		finishComposition();
		self.myCommit(tmpStr);
        $(".exampleCommit").fadeIn(100).fadeOut(100).fadeIn(100);
        return;
        
        /*  Starts a new compositions in FLAT Format (adds the needed lines to StringBuilder sb) */
        function startNewComposition(ehrId, templateId, colName, language, territory)
        {
            var compositionStr = "[{ \n";
            compositionStr += "\"action\": \"CREATE\", \n";
            compositionStr += "\"ehrId\": \"9143c9ea-e4a8-4a77-b0eb-359b70d73a2b\", \n";
            compositionStr += "\"format\": \"FLAT\", \n";
            compositionStr += "\"templateId\": \"" + templateId + "\", \n";
            compositionStr += "\"composition\": { \n";
            compositionStr += "\"ctx/language\": \"" + language + "\", \n";
            compositionStr += "\"ctx/territory\": \"" + territory + "\", \n";
            
            //reset counter and lastValues 
            cardinalityCounts = {};
            lastValues = {};
            //add lastValue for ehrId
            lastValues[colName] = {path: "stub", value: ehrId};
            tmpStr += compositionStr;
            
            compositionToFinish = true;
        }

        /*  Finishes a compositions (adds the needed lines to StringBuilder sb) and if necessary also finishes contribution and posts the data*/
        function finishComposition()
        {
            tmpStr = tmpStr.substr(0, tmpStr.lastIndexOf(","));
            
            tmpStr += "}";
            tmpStr += "}";
            tmpStr += "]";

            if (tmpStr.indexOf(":-1") > -1)
            {
                alert("Testcommit contains -1 index. You probably want to change that by adding a subtree somewhere...");
            }
        }

        /* updates index-counts and lastValue-entries for subtree specified by given path - clear deprecated entries and update index and lastValue for current*/
        function newSubtree(path, colName, currentValue)
        {
            var index = getIndex(path); //remeber index for path
            deleteAllIndexesAndLastValuesContainingPath(path); //delete all index-entries and lastValues for descendants of given path, because it's a new subtree
            cardinalityCounts[path] = index + 1; //make entry for new index of this subtree
            lastValues[colName] = {path: path, value: currentValue}; //make entry for future checks for lastValue 
        }

        /*  Adds an entry for one attribute of composition in FLAT Format and appends it as new line to StringBuilder sb */
        function newEntry(path, colName, valueToRemember, valueToAdd, castInstruction)
        {
            //treat value if necessary
            if (castInstruction == "replaceDotWithComma")
            {
                valueToAdd = valueToAdd.Replace('.', ',');
            }
            //replace <<index>> tags with real counters for that path
            var entry = "";
            var subpaths = path.split("<<index>>");
            for (var i2 = 0, le = subpaths.length; i2 < le - 1; i2++) //for each subpath 
            {
                var subpath = subpaths[0];
                for (var j2 = 1; j2 <= i2; j2++)
                {
                    subpath += "<<index>>" + subpaths[j2]; //build subpath for index retrieval
                }
                entry += subpaths[i2] + getIndex(subpath); //and append new part of subpath and its index to the entry string
            }
            entry += subpaths[subpaths.length - 1]; //append last bit of path (or whole path if there where no <<index>>-markers)
            entry = entry.replace("<<value>>", valueToAdd) + ','; //replace <<value>> tag with real value
            tmpStr += entry + "\n";
            lastValues[colName] = {path: path, value: valueToRemember}; //make entry for future checks for lastValue 
        }

        /*  returns index(int) for given path - returns index -1 if no index entry in cardinalityCounts exists*/
        function getIndex(path)
        {
            if (!(path in cardinalityCounts))
            {
                return -1;
            }
            else {
                return cardinalityCounts[path];
            }
        }

        /*  returns if given value for given path is a changed value
         @return bool  true if dict lastValues hold no or different value for key*/
        function valueChanged(col, value)
        {
            return (!(col in lastValues) || (lastValues[col].value != value));
        }

        /* Just read the methods name */
        function deleteAllIndexesAndLastValuesContainingPath(path)
        {
            var removals = [];
            for (var key in cardinalityCounts)
            {
                if (key.indexOf(path) > -1)
                {
                    removals.push(key);
                }
            }
            for (var i = 0, le = removals.length; i < le; i++)
            {
                //cardinalityCounts.splice(cardinalityCounts.indexOf(key), 1);
                delete cardinalityCounts[key];
            }

            removals = [];
            for (var key in lastValues)
            {
                if (key.indexOf(path) > -1)
                {
                    removals.push(key);
                }
            }
            for (var i = 0, le = removals.length; i < le; i++)
            {
                //lastValues.splice(lastValues.indexOf(key), 1);
                delete lastValues[key];
            }
        }
    };
    
    self.testCommit = function() {
        $.ajaxSetup({
            headers: {'Authorization': "Basic " + btoa(iVM.uname() + ":" + iVM.pw()), 'Content-Type': "application/json"}
        });
        $.post("http://172.24.8.56:8081/rest/v1/composition/contribution", self.myCommit())
                .done(function(data) {
                    alert(JSON.stringify(data, null, 2));
                })
                .fail(function(jqxhr, textStatus, error) {
					console.log(jqxhr);
                    console.log("Don't worry, if you didn't expect that loading from your Think! instance would work. (e.g. if you are using this locally)");
                });
    };
    
    self.changeParameterValue = function() {
        self.currentParameter.parameter(this);
        self.probableParams.removeAll();
    };
    
    self.clearProbableParams = function() {
        self.probableParams.removeAll();
    };
}
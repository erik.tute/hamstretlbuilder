conditionsData = [
    "true",
	"changed",
    "columnX=valueY"
];

actionsData = [
    {
        method: "newComposition",
        parameters: [
            {parameterName: "templateId"}, 
            {parameterName: "ehrId_column"}, 
            {parameterName: "ctx/language", defaultValue:"de", exampleValues:["de", "en"]}, 
            {parameterName: "ctx/territory", defaultValue:"DE", exampleValues:["DE", "EN"]}]
    },
    {
        method: "newSubtree",
        parameters: [{parameterName: "path"}]
    },
    {
        method: "newEntry",
        parameters: [{parameterName: "path"}, {parameterName: "cast", exampleValues:["noCast", "replaceDotWithComma"]}, {parameterName: "sourceColumn"}]
    }
];
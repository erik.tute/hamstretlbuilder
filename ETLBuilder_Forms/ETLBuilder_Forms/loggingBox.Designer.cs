﻿namespace HaMSTR_ETLBuilder_Forms
{
    partial class loggingBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(loggingBox));
            this.txtLogInfo = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txtLogInfo
            // 
            this.txtLogInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogInfo.Location = new System.Drawing.Point(0, 0);
            this.txtLogInfo.Name = "txtLogInfo";
            this.txtLogInfo.ReadOnly = true;
            this.txtLogInfo.Size = new System.Drawing.Size(627, 415);
            this.txtLogInfo.TabIndex = 0;
            this.txtLogInfo.Text = "";
            // 
            // loggingBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 415);
            this.Controls.Add(this.txtLogInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "loggingBox";
            this.Text = "Log";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtLogInfo;
    }
}
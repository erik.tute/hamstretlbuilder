﻿#region namespaces
using Devart.Data.PostgreSql;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;
#endregion

namespace HaMSTR_ETLBuilder_Forms
{
    #region class for assembling information to pass to mapping component in browser (the one in 3rd tab)
    public class EtlBuilderInfo
    {
        public string columnInstructionsJSON { get; set; }
        public string exampleDataJSON { get; set; }
        public string templatePathesJSON { get; set; }
        public string templateOPTJSON { get; set; }
        public string templateCompositionPathes { get; set; }      

        public EtlBuilderInfo(DataTable datatable, string instructionsString, string templateId, RepoHandle repoHandle)
        {
            this.setExampleDataFromDataTable(datatable);
            this.columnInstructionsJSON = instructionsString;
            this.getPathsAndOPT(templateId, repoHandle);
            
        }

        /* Pfade und OPT für übergebene templateId von Think-Plattform abfragen und setzen. */
        public void getPathsAndOPT(string templateId, RepoHandle repoHandle)
        {
            try
            {
                this.templateOPTJSON = repoHandle.httpRequest("template/" + templateId + "/", false, null, null);
                this.templatePathesJSON = repoHandle.httpRequest("template/" + templateId + "/example", false, null, null);
                this.templateCompositionPathes = "hallo"; 
            }
            catch (Exception ex)
            {
                MessageBox.Show("EtlBuilderInfo -" + " failed while trying to get the templates paths and OPT: " + ex.Message);
            }
        }

        public void setExampleDataFromDataTable(DataTable dt)
        {
            this.exampleDataJSON = "[";
            int c = 0;
            foreach (DataColumn col in dt.Columns)
            {
                if (c == 0)
                {
                    this.exampleDataJSON += "\""+ col.ColumnName +"\"";
                }
                else
                {
                    this.exampleDataJSON += ",\"" + col.ColumnName + "\"";
                }
                c++;
            }
            this.exampleDataJSON += "]";
        }
    }
    #endregion

    #region class dbConnect
    // Class with connection and executeQuery Methods
    public class dbConnect
    {
        public DataSet ds = new DataSet();
        public DataTable dt = new DataTable();
        public bool pgIsOpen = false;
        public bool sqlServerIsOpen = false;

        public string tableAsJSON { get; set; }

        public PgSqlConnection pgConnection { get; set; }

        public SqlConnection sqlServerConnection { get; set; }

        //bool is true if checkBoxPostgres is checked
        public void openConnection(string connectionString, bool postgres)
        {
            try
            {
                if (postgres)
                {
                    // Connection via dotConnect for PostgreSQL
                    pgConnection = new PgSqlConnection(connectionString);
                    pgConnection.Open();
                    pgIsOpen = true;
                }
                else
                {
                    sqlServerConnection = new SqlConnection(connectionString);
                    sqlServerConnection.Open();
                    sqlServerIsOpen = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No connection: " + ex.Message + " | Try again!");
            }
        }

        public void closeConnection()
        {
            if (pgIsOpen) { pgConnection.Close(); pgIsOpen = false; }
            else if (sqlServerIsOpen) { sqlServerConnection.Close(); sqlServerIsOpen = false; }
        }

        public void executeQuery(string sqlQuery)
        {
            try
            {
                if (pgIsOpen)
                {
                    dt.Clear();
                    PgSqlCommand command = new PgSqlCommand(sqlQuery, pgConnection);
                    PgSqlDataAdapter da = new PgSqlDataAdapter();
                    da.SelectCommand = command;
                    da.Fill(dt);
                }
                else if (sqlServerIsOpen)
                {
                    dt.Clear();
                    SqlCommand command = new SqlCommand(sqlQuery, sqlServerConnection);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = command;
                    da.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failure occured: " + ex.Message);
            }
        }

        public DataTable previewDataTable(DataTable dt, int numOfRows)
        {
            try
            {
                DataTable dtn = dt.Clone();
                if (numOfRows > dt.Rows.Count)
                {
                    numOfRows = dt.Rows.Count;
                }

                for (int i = 0; i < numOfRows; i++)
                {
                    dtn.ImportRow(dt.Rows[i]);
                }

                return dtn;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return null;
            }

        }
    }
    #endregion

    #region classes for handling instructions
    public class InstructionsHandler
    {
        public IContributionsHandler contributionsHandler { get; set; }

        public ColumnInstructions[] columnInstructions { get; set; }
        public IDictionary<string, int> columnInstructionsKeyToIndex { get; set; }
        private Dictionary<string, string> lastValues { get; set; }
        public string instructionsAsJSON { get; set; }

        // Alt: public InstructionsHandler(string instructionsString, Microsoft.SqlServer.Dts.Tasks.ScriptTask.ScriptObjectModel Dts, IContributionsHandler contributionsHandler)
        public InstructionsHandler(string instructionsString, IContributionsHandler contributionsHandler)
        {
            //this.Dts = Dts;
            this.contributionsHandler = contributionsHandler;

            this.lastValues = new Dictionary<string, string>();
            /* deserialze instructions (given as JSON-String) on what to do for which columnName and values */
            this.columnInstructionsKeyToIndex = new Dictionary<string, int>();
            try
            {
                if (instructionsString == "")
                {
                    instructionsString = "[]";
                }
                this.instructionsAsJSON = instructionsString.Replace("\n", ""); //remove \n
                this.columnInstructions = new JavaScriptSerializer().Deserialize<ColumnInstructions[]>(instructionsString);
                
                //fill lookup dictionary for indexes of columns in columnInstructions - so later one can lookup instructions by columnName without iterating
                for (int i = 0, le = this.columnInstructions.Length; i < le; i++)
                {
                    this.columnInstructionsKeyToIndex[this.columnInstructions[i].colName] = i;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "InstructionsHandler");
            }
        }

        public void runOn(DataRow row, DataColumn col)
        {
            if ((row == null) && (col == null)) //just to finish last contribution
            {
                this.contributionsHandler.newContribution(null, null, null, null, null, null, null);
                return;
            }

            if (this.columnInstructionsKeyToIndex.ContainsKey(col.ColumnName)) //if collumn has no instruction it will simply be ignored
            {
                //iterate through all conditions for current collumn
                foreach (CAndA cAndA in this.columnInstructions[this.columnInstructionsKeyToIndex[col.ColumnName]].cAndAs)
                {
                    this.runActions(col.ColumnName, row, cAndA);//outsourced that just to fit into my mind
                }
            }
            this.lastValues[col.ColumnName] = row[col.ColumnName].ToString();
        }

        /* runs actions depending on the current input value and the given instructions */
        private void runActions(string columnName, DataRow row, CAndA cAndA)
        {
            Action[] actionsToTake = null;
            //conditions can be concatenated by || because this is handy sometimes
            string[] separator = new string[] { "||" };
            string[] tmpConditions = cAndA.condition.Split(separator, StringSplitOptions.None);
            int testvalue = 0; 
            foreach (string s in tmpConditions)
            {
               
                if (s == "changed")
                {
                    if ((row[columnName].ToString() != "") && ((!this.lastValues.ContainsKey(columnName)) || (this.lastValues[columnName] != row[columnName].ToString())))
                    {
                        actionsToTake = cAndA.actions;
                        break;
                    }
                }

                

                else if (s == "true")
                {
                    actionsToTake = cAndA.actions;
                    break;
                }
                


                else if (s.Contains("="))
                {
                    string[] components = s.Split('=');
                    if (row[components[0]].ToString() == components[1])
                        
                    {
                        actionsToTake = cAndA.actions;
                        break;
                    }

                   
                    }

                //changed & columnX=valueY
                else if (s.Contains("%"))
                {
                    string[] components = s.Split('%');
                    if ((row[columnName].ToString() != "") && ((!this.lastValues.ContainsKey(columnName)) || (this.lastValues[columnName] != row[columnName].ToString())))

                      

                    {
                        if (row[components[0]].ToString() == components[1]) {
                            actionsToTake = cAndA.actions;
                            break;
                        }
                    }


                }

            


                else if (s.Contains("§"))
                {
                    string[] components = s.Split('§');
                    if (row[components[0]].ToString().StartsWith(components[1]))

                    {
                        actionsToTake = cAndA.actions;
                        break;
                    }


                }
                //not empty
                else if (s.Contains("1"))
                {
                    string[] components = s.Split('1');
                    if (row[components[0]].ToString() != "")


                    {
                        actionsToTake = cAndA.actions;
                        break;
                    }


                }
                //columnX!=valueY
                else if (s.Contains("!"))
                {
                    string[] components = s.Split('!');


                  
                        if (row[components[0]].ToString() != components[1])


                        {
                            actionsToTake = cAndA.actions;
                            break;
                        
                    }

                }
                // empty
                else if (s.Contains("0"))
                {
                    string[] components = s.Split('0');
                    if (row[components[0]].ToString() == "")


                    {
                        actionsToTake = cAndA.actions;
                        break;
                    }
                }
             /*   else if (s.Contains("3"))
                {

                    string[] components = s.Split('3');


                    if ((row[components[0]].ToString() != "") && ((!this.lastValues.ContainsKey(components[0])) || (this.lastValues[components[0]] == row[components[0]].ToString())))






                    {
                        actionsToTake = cAndA.actions;
                        break;
                    }
                }
                */
               


                else
                {
                    actionsToTake = null;
                    MessageBox.Show("INSTRUCTIONS " + "Invalid condition: " + cAndA.condition, "InstructionsHandler");
                }
            }

            //run the actions
            if (actionsToTake != null)
            {
                foreach (Action a in actionsToTake)
                {
                    if (a.method == "newContribution")
                    {
                        this.lastValues = new Dictionary<string, string>();
                        string subjectId = row[a.parameters[2]].ToString();
                        this.contributionsHandler.newContribution(a.parameters[0], subjectId, a.parameters[1], columnName, a.parameters[3], a.parameters[4], this.instructionsAsJSON);
                    }
                    else if (a.method == "newSubtree")
                    {
                        this.contributionsHandler.newSubtree(a.parameters[0]);
                    }
                    else if (a.method == "newEntry")
                    {
                        string castInstruction = "noCast";
                        if (a.parameters.Length > 1)
                        {
                            castInstruction = a.parameters[1];
                        }
                        string val = row[columnName].ToString();
                        if ((a.parameters.Length > 2) && (a.parameters[2] != ""))
                        {
                            val = "";
                            String[] sourceColumns = a.parameters[2].Split(new string[] { "," }, StringSplitOptions.None);
                            for (int i = 0; i < sourceColumns.Length; i++)
                            {
                                string sep = " ";
                                if (i==0) {
                                    sep = "";
                                }
                                val += sep + row[sourceColumns[i]].ToString();
                            }
                        }
                        this.contributionsHandler.newEntry(a.parameters[0], val, castInstruction);
                    }
                }
            }
        }
    }

    public class ColumnInstructions
    {
        public string colName { get; set; }
        public CAndA[] cAndAs { get; set; }
    }

    public class CAndA //conditions And Actions
    {
        public string condition { get; set; }
        public Action[] actions { get; set; }
    }

    public class Action
    {
        public string method { get; set; }
        public string[] parameters { get; set; }
    }
    #endregion

    #region interface and classes for building contributions

    public interface IContributionsHandler
    {
        /*  Starts a new composition */
        void newContribution(string action, string subjectId, string templateId, string colName, string language, string territory, string instructionsAsJSON);
        /* updates index-counts for subtree specified by given path - clear deprecated entries and update index and lastValue for current*/
        void newSubtree(string path);
        /*  Adds an entry for one attribute of composition in FLAT Format and appends it as new line to StringBuilder sb */
        void newEntry(string path, string valueToAdd, string castInstruction);
    }

    public class ContributionsHandler : IContributionsHandler
    {
        private string curDir = Directory.GetCurrentDirectory();
        public Object contributionsTarget { get; set; }
        public int contributionsCounter { get; set; }
        public int entryCounter { get; set; }
        private Contribution contributionUnderConstruction { get; set; }
        public IDictionary<string, string> subjectIdsToEhrIds { get; set; }
        public ConcurrentDictionary<Contribution, bool> waitingForCommit { get; set; } 
        public bool checkWaitingContributionsRunning { get; set; }
        public IDictionary<string, string> atCodes { get; set; }
        public IDictionary<string, string> stringsToReplace { get; set; }
        public string namespaceTag { get; set; }
        public loggingBox log { get; set; }
        public string defaultEHRId { get; set; }
        public bool skipInstructionsJSON { get; set; }

        public int maxOpenRequests = 20;

        public ContributionsHandler(Object contributionsTarget)
        {
            this.contributionsCounter = 0;
            this.entryCounter = 0;
            this.contributionUnderConstruction = null;
            this.contributionsTarget = contributionsTarget;
            this.subjectIdsToEhrIds = new Dictionary<string, string>();
            this.waitingForCommit = new ConcurrentDictionary<Contribution, bool>();
            this.checkWaitingContributionsRunning = false;
            this.defaultEHRId = null;
            this.skipInstructionsJSON = true;
            if (this.contributionsTarget.GetType() == typeof(RepoHandle))
            {
                RepoHandle repoHandle = (RepoHandle)contributionsTarget;
                this.log = repoHandle.log;
            }
        }

        void IContributionsHandler.newContribution(string action, string subjectId, string templateId, string colName, string language, string territory, string instructionsAsJSON)
        {
            //if necessary, commit last contribution
            if (this.contributionUnderConstruction != null)
            {
                this.waitingForCommit[this.contributionUnderConstruction] = true;
            }
            //if this method was just invoked to finish last contribution
            if (subjectId == null)
            {
                this.contributionUnderConstruction = null;
                this.checkWaitingContributions();
                return;
            }
            //if not, trigger ehrId detection
            else
            {
                if (!subjectIdsToEhrIds.ContainsKey(subjectId))
                { 
                   subjectIdsToEhrIds[subjectId] = "requestEHRId"; 
                }
            }
            this.checkWaitingContributions();
            //create contribution
            this.contributionsCounter++;
            if (skipInstructionsJSON) {
                instructionsAsJSON = "justaplaceholder";
            }
            this.contributionUnderConstruction = new Contribution(action, subjectId, templateId, language, territory, instructionsAsJSON);
        }

        void IContributionsHandler.newSubtree(string path)
        {
            this.contributionUnderConstruction.newSubtree(path);
        }

        void IContributionsHandler.newEntry(string path, string valueToAdd, string castInstruction)
        {
            object finalValue = valueToAdd;
 
            //treat value if necessary
            String[] castInstructions = castInstruction.Split(new string[] { "," }, StringSplitOptions.None);
            for (int i = 0; i < castInstructions.Length; i++)
            {
                if (castInstructions[i] == "replaceDotWithComma")
                {
                    finalValue = finalValue.ToString().Replace('.', ',');
                }
                if (castInstructions[i] == "replaceCommaWithDot")
                {
                    finalValue = finalValue.ToString().Replace(',', '.');
                }
                if (castInstructions[i] == "replace")
                {
                    finalValue = finalValue.ToString().Replace(castInstructions[i + 1], castInstructions[i + 2]);
                }
                if (castInstructions[i] == "constantValue")
                {
                    finalValue = castInstructions[i + 1];
                }
                if (castInstructions[i] == "insertValueIntoConstant")
                {
                    finalValue = castInstructions[i + 1].Replace("<TAG_FOR_VALUE>", finalValue.ToString());
                }
                if (castInstructions[i] == "split")
                { //split,:,0
                    String[] tmpComponents = finalValue.ToString().Split(new string[] { castInstructions[i + 1] }, StringSplitOptions.None);
                    int index = Int32.Parse(castInstructions[i + 2]);
                    if (tmpComponents.Length > index)
                    {
                        finalValue = tmpComponents[index];
                    }
                    else
                    {
                        MessageBox.Show("Split string " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Given index out of range: " + finalValue + ", " + castInstruction, "ContributionsHandler.newEntry");
                    }
                }
                if (castInstructions[i] == "asInt")
                {
                    try
                    {
                        finalValue = Int32.Parse(finalValue.ToString());
                    }
                    catch
                    {
                        if (catchToNullFlavour(finalValue))
                        {
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                if (castInstructions[i] == "asDouble")
                {
                    try {
                        finalValue = Convert.ToDouble(finalValue.ToString());
                    }
                    catch
                    {
                        if (catchToNullFlavour(finalValue))
                        {
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                if (castInstructions[i] == "catchNullFlavours")
                {
                    if (catchToNullFlavour(finalValue))
                    {
                        return;
                    }
                }
                if (Array.IndexOf(castInstructions, "asISO8601") > -1)
                {
                    try
                    {
                        finalValue = Convert.ToDateTime(valueToAdd).ToString("s", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        if (catchToNullFlavour(finalValue))
                        {
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                if (castInstructions[i] == "noEmptyEntry")
                {
                    if (finalValue.ToString() == "")
                    {
                        return;
                    }
                }
                if (castInstructions[i] == "castUsingDictionary")
                {
                    if (this.stringsToReplace.ContainsKey(finalValue.ToString()))
                    {
                        finalValue = this.stringsToReplace[finalValue.ToString()];
                    }
                }
                if (castInstructions[i] == "castIntoAtCode")
                {
                    if (this.atCodes.ContainsKey(finalValue.ToString()))
                    {
                        finalValue = this.atCodes[finalValue.ToString()];
                    }
                    else
                    {
                        MessageBox.Show("Cast At Codes " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " No atCode given for value: " + valueToAdd, "ContributionsHandler.newEntry");
                    }
                }
            }
            this.entryCounter++;
            this.contributionUnderConstruction.newEntry(path, finalValue);
            

            bool catchToNullFlavour(object val) {
                string[] unknownStrings = { "", "-", "NA", "ND", "UK", "NK", "UN", "UNKNOWN", "UNK" };
                string[] notApplicableStrings = { "NOT APPLICABLE" };
                if (Array.IndexOf(unknownStrings, val.ToString().ToUpper().Trim()) >-1)
                {
                    this.entryCounter++;
                    this.entryCounter++;
                    this.contributionUnderConstruction.newEntry(path + "/_null_flavour|value", "unknown");
                    this.contributionUnderConstruction.newEntry(path + "/_null_flavour|code", "253");
                    return true;
                }
                else if (Array.IndexOf(notApplicableStrings, val.ToString().ToUpper().Trim()) > -1)
                {
                    this.entryCounter++;
                    this.entryCounter++;
                    this.contributionUnderConstruction.newEntry(path + "/_null_flavour|value", "not applicable");
                    this.contributionUnderConstruction.newEntry(path + "/_null_flavour|code", "273");
                    return true;
                }
                return false;
            }
        }

        public void assignEhrId(string subjectId, string ehrId)
        {
            if ((this.subjectIdsToEhrIds.ContainsKey(subjectId)) && ((this.subjectIdsToEhrIds[subjectId] == "requested") || (this.subjectIdsToEhrIds[subjectId] == "requestEHRId")))
            {
                this.subjectIdsToEhrIds[subjectId] = ehrId;
                this.checkWaitingContributions();
            }
            else
            {
                this.log.inputText = "ServerResponseHandling " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Unexpected dictionary-state for subjectId: " + subjectId;
            }
        }

        public void checkWaitingContributions()
        {
            //sicherstellen das checkWaitingContributions nur einmal läuft
            if (this.checkWaitingContributionsRunning)
            {
                this.log.inputText = "@checkWaitingContributions: " + "i am not supposed to be running in more than one thread!";
                return;
            }
            else
            {
                this.checkWaitingContributionsRunning = true;
            }

            List<Contribution> toRemove = new List<Contribution>();
            foreach (KeyValuePair<Contribution, bool> con in this.waitingForCommit)
            {
                if (contributionsTarget.GetType() == typeof(RepoHandle))
                {
                    RepoHandle repoHandle = (RepoHandle)contributionsTarget;

                    //um die Anzahl an requests im Zaum zu halten
                    if (repoHandle.openRequests.Count > maxOpenRequests)
                    {
                        break;
                    }
                    else if (subjectIdsToEhrIds[con.Key.subjectId] == "requestEHRId") //ehrId has to be set
                    {
                        subjectIdsToEhrIds[con.Key.subjectId] = "requested"; 
                        repoHandle.httpRequest("ehr?subjectId=" + con.Key.subjectId + "&subjectNamespace=" + namespaceTag, true, "", "application/json");
                    }
                    else if (subjectIdsToEhrIds[con.Key.subjectId] != "requested") //ehrId is already set. Thus, send composition
                    {
                        con.Key.ehrId = subjectIdsToEhrIds[con.Key.subjectId];
                        repoHandle.httpRequest("composition/contribution/", true, "[" + con.Key.asJSON() + "]", "application/json");       
                        toRemove.Add(con.Key); //remember which contributions to remove from list
                    }
                }
                else
                {
                    RichTextBox exampleContributionsTextBox = (RichTextBox)contributionsTarget;
                    con.Key.ehrId = this.defaultEHRId;
                    exampleContributionsTextBox.AppendText("[" + JsonConvert.SerializeObject(con.Key, Formatting.Indented) + "]" + Environment.NewLine);
                    toRemove.Add(con.Key);
                }
            }
            //remove contributions from waiting list
            foreach (Contribution con in toRemove)
            {
                bool useless; this.waitingForCommit.TryRemove(con, out useless);
                if (!useless) this.log.inputText = "ContributionsHandler " + DateTime.Now.ToString("dd / MM / yyyy h: mm: ss tt") + "Removing contribution(type 3) failed";
            }

            this.checkWaitingContributionsRunning = false;
        }

        public void setAtCodes(ColumnInstructions atCodesAsInstructions)
        {
            this.atCodes = new Dictionary<string, string>();
            if (atCodesAsInstructions.cAndAs.Length > 0)
            {
                foreach (Action a in atCodesAsInstructions.cAndAs[0].actions)
                {
                    this.atCodes[a.parameters[0]] = a.method;

                }
            }
        }

        public void setStringsToReplace(ColumnInstructions stringsToReplaceAsInstructions)
        {
            this.stringsToReplace = new Dictionary<string, string>();
            if (stringsToReplaceAsInstructions.cAndAs.Length > 0)
            {
                foreach (Action a in stringsToReplaceAsInstructions.cAndAs[0].actions)
                {
                    this.stringsToReplace[a.method] = a.parameters[0];
                }
            }
        }
    }

    public class Contribution
    {
        [ScriptIgnore]
        private IDictionary<string, int> cardinalityCounts { get; set; } //to support cardinality in templates, count how many instances of a path already exist
        [ScriptIgnore]
        public string subjectId { get; set; }
        public string action { get; set; }
        public string ehrId { get; set; }
        public string format { get; set; }
        public string templateId { get; set; }
        
        public IDictionary<string, object> composition { get; set; }

        public Contribution(string action, string subjectId, string templateId, string language, string territory, string instructionsAsJSON)
        {
            this.action = action;
            this.ehrId = null;
            this.format = "FLAT";
            this.templateId = templateId;
            this.composition = new Dictionary<string, object>();
            this.newEntry("ctx/language", language);
            this.newEntry("ctx/territory", territory);
            this.newEntry("ctx/composer_name", "HaMSTR");
            

           


            this.subjectId = subjectId;
            this.cardinalityCounts = new Dictionary<string, int>(); //reset cardinality counters
        }

        public string asJSON()
        {
            var serializer = new JavaScriptSerializer() { MaxJsonLength = 86753090 };
            return serializer.Serialize(this); 
        }

        /* updates index-counts for subtree specified by given path - clear deprecated entries and update index and lastValue for current*/
        public void newSubtree(String path)
        {
            this.deleteAllIndexesContainingPath(path); //delete all index-entries descendants of given path, because it's a new subtree
            this.cardinalityCounts[path] = this.getIndex(path) + 1; //make entry for new index of this subtree
        }

        public void newEntry(string path, object valueToAdd)
        {
            const string counterMarkup = "<<index>>";
            //replace <<index>> tags with real counters for that path
            String pathEntry = "";
            String[] subpaths = path.Split(new string[] { counterMarkup }, StringSplitOptions.None); //split by <<index>>
            for (int i = 0, le = subpaths.Count(); i < le - 1; i++) //for each subpath 
            {
                string subpath = subpaths[0];
                for (int j = 1; j <= i; j++)
                {
                    subpath += counterMarkup + subpaths[j]; //build subpath for index retrieval
                }
                pathEntry += subpaths[i] + this.getIndex(subpath); //and append new part of subpath and its index to the entry string
            }
            pathEntry += subpaths[subpaths.Count() - 1]; //append last bit of path (or whole path if there where no <<index>>-markers) 
            this.composition[pathEntry] = valueToAdd;
        }

        #region helping methods
        /*  returns index(int) for given path - returns index -1 if no index entry in cardinalityCounts exists*/
        private int getIndex(String path)
        {
            if (!this.cardinalityCounts.ContainsKey(path))
            {
                return -1;
            }
            else
            {
                return this.cardinalityCounts[path];
            }
        }

        /*  deletes all indexes containing the given path */
        private void deleteAllIndexesContainingPath(String path)
        {
            List<string> removals = new List<string>();
            foreach (KeyValuePair<string, int> kv in this.cardinalityCounts)
            {
                //if (kv.Key.Contains(path))
                if ((kv.Key.Contains(path)) && (kv.Key != path))
                {
                    removals.Add(kv.Key);
                }
            }
            foreach (string key in removals)
            {
                this.cardinalityCounts.Remove(key);
            }
        }
        #endregion
    }
    #endregion

    #region interface and class implementation for handling of server-responses
    public interface IServerResponseHandling
    {
        /* handling of positive server responses for async-get-requests */
        void handleGetResponse(string resultString, string request_address);
        /* handling of positive server responses for async-post-requests */
        void handlePostResponse(string response, string requestBody);
        /* handling of negative server responses for async-post-requests */
        void handlePostError(string response, string requestBody);
    }

    public class ServerResponseHandling : IServerResponseHandling
    {
        private string curDir = Directory.GetCurrentDirectory();
        public string sqlStatement { get; set; }
        public PgSqlConnection errorTableConnection { get; set; }
        public int invalidContributionsCounter { get; set; }
        public int storedInvalidCounter { get; set; }
        public int modifiedCounter { get; set; }
        public int validCounter { get; set; }
        public DateTime lastLog { get; set; }
        public int numberOfRequestStats { get; set; }
        public RepoHandle repoHandle { get; set; }
        public ContributionsHandler contributionsHandler { get; set; }
        public ConcurrentDictionary<string, string[]> invalidCompositionsPaths { get; set; }
        //folgende sind hinzugefügt, damit diese im Cache gehalten werden, bis sie vom User gespeichert werden
        public ConcurrentDictionary<string, string> tmpProblematicRequests { get; set; }
        public const string suppressedBadEntriesTag = "suppressedBadEntries"; //Tag to add to feeder_audit version_id to indicate that bad entries where removed
        public const string suppressedBadEntriesPathTag = "report/_feeder_audit/originating_system_audit"; //path where to add this info
        public const double worthCleansingLevel = 0.1;//level where a composition is deemed to be worth to get cleaned from bad entries and resubmitted
        public const string versionTag = "2017-05-09-001"; //version of this component

        public ServerResponseHandling(int numberOfRequestStats)
        {
            this.storedInvalidCounter = 0;
            this.modifiedCounter = 0;
            this.validCounter = 0;
            this.numberOfRequestStats = numberOfRequestStats;
            this.lastLog = DateTime.Now;
           
            this.invalidCompositionsPaths = new ConcurrentDictionary<string, string[]>();

            this.tmpProblematicRequests = new ConcurrentDictionary<string, string>();
        }

        /* handling of positive server responses for async-post-requests */
        void IServerResponseHandling.handlePostResponse(string response, string requestBody)
        {
            if (response.IndexOf("/ehrbase/")>-1) //repo is ehrbase
            {
                if (requestBody.IndexOf("{\"namespace\": \"")>-1) //create ehr response
                {
                    string subjectId = requestBody.Replace("\"HIER_OBJECT_ID\",\"value\": \"", "§").Split('§')[1]; //extract subjectId from requestBody
                    subjectId = subjectId.Replace("\"}}},\"archetype_node_id\":", "§").Split('§')[0];
                    contributionsHandler.assignEhrId(subjectId, response.Replace("/ehr/", "§").Split('§')[1]);
                }
                else //create contribution response
                {
                    this.validCounter++;
                }   
            }
            else //repo is think/better
            {
                Dictionary<string, object> responseDict = new JavaScriptSerializer().DeserializeObject(response) as Dictionary<string, object>;
                if (!responseDict.ContainsKey("commitData"))
                { //is no response for contribution-request, so its a response for ehr-creation-request
                    if (responseDict.ContainsKey("ehrId"))
                    {
                        string subjectId = this.substrBetween("subjectId=", "&", requestBody); //extract subjectId from requestBody which isn't a body but a URL-string in that case
                        contributionsHandler.assignEhrId(subjectId, responseDict["ehrId"].ToString());
                    }
                    else
                    {
                        this.contributionsHandler.log.inputText = DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Unexpected response: " + response + " on request: " + requestBody;
                    }
                }
                else
                {
                    this.validCounter++;
                }
            }

            this.maybeLog();
        }

        /* returns substring from sourceString between delimiterStart and delimiterEnd*/
        private string substrBetween(string delimiterStart, string delimiterEnd, string sourceString)
        {
            int pFrom = sourceString.IndexOf(delimiterStart) + delimiterStart.Length;
            string tmpStr = sourceString.Substring(pFrom);
            int pTo = tmpStr.IndexOf(delimiterEnd);
            return tmpStr.Substring(0, pTo);
        }

        /* handling of negative server responses for async-post-requests */
        void IServerResponseHandling.handlePostError(string response, string requestBody)
        {
            #region Beispiele negative response body
            /* 
            //ehr-creation negative response body
            {
              "status": 400,
              "code": "EHR-2124",
              "userMessage": "The EHR for this subject already exists.",
              "developerMessage": "The EHR with this subject ID and namespace combination already exists.",
              "exceptionMessage": "EHR with this subject ID and subject namespace already exists!",
              "moreInfo": "https://confluence.ehrvendor.com/display/ehrrest/EHR-2124",
              "requestHref": "http://172.24.8.56:8081/rest/v1/ehr?subjectId=1&subjectNamespace=Test"
            }

            //contribution negative response bodys
            { //1
              "status": 400,
              "code": "COMP-1041",
              "userMessage": "Could not parse submitted composition.",
              "developerMessage": "Cannot parse the submitted composition JSON. Is it in the right format?",
              "exceptionMessage": "Expected a JSON array!",
              "moreInfo": "https://confluence.ehrvendor.com/display/ehrrest/COMP-1041",
              "requestHref": "http://172.24.8.56:8081/rest/v1/composition/contribution"
            }
            { //2
              "status": 400,
              "code": "COMP-1081",
              "userMessage": "Composition validation failed (there are missing or invalid values).",
              "developerMessage": "Composition validation failed (there are missing or invalid values).",
              "exceptionMessage": "There were errors in validation: Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:41.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #3']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #10']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #15']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #15']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #18']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #18']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #18']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #18']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #20']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #30']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #30']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #30']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #30']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #32']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #33']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #33']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #33']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #33']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #35']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #38']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #38']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #38']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #38']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #40']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #40']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #40']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #40']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #45']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #45']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #49']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #52']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #52']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #52']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #52']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #54']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #57']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #57']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #57']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #57']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:41.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #59']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #59']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #59']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #59']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #61']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #65']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #68']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:41.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #72']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #72']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #72']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #72']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #81']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #81']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #82']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #83']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #83']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #83']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #83']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #85']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #86']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #90']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #91']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #93']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #97']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #100']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #100']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #100']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #100']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #101']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #112']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #112']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #112']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #112']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #115']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #117']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #119']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #120']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #122']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #125']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #128']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #128']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #128']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #128']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #132']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #137']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #137']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #137']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #137']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #139']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #146']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #146']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #146']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #146']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #148']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #153']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #155']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #155']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #155']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #155']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #158']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #158']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #158']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #158']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #160']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #165']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #169']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #173']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #173']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #173']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #173']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #176']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #180']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #180']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #180']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #180']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #181']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #188']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #193']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #193']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #193']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #193']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #196']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #201']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #201']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #201']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #201']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #205']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #210']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #211']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #211']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #211']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #211']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #213']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #214']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #215']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #218']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #220']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #220']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #220']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #220']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #222']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #222']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #222']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #222']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #227']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #230']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #230']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #230']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #230']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #234']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #237']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #239']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #240']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #242']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #242']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #242']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #242']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #243']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #245']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #245']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #245']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #245']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #246']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #246']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #246']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #246']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #247']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #248']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #248']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #248']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #248']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #251']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #251']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #251']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #251']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #257']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #260']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #260']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #260']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #260']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #262']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #265']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #266']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #266']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #266']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #266']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #269']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #277']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #281']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #281']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #281']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #281']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #282']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #282']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #282']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #282']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #285']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #289']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #293']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #293']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #293']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #293']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #296']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #300']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #300']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #300']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #300']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #302']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #302']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #302']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #302']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #304']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #304']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:165.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #307']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:18.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #307']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #309']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:52.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #311']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #311']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #311']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #311']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #312']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:50.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #314']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:50.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #315']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #315']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #315']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #315']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #317']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #317']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #317']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #317']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #320']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:18.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #321']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:51.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #325']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #325']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #325']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #325']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:52.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #327']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:50.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #328']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:18.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #331']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #336']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #337']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #340']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #340']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #340']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #340']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #342']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #345']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #345']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #345']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #345']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #346']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #346']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #346']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #346']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:18.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #347']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #347']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #347']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #347']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #349']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #350']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #354']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #354']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #354']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #354']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #357']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #357']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #357']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #357']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #361']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #364']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #364']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #366']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #368']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #368']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #368']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #368']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #370']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #371']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #373']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #377']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #378']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #378']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #378']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #378']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #379']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #381']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #381']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #381']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #381']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #383']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #383']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #383']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #383']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #385']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #387']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #387']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #387']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #387']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #390']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #390']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #390']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #390']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #392']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #396']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #398']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #398']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #398']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #398']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #400']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #400']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #400']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #400']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #401']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #404']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #405']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #407']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #409']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #410']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #411']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #414']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #418']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #421']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #426']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #428']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #430']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #433']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #433']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #433']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #433']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #436']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #440']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #443']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #446']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #451']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #451']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #451']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #451']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #453']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #453']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #453']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #453']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #457']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #460']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #461']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #465']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #472']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #474']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #477']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #477']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #477']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #477']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #481']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #485']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #488']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #488']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #488']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #488']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #490']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #490']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #490']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #490']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #495']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #499']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:59.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #503']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:49.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #504']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #506']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #506']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #506']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #506']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #509']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #511']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #514']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:47.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #517']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #517']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #517']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #517']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #518']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #518']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #518']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #518']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #524']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #526']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #528']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #528']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #528']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #528']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #531']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:42.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #535']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #536']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #537']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #537']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #537']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #537']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #539']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #539']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #539']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #539']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #541']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:48.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #543']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:43.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #544']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #546']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #547']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #549']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #549']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #549']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #549']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #551']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #552']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #555']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #555']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #555']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #555']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #559']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #559']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #559']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #559']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #562']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #563']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #565']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #567']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #569']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #572']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #575']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #575']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #575']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #575']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:45.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #579']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:44.0, U:l/Min; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #580']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #580']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #580']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #580']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:0.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #581']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #595']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #595']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #595']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #595']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:0.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #596']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Hi (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:14.0, U:cmH2O; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #601']/data/events[0]/data missing; Required property /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #601']/data/events[at0002,'Any event']/data missing; Existence not matched at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #601']/data/events[at0002,'Any event']/data, expected: [1..1], actual: 0; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #601']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:46.0, U:l/Min; Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #603']/data/events[at0002,'Any event']/state/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Lo (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:0.0, U:l/Min",
              "moreInfo": "https://confluence.ehrvendor.com/display/ehrrest/COMP-1081",
              "requestHref": "http://172.24.8.56:8081/rest/v1/composition/contribution"
            }

            */
            #endregion

            //handle if there was no response - probably because connection was aborted for some reason
            if (response == "no")
            {
                //if there was no response on first try - give it another
                if (!requestBody.Contains("RESENT_CONTRIBUTION-"))
                {
                    this.contributionsHandler.log.inputText = requestBody.Substring(0, 100);
                    string s = requestBody.Replace("report/context/report_id\":\"", "report/context/report_id\":\"RESENT_CONTRIBUTION-");
                    repoHandle.httpRequest("composition/contribution/", true, s, "application/json");
                    return;
                }


                if (!requestBody.Contains(suppressedBadEntriesTag)) this.storedInvalidCounter++;
                //this.storeProblematicRequests(errorTableConnection, requestBody, response, sqlStatement);
                tmpProblematicRequests.TryAdd(requestBody, response);


                if (this.invalidCompositionsPaths.ContainsKey("No response"))
                {
                    int tmp = Int32.Parse(this.invalidCompositionsPaths["No response"][0]);
                    this.invalidCompositionsPaths["No response"][0] = "" + (tmp + 1);
                }
                else
                {
                    this.invalidCompositionsPaths["No response"] = new string[] { "1", "(usually connection timeout because validation took to long)", "0" };
                }
            }
            else
            {
                Dictionary<string, object> responseDict = new JavaScriptSerializer().DeserializeObject(response) as Dictionary<string, object>;
                if (responseDict.ContainsKey("code"))
                {
                    if (responseDict["code"].ToString().StartsWith("EHR") == true)
                    {
                        if (responseDict["code"].ToString() == "EHR-2124")
                        { //ehr for subjectId in namespace already exists - get the ehrId from Think!-platform
                            repoHandle.httpRequest("ehr?" + responseDict["requestHref"].ToString().Split(new[] { "ehr?" }, StringSplitOptions.None)[1].Replace("\\u0026", "&"), true, null, "application/json");
                        }
                        else
                        {
                            File.AppendAllText(curDir + "/Log/Log.txt", "RepoHandle - unexpected response: " + string.Join(";", responseDict.Select(x => x.Key + "=" + x.Value).ToArray()) + Environment.NewLine);
                            this.contributionsHandler.log.inputText = "RepoHandle - unexpected response - see Log.txt for details...";
                            return;
                        }
                    }
                    else if (responseDict["code"].ToString().StartsWith("COMP") == true)
                    {
                        string errorDescription = response;
                        Regex rgx = new Regex("");
                        int suppressBadValuesState = 0;
                        string newRequestBody = requestBody;

                        //try to produce helpful statistics about invalid contributions
                        try
                        {
                            string aTmpStr = responseDict["exceptionMessage"].ToString().Replace("There were errors in validation:", "");
                            string[] compositionErrors = aTmpStr.Split(new string[] { "; " }, StringSplitOptions.None);

                            // state = 0 -> just count distinct  problems, 
                            // state = 1 -> try to suppress problematic entries but nothing suppressed yet 
                            // state = 2 -> try to suppress problematic entries at least one bad entry suppressed already
                            if ((compositionErrors.Count() / Regex.Matches(newRequestBody, "\\\",").Count < worthCleansingLevel) && (!requestBody.Contains(suppressedBadEntriesTag)))
                            {
                                suppressBadValuesState = 0; //since Version on 15.07.2019 goes into infinite loop on bad compositions and i have no time to search for the problem i don't suppress bad values
                            }

                            foreach (string compositionError in compositionErrors)
                            {
                                //wenn Anzahl Fehler < x,x% der Composition Einträge, entsprechende Einträge rausschmeißen
                                if (suppressBadValuesState > 0)
                                {
                                    try
                                    {

                                        if (Regex.Matches(compositionError, ".+\\(path:").Count > 0)
                                        {
                                            //"Invalid decimal value: 3,7 (path: report/ventilator_observations:1/ventilator_settings_findings2/oxygen_delivery/ambient_oxygen_en/percent_o2_en|numerator)",
                                            string tmpPath = compositionError.Trim();
                                            string[][] requestPatterns = new string[][] { new string[] {" #\\d+", ""},
                                                new string[]{".+\\(path:", ""},
                                                new string[]{"\\)", ""},
                                                new string[]{"\\/", "\\/"},
                                                new string[]{"\\[", "\\["},
                                                new string[]{"\\]", "\\]"},
                                                new string[]{"\\|", "\\|"} };
                                            foreach (string[] pattern in requestPatterns)
                                            {
                                                rgx = new Regex(pattern[0]);
                                                tmpPath = rgx.Replace(tmpPath, pattern[1]);
                                            }

                                            rgx = new Regex("\\\"" + tmpPath.Trim() + "\\\":(\\\"[^\\\"]*\\\",|[^\\\"]*,)?");
                                            newRequestBody = rgx.Replace(newRequestBody, "", 1);

                                            suppressBadValuesState = 2;
                                            this.logProblematicEntry(compositionError, true);
                                        }
                                        else if (Regex.Matches(compositionError, "actual:").Count > 0)
                                        {
                                            //Invalid value at /content[openEHR-EHR-OBSERVATION.ventilator_vital_signs.v1,'Ventilator observations #372']/data/events[at0002,'Any event']/data/items[openEHR-EHR-CLUSTER.ventilator_settings2.v1,'Ventilator settings/findings']/items[at0015,'Positive End Expiratory Pressure (PEEP)']/value, expected: M:[0.0..200.0], P:[1..1], U:mbar, actual: quantity M:413.0, P:1, U:mbar";
                                            string tmpError = compositionError.Trim();
                                            //get entry number from error description - result z.B. 372
                                            string tmpN = "";
                                            rgx = new Regex("#[0-9]+']");
                                            Match match = rgx.Match(tmpError);
                                            if (match.Success)
                                            {
                                                tmpN = match.Value;
                                                rgx = new Regex("[0-9]+");
                                                match = rgx.Match(tmpN);
                                                if (match.Success)
                                                {
                                                    tmpN = match.Value;
                                                }
                                            }
                                            else //bei der ersten instanz geben sie keine #1 an, sondern einfach keine Nummer
                                            {
                                                tmpN = "1";
                                            }
                                            int tmpInt = Int32.Parse(tmpN);
                                            tmpN = "" + (tmpInt - 1); //die Nummer im Webtemplate-Path ist eins niedriger als die in der Fehlermeldung
                                                                      //get value from error description - result z.B. 4130
                                            string tmpM = "";
                                            rgx = new Regex("actual:[^;]+"); //z.B. actual: quantity M:413.0, P:1, U:mbar"
                                            match = rgx.Match(tmpError);
                                            if (match.Success)
                                            {
                                                tmpM = match.Value;
                                                rgx = new Regex("M:.+?, "); // z.B. M:4130, 
                                                match = rgx.Match(tmpM);
                                                if (match.Success)
                                                {
                                                    tmpM = match.Value;
                                                    rgx = new Regex(", ");
                                                    tmpM = rgx.Replace(tmpM, "");
                                                    rgx = new Regex("M:");
                                                    tmpM = rgx.Replace(tmpM, "");
                                                    tmpM = tmpM.Split('.')[0];
                                                }
                                            }
                                            rgx = new Regex("\\\"[^,]+?:" + tmpN + "[^\"]+\":\\s?(\\\"" + tmpM + "\\\",|" + tmpM + ",)");
                                            //get path so other pathes like e.g. unit can get removed too
                                            string tmpP = "wuseldusel";
                                            match = rgx.Match(newRequestBody);
                                            if (match.Success)
                                            {
                                                tmpP = match.Value;
                                                tmpP = tmpP.Split('|')[0];
                                            }
                                            rgx = new Regex(tmpP + "\\|[^\"]+\\\":\\s?(\\\"[^\"]*\\\",|[^\"]*,)?");
                                            newRequestBody = rgx.Replace(newRequestBody, "");

                                            suppressBadValuesState = 2;
                                            this.logProblematicEntry(compositionError, true);
                                        }
                                        else // auch Fehler merken, falls ich den nicht kannte und also auch nicht aufbereiten kann
                                        {
                                            this.logProblematicEntry(compositionError);
                                        }
                                        //in feeder_audit sollten ja sowieso noch Original-Werte hinzugefügt werden (TODO) deswegen hier nicht zwingend nötig die irgendwo zu speichern
                                    }
                                    catch (Exception ex)
                                    {
                                        this.contributionsHandler.log.inputText = DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Error while trying to parse server's hint's line on invalid contribution: " + ex.Message;
                                        File.AppendAllText(curDir + "/Log/LogErrorParsingHint" + Thread.CurrentThread.ManagedThreadId + ".txt", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Response: " + response);
                                        File.AppendAllText(curDir + "/Log/LogErrorParsingHint" + Thread.CurrentThread.ManagedThreadId + ".txt", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Request " + requestBody);
                                    }
                                }
                                else //sonst zählen
                                {
                                    // if this composition is not a modified one, log the problematic entries to help while data cleansing/debugging of mapping
                                    if (!requestBody.Contains(suppressedBadEntriesTag)) this.logProblematicEntry(compositionError);

                                    if (requestBody.Contains(suppressedBadEntriesTag))
                                    {
                                        this.contributionsHandler.log.inputText = "ServerResponseHandling | " + "ok, i noticed the modification marker and the contribution still sucks. There is a special place in hell for that one. Check the Log folder for details.";
                                        File.AppendAllText(curDir + "/Log/Log" + Thread.CurrentThread.ManagedThreadId + ".txt", "modified request which is still invalid - " + requestBody + Environment.NewLine);
                                    }

                                    this.storedInvalidCounter++;
                                }
                            }


                            if (suppressBadValuesState > 0)
                            {
                                /// <summary>
                                /// erstmal ersetzt und HIERMIT kenntlich gemacht, da der Feeder bei meiner THink!-Plattform-Version nicht funzt...ja klar, und ich wunder mich über die Endlosschleife
                                /// </summary>
                                
                                //string entryPoint = "composition\": {";
                                //rgx = new Regex("composition.:.*?{");


                                //newRequestBody = rgx.Replace(newRequestBody, entryPoint + "\"" + suppressedBadEntriesPathTag + "|system_id\": \"SSIS-generic-think-feeder - " + versionTag + "\",");
                                //newRequestBody = rgx.Replace(newRequestBody, entryPoint + "\"" + suppressedBadEntriesPathTag + "|version_id\": \"SSIS-generic-think-feeder - " + suppressedBadEntriesTag + "\",");

                                if (suppressBadValuesState == 1)
                                {
                                    this.storedInvalidCounter++;
                                }
                                else //Composition changed, send it again
                                {
                                    File.AppendAllText(curDir + "/Log/Log" + Thread.CurrentThread.ManagedThreadId + ".txt", "original request - " + requestBody + Environment.NewLine);
                                    File.AppendAllText(curDir + "/Log/Log" + Thread.CurrentThread.ManagedThreadId + ".txt", "modified request - " + newRequestBody + Environment.NewLine);
                                    repoHandle.httpRequest("composition/contribution/", true, newRequestBody, "application/json");
                                    this.modifiedCounter++;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            this.contributionsHandler.log.inputText = DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Error while trying to parse servers hint on invalid contribution: " + Environment.NewLine + ex.Message + Environment.NewLine + "Check Log folder for details.";
                            File.AppendAllText(curDir + "/Log/Log_ParseServerResponseError" + Thread.CurrentThread.ManagedThreadId + ".txt", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Response: " + response);
                            File.AppendAllText(curDir + "/Log/Log_ParseServerResponseError" + Thread.CurrentThread.ManagedThreadId + ".txt", DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Request " + requestBody);
                        }

                        if (suppressBadValuesState != 1) //store it with error details in error table
                        {
                            tmpProblematicRequests.TryAdd(requestBody, "error handling state: " + suppressBadValuesState + ", response: " + errorDescription);
                        }
                    }
                    else
                    {
                        this.contributionsHandler.log.inputText = DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Pretty unexpected response: " + response;
                    }
                }
                else
                {
                    if ((repoHandle.isEhrbase) && (responseDict.ContainsKey("error")) && (responseDict.ContainsKey("status")))
                    {
                        if (response.IndexOf("already an EHR") >-1)
                        {
                            //get ehr_id from repo
                            string subjectId = requestBody.Replace("\"HIER_OBJECT_ID\",\"value\": \"", "§").Split('§')[1]; //extract subject_id from requestBody
                            subjectId = subjectId.Replace("\"}}},\"archetype_node_id\":", "§").Split('§')[0];
                            string namespce = requestBody.Replace("\"namespace\": \"", "§").Split('§')[1]; //extract namespace
                            namespce = namespce.Replace("\",\"type\": \"PERSON\"","§").Split('§')[0];
                            repoHandle.httpRequest("ehr?subjectId=" + subjectId + "&subjectNamespace=" + namespce, true, null, "application/json");
                        }
                        else
                        {
                            this.contributionsHandler.log.inputText = "ServerResponseHandling | " + "ehrbase did not like this contribution. Check the Log folder for details." + Environment.NewLine;
                            File.AppendAllText(curDir + "/Log/Log" + Thread.CurrentThread.ManagedThreadId + ".txt", "Request invalid: " + requestBody + Environment.NewLine + "Response: " + response + Environment.NewLine);
                            this.storedInvalidCounter++;
                        }
                    }
                    else
                    {
                        this.contributionsHandler.log.inputText = DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Error response without further reactions: " + response + Environment.NewLine;
                    }
                }
            }

            this.maybeLog();
        }

        //depending on current counts from responses on posted contributions log some information
        public void maybeLog(bool loganyway = false)
        {
            if ((loganyway) || ((DateTime.Now - this.lastLog).TotalSeconds > 60 / this.numberOfRequestStats) || (this.storedInvalidCounter + this.validCounter == this.contributionsHandler.contributionsCounter)) {
                this.lastLog = DateTime.Now;
                this.contributionsHandler.log.inputText = DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") +
                    " Responses for " + Math.Round((double)(this.storedInvalidCounter + this.validCounter) * 100 / this.contributionsHandler.contributionsCounter)
                    + "% of contributions:" + Environment.NewLine +
                    "   - Posted requests: " + this.repoHandle.requestCounter + Environment.NewLine +
                    "   - Open requests: " + this.repoHandle.openRequests.Count + Environment.NewLine +
                    "   - Invalid contributions: " + this.storedInvalidCounter + Environment.NewLine +
                    "   - Modified contributions (reposted): " + this.modifiedCounter + Environment.NewLine +
                    "   - Valid contributions (confirmed from repository): " + this.validCounter + Environment.NewLine +
                    "   - Checksum: Valid + Invalid = " + (this.storedInvalidCounter + this.validCounter) + Environment.NewLine;
            }
            if (this.storedInvalidCounter + this.validCounter == this.contributionsHandler.contributionsCounter) {
                this.contributionsHandler.log.inputText = "Got responses for all contributions! Job done.";
            }
            this.contributionsHandler.checkWaitingContributions();
        }

        private void logProblematicEntry(string compositionError, bool suppressed = false)
        {
            Regex rgx = new Regex("");
            string tmpStr = compositionError.Trim();

            //extract some extra info from the error instance which shall be logged individually
            string extraInfo = "";
            if (compositionError.Contains("actual:"))
            {
                rgx = new Regex("actual:.+");
                Match match = rgx.Match(compositionError);
                if (match.Success)
                {
                    extraInfo = match.Value;
                }
            }
            else if (compositionError.Contains("value:"))
            {
                rgx = new Regex("value:.+\\(");
                Match match = rgx.Match(compositionError);
                if (match.Success)
                {
                    extraInfo = match.Value;
                }
            }

            //extract the error path which can occur in multiple error-instances but shall be displayed only once
            string[][] patterns = new string[][] { new string[] {" #\\d+", ""},
                                        new string[]{":\\d+\\/", ":0/"},
                                        new string[]{":\\d+\\|", ":0|"},
                                        new string[]{"actual:.+", "actual:aValue"},
                                        new string[]{"value:.+\\(", "value: aValue ("} };
            foreach (string[] pattern in patterns)
            {
                rgx = new Regex(pattern[0]);
                tmpStr = rgx.Replace(tmpStr, pattern[1]);
            }
            tmpStr = tmpStr.Trim();

            int counterIndex = suppressed ? 2 : 0;
            //enter the entry in dict for problematic entries
            if (!this.invalidCompositionsPaths.ContainsKey(tmpStr))
            {
                this.invalidCompositionsPaths[tmpStr] = new string[] { "0", extraInfo, "0" };
            }
            int tmp = Int32.Parse(this.invalidCompositionsPaths[tmpStr][counterIndex]);
            this.invalidCompositionsPaths[tmpStr][counterIndex] = "" + (tmp + 1);
            this.invalidCompositionsPaths[tmpStr][1] += " --- " + extraInfo;
        }

        public void logInvalidCompositionsPaths()
        {
            if (this.invalidCompositionsPaths.Count == 0)
            {
                MessageBox.Show("There are no InvalidCompositionsPaths to log.", "ServerResponseHandling");
                return;
            }
            File.WriteAllText(curDir + "/Log/Log_InvalidCompositionPaths.txt", "RepoHandle - Logging InvalidCompositionsPaths: " + Environment.NewLine);
            File.AppendAllText(curDir + "/Log/Log_InvalidCompositionPaths.txt", Environment.NewLine);
            foreach (KeyValuePair<string, string[]> entry in this.invalidCompositionsPaths)
            {
                File.AppendAllText(curDir + "/Log/Log_InvalidCompositionPaths.txt", entry.Value[0] + "/" + entry.Value[2] + " x " + entry.Key + Environment.NewLine);
                File.AppendAllText(curDir + "/Log/Log_InvalidCompositionPaths.txt", "Extra Infos: " + entry.Value[1] + Environment.NewLine);
                File.AppendAllText(curDir + "/Log/Log_InvalidCompositionPaths.txt", Environment.NewLine);
            }
        }

        public void storeProblematicRequests(PgSqlConnection con, string requestBody, string response, string sqlStatement)
        {
            //string tmp = "INSERT INTO " + errorTable + " (composition, error_description) VALUES (@composition, @error_desc);";
            try
            {
                PgSqlCommand sql = new PgSqlCommand(sqlStatement, con);

                sql.Parameters.Add("@composition", PgSqlType.Text);
                sql.Parameters.Add("@error_desc", PgSqlType.VarChar);
                sql.Parameters["@composition"].Value = requestBody;
                sql.Parameters["@error_desc"].Value = response;
                sql.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + Environment.NewLine + " Error while storing invalid composition. SQL: " + sqlStatement + " Error Message: " + ex.Message, "ServerResponseHandling");
            }
        }

        /* handling of positive server responses for async-get-requests */
        void IServerResponseHandling.handleGetResponse(string response, string request_address)
        {
            Dictionary<string, object> responseDict = new JavaScriptSerializer().DeserializeObject(response) as Dictionary<string, object>;
            if ((repoHandle.isEhrbase) && (responseDict.ContainsKey("ehr_id")))
            {
                Dictionary<string, object> ehrId = responseDict["ehr_id"] as Dictionary<string, object>;
                string a = ehrId["value"].ToString(); //get ehr_id from response
                string subjectId = request_address.Replace("subject_id=", "§").Split('§')[1].Split('&')[0]; //get subject_id from request
                contributionsHandler.assignEhrId(subjectId, ehrId["value"].ToString());
            }
            else if (responseDict.ContainsKey("ehrId"))
            {
                Dictionary<string, object> ehrStatus = responseDict["ehrStatus"] as Dictionary<string, object>;
                string subjectId = ehrStatus["subjectId"].ToString(); //extract subjectId from response

                contributionsHandler.assignEhrId(subjectId, responseDict["ehrId"].ToString());
            }
            else
            {
                this.contributionsHandler.log.inputText = DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Unexpected response: " + response;
            }
        }
    }
    #endregion

    #region classes for interactions with REST-API

    public class RepoHandle
    {
        private string curDir = Directory.GetCurrentDirectory();
        public string thinkUserName { get; set; }
        public string thinkUserPwd { get; set; }
        public string thinkHTTPConnection { get; set; }
        public Boolean isEhrbase { get; set; }
        public ConcurrentDictionary<string, bool> openRequests { get; set; }
        public DateTime lastResponse { get; set; }
        public int requestCounter { get; set; }
        public IServerResponseHandling responseHandle { get; set; }
        public loggingBox log { get; set; }

        public RepoHandle(string thinkHTTPConnectionName, string thinkUserName, string thinkUserPwd, loggingBox log, IServerResponseHandling responseHandle)
        {
            this.thinkHTTPConnection = thinkHTTPConnectionName;
            this.thinkUserName = thinkUserName;
            this.thinkUserPwd = thinkUserPwd;
            this.log = log;
            this.responseHandle = responseHandle;
            if (this.thinkHTTPConnection.IndexOf("/ehrbase/") > -1 )
            {
                this.isEhrbase = true;
            }
            else
            {
                this.isEhrbase = false;
            }

            this.openRequests = new ConcurrentDictionary<string, bool>();
            this.lastResponse = DateTime.UtcNow;
            this.requestCounter = 0;
        }

        /* 	make a http-request to Think-REST-API
        @param urlAppendix specifies to which part of the API e.g. http://www.thinkehraddress.de/composition/contribution would use appendix "composition/contribution" (yes, without first slash)
        @param async request synchronous or asynchronous
        @param requestBody if value is given, this will be the post-requests body. If no value is given there will be a get-request.
        @param contentType if value is given, contentType-header will be set with that value
        */
        public string httpRequest(string urlAppendix, bool async, string requestBody, string contentType)
        {
            try
            {
                string tmp_base_con = thinkHTTPConnection;
                //catch and adapt requests for ehrbase
                /*get WebTemplate and example paths for template
                * query repository
                * ehr get existing ids and create if necessary
                * compostition/contribution including basic error handling, e.g. logging invalid contributions with responses
                * delete ehrs or compositions
                */
                if (this.isEhrbase)
                {
                   /* if (this.log != null)
                    {
                        this.log.inputText = (DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + "base address: " + tmp_base_con + "urlAppendix: " + urlAppendix + " async: " + async +
                        " requestBody: " + requestBody + " contentType: " + contentType + Environment.NewLine);
                    }*/
                    if (urlAppendix.IndexOf("template/") > -1)
                    {
                        if (urlAppendix.IndexOf("/example") > -1) //is example request - which will be replaced with WebTemplate request to ehrbase
                        {
                            tmp_base_con = tmp_base_con.Replace("/rest/openehr/", "/rest/ecis/");
                            urlAppendix = urlAppendix.Replace("/example", "");
                        }
                        else // is opt request, lol seems that it was always a WebTemplate request...I have to change less stuff if i just fire this request here a second time
                        {
                            tmp_base_con = tmp_base_con.Replace("/rest/openehr/", "/rest/ecis/");
                            //urlAppendix = urlAppendix.Replace("template/", "definition/template/adl1.4/");
                        }
                    }
                    else if (urlAppendix.IndexOf("query?") > -1) //query repo (post)
                    {
                        string[] parts = urlAppendix.Split('?');
                        urlAppendix = parts[0].Replace("query", "query/aql/");
                        requestBody = "{\"q\": \"" + parts[1].Replace("aql=", "") + "\"}"; //with \\\" final JSON string contains \"
                        contentType = "application/json";
                    }
                    else if (urlAppendix.IndexOf("ehr?") > -1) 
                    {
                        if (requestBody == null) //get ehr_id for subject_id (get)
                        {
                            urlAppendix = urlAppendix.Replace("subjectId", "subject_id").Replace("subjectNamespace", "subject_namespace");
                        }
                        else  //create ehr (post)
                        {
                            string[] parts = urlAppendix.Split('?');
                            urlAppendix = parts[0];
                            string[] parts2 = parts[1].Split('&');
                            requestBody = "{\"_type\": \"EHR_STATUS\",\"name\": {\"_type\": \"DV_TEXT\",\"value\": \"EHR Status\"}," +
                                "\"subject\": {\"_type\": \"PARTY_SELF\",\"external_ref\": {\"namespace\": \"" + parts2[1].Replace("subjectNamespace=", "") + "\",\"type\": \"PERSON\"," +
                                "\"id\": {\"_type\": \"HIER_OBJECT_ID\",\"value\": \"" + parts2[0].Replace("subjectId=", "") + "\"}}},\"archetype_node_id\": \"openEHR-EHR-EHR_STATUS.generic.v1\"," +
                                "\"is_modifiable\": true,\"is_queryable\": true}";
                            contentType = "application/json";
                        }
                    }
                    else if (urlAppendix.IndexOf("composition/contribution") > -1) //sending a contribution
                    {
                        tmp_base_con = tmp_base_con.Replace("/rest/openehr/", "/rest/ecis/");
                        urlAppendix = urlAppendix.Replace("composition/contribution/", "composition/?format=FLAT&ehrId=") + "";
                        string[] parts = requestBody.Replace(",\"composition\":","§").Split('§');
                        requestBody = parts[1].Substring(0, parts[1].Length - 2);
                        parts = parts[0].Split(',');
                        string templateId = "";
                        string ehrId = "";
                        foreach (var s in parts)
                        {
                            if (s.IndexOf("templateId") > -1)
                            {
                                templateId = s.Split(':')[1].Replace("\"", "");
                            }
                            else if (s.IndexOf("ehrId") > -1)
                            {
                                ehrId = s.Split(':')[1].Replace("\"", "");
                            }
                        }
                        urlAppendix += ehrId + "&templateId=" + templateId;
                    }
                    else if (contentType == "DELETE-ADMIN") //delete ehr or composition (delete)
                    {
                        if (urlAppendix.IndexOf("ehrs") > -1)
                        {
                            tmp_base_con = tmp_base_con.Replace("openehr/v1/", "admin/");
                            urlAppendix = urlAppendix.Replace("ehrs/", "ehr/");
                        }
                        else
                        {
                           urlAppendix = urlAppendix.Replace("compositions/", "ehr/" + requestBody + "/composition/").Replace("/openehr/v1", "");
                           requestBody = "";
                        }
                    }
                    else
                    {
                        string reqBodyNull = " (not null)";
                        if (requestBody == null)  { reqBodyNull = " (null)"; };
                        this.log.inputText = (DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + "urlAppendix: " + urlAppendix + " async: " + async + " requestBody: " + requestBody + reqBodyNull + " contentType: " + contentType + Environment.NewLine);
                        this.log.inputText = (DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + "request address: " + tmp_base_con + urlAppendix + Environment.NewLine);
                    }
                }

                // Create a webclient
                Uri address = new Uri(tmp_base_con + urlAppendix);
                WebClient myWebClient = new WebClient();
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //TLS 1.2
                myWebClient.Encoding = Encoding.UTF8;
                myWebClient.Credentials = new System.Net.NetworkCredential(thinkUserName, thinkUserPwd);
                myWebClient.BaseAddress = address.AbsoluteUri;

                this.requestCounter++;
                if (contentType != null)
                {
                    if (contentType == "DELETE-ADMIN")
                    { //catch special case DELETE ON ADMIN-REST-API
                        if (!this.isEhrbase) { myWebClient.BaseAddress = myWebClient.BaseAddress.Replace("/rest/", "/admin/rest/"); }
                        return myWebClient.UploadString(myWebClient.BaseAddress, "DELETE", requestBody);
                    }
                    else
                    {
                        myWebClient.Headers.Add(HttpRequestHeader.ContentType, contentType);
                    }
                }

                //send request
                if ((requestBody == null) && (!async)) //Sending and response handling to sync GET requests e.g. template example and opt
                {
                    return myWebClient.DownloadString(address);
                }
                else if ((requestBody == null) && (async)) // Sending and response handling to async GET requests e.g. ?
                {
                    myWebClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(getCallback);
                    myWebClient.DownloadStringAsync(address);
                    this.openRequests[address.ToString()] = true;
                    return "async request";
                }
                else
                {
                    if (async) // Sending and response handling to async POST requests e.g. create ehr or sending contribution
                    {
                        myWebClient.UploadStringCompleted += (sender, e) => postCallback(sender, e, requestBody);
                        myWebClient.UploadStringAsync(address, requestBody);
                        if (this.isEhrbase)
                        {
                            this.openRequests[requestBody] = true;
                        }
                        else
                        {
                            if (requestBody == "")
                            {
                                this.openRequests[address.ToString()] = true;
                            }
                            else
                            {
                                this.openRequests[requestBody] = true;
                            }
                        }
                        return "async request";
                    }
                    else  //Sending and response handling to sync POST requests, e.g. query on ehrbase
                    {
                        string res = myWebClient.UploadString(address, requestBody);
                        //catch and adapt requests for ehrbase
                        if (this.isEhrbase)
                        {
                            if (urlAppendix.IndexOf("query/aql") > -1) //catch query request response
                            {
                                dynamic resObj = JsonConvert.DeserializeObject<ExpandoObject>(res, new ExpandoObjectConverter());
                                string myJSONresults = "{\"resultSet\": [";
                                if (res != null)
                                {
                                    int row_index = 0;
                                    foreach (dynamic row in resObj.rows)
                                    {
                                        if (row_index > 0) {myJSONresults += ","; }
                                        myJSONresults += "{";
                                        int col_index = 0;
                                        foreach (dynamic col in row)
                                        {
                                            if (col_index > 0) { myJSONresults += ","; }
                                            myJSONresults += "\""+resObj.columns[col_index].name + "\":" + "\"" + col + "\"";
                                            col_index++;
                                        }
                                        myJSONresults += "}";
                                        row_index++;
                                    }
                                }
                                myJSONresults += "]}";
                                res = myJSONresults;
                            }
                        }
                        return res;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error on http-request: " + ex.Message);
                return "Error on http-request";
            }
        }

        /* callback for async-get-requests to Think-REST-API */
        private void getCallback(object sender, DownloadStringCompletedEventArgs e)
        {
            this.lastResponse = DateTime.UtcNow;
            try
            {
                WebClient tmpWC = (WebClient)sender;

                bool notUseless = false;
                int aCounter = 0;
                while ((!notUseless) && (aCounter < 10))
                {
                    this.openRequests.TryRemove(tmpWC.BaseAddress, out notUseless);
                    if (!notUseless) System.Threading.Thread.Sleep(50);
                    aCounter++;
                }
                if (!notUseless) this.log.inputText = "(RepoHandle, getCallBack) " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "Removing request type 1 failed";

                this.responseHandle.handleGetResponse(e.Result, tmpWC.BaseAddress);
            }
            catch (Exception ex)
            {
                this.log.inputText = "(RepoHandle) " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " HTTP-GET-Request failed: " + ex.Message;
            }
        }

        /* callback for async-post-requests to Think-REST-API */
        private void postCallback(Object sender, UploadStringCompletedEventArgs e, string requestBody)
        {
            int state = 0;
            this.lastResponse = DateTime.UtcNow;
            Exception error = e.Error;
            try
            {
                if (requestBody == "")
                { //if there is no requestBody given, address is used to identify requests
                    WebClient tmpWC = (WebClient)sender;
                    requestBody = tmpWC.BaseAddress;
                    state = 1;
                }

                bool notUseless = false;
                int aCounter = 0;
                while ((!notUseless) && (aCounter < 10))
                {
                    state = 2;
                    openRequests.TryRemove(requestBody, out notUseless);
                    if (!notUseless) System.Threading.Thread.Sleep(50);
                    aCounter++;
                }

                if (!notUseless)
                {
                    this.log.inputText = "RepoHandle " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "Removing request type 2 failed (Possible reason: Identical compositions sent simultaneously. This is not necessarily a problem.)" + Environment.NewLine;
                    this.log.inputText = requestBody + Environment.NewLine;
                }

                state = 3;
                if (error != null)
                {
                    state = 4;
                    WebException webEx = (WebException)error;
                    state = 5;
                    HttpWebResponse webResp = (HttpWebResponse)webEx.Response;
                    state = 6;
                    if (webEx.Response != null)
                    {
                        state = 7;
                        string response = new StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                        state = 8;
                        this.responseHandle.handlePostError(response, requestBody);
                        state = 9;
                    }
                    else
                    {
                        state = 10;
                        File.AppendAllText(curDir + "/Log/Log" + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", "RepoHandle Request Body: " + requestBody + Environment.NewLine);
                        state = 11; 
                        File.AppendAllText(curDir + "/Log/SpecialRequest" + "_" + requestBody.GetHashCode() + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", requestBody);
                        File.AppendAllText(curDir + "/Log/Log_" + Thread.CurrentThread.ManagedThreadId + ".txt", requestBody.GetHashCode() + "," + requestBody.Length + ", " + requestBody.Count(x => x == ',') + Environment.NewLine);
                        this.responseHandle.handlePostError("no", requestBody);
                        state = 12;
                    }
                }
                else
                {
                    state = 13;
                    if (this.isEhrbase)
                    {
                        
                        if (e.Result == "") //create ehr response
                        {
                            WebHeaderCollection myWebHeaderCollection = ((WebClient)sender).ResponseHeaders;
                            this.responseHandle.handlePostResponse(myWebHeaderCollection.Get("Location"), requestBody);
                        }
                        else //create contribution response
                        {
                            this.responseHandle.handlePostResponse(e.Result, requestBody);
                        }
                    }
                    else
                    {
                        this.responseHandle.handlePostResponse(e.Result, requestBody);
                    }    
                }
            }
            catch (Exception ex)
            {
                this.log.inputText = "RepoHandle " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " Post-request failed for unknown reason! State:" + state + ", Exception Details: " + ex.Message + Environment.NewLine + "Check Log folder for details";
                File.AppendAllText(curDir + "/Log/LogPostFailedUnknownReason" + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", "RepoHandle " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "Request Body: " + requestBody);
                File.AppendAllText(curDir + "/Log/LogPostFailedUnknownReason" + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", "RepoHandle " + DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + "RepoHandle - error: " + error.ToString());
            }
        }
    }
    #endregion
}

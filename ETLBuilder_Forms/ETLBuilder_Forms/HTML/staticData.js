conditionsData = [
    "true",
	"changed",
    "columnX=valueY"
];

actionsData = [
    {
        method: "newContribution",
        parameters: [
			{parameterName: "action", defaultValue:"CREATE", exampleValues:["CREATE", "UPDATE", "DELETE"]}, 
            {parameterName: "templateId"}, 
            {parameterName: "subjectId_column"}, 
            {parameterName: "ctx/language", defaultValue:"de", exampleValues:["de", "en"]}, 
            {parameterName: "ctx/territory", defaultValue:"DE", exampleValues:["DE", "EN"]}]
    },
    {
        method: "newSubtree",
        parameters: [{parameterName: "path"}]
    },
    {
        method: "newEntry",
        parameters: [
		{parameterName: "path"}, 
		{parameterName: "cast", exampleValues:[	"constantValue,<REPLACE_WITH_VALUE>,asInt",
                                                "constantValue,<REPLACE_WITH_VALUE>,asDouble",
                                                "constantValue,<REPLACE_WITH_VALUE>",
												"noCast", 
                                                "replaceDotWithComma",
                                                "replaceCommaWithDot",
                                                "replace,<TO_REPLACE>,<TO_REPLACE_WITH>",
												"insertValueIntoConstant,<REPLACE_WITH_CONSTANT_AND_USE_THIS_<TAG_FOR_VALUE>>",
												"split,<REPLACE_WITH_DELIMITER>,<REPLACE_WITH_INDEX>",
												"asInt",
                                                "asDouble",
                                                "asISO8601",
                                                "catchNullFlavours",
												"noEmptyEntry",
												"castIntoAtCode",
												"castUsingDictionary"
									]},
		{parameterName: "sourceColumn"}]
    },
    {
        method: "newSubtrees + newEntry",
        parameters: [{ parameterName: "path" }]
    }
];
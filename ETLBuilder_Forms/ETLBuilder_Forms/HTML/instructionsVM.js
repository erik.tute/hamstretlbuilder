function InstructionsVM() {
    var self = this;
    self.myJSON = ko.observable("");
    self.myCommit = ko.observable(false);
    self.uname = ko.observable("");
    self.pw = ko.observable("");
    self.columns = ko.observableArray();
    self.paths = ko.observableArray();
    self.probableParams = ko.observableArray();
    self.probableConditions = ko.observableArray();
    self.probableActions = ko.observableArray();
    self.exampleDataHeader = ko.observableArray();
    self.exampleData = ko.observableArray();
    self.currentParameter = false;
    self.currentCondition = false;
    self.currentActions = false;
    self.atCodes = [];
    self.newInstructions = "";

    self.addStringsToReplace = function () {
        var match = ko.utils.arrayFirst(self.columns(), function (item) {
            return "NoCollumnJustADictionaryWithStringsToReplace" === item.colName;
        });

        if (!match) {
            iVM.addColumn(null, null,
                {
                    colName: "NoCollumnJustADictionaryWithStringsToReplace",
                    cAndAs: [
                        {
                            condition: "true",
                            actions: [
                                { method: "ExampleStringToReplace", parameters: ["ExampleStringToReplaceWith"] }
                            ]
                        }]
                }
            );
        }
        else {
            self.addAction(match.cAndAs()[0], null, { method: "ExampleStringToReplace", parameters: ["ExampleStringToReplaceWith"] });
        }
    };

    self.addStringsToReplaceFromExcel = function (StringToReplace, StringToReplaceWith) {

        var match = ko.utils.arrayFirst(self.columns(), function (item) {
            return "NoCollumnJustADictionaryWithStringsToReplace" === item.colName;
        });

        if (!match) {
            iVM.addColumn(null, null,
                {
                    colName: "NoCollumnJustADictionaryWithStringsToReplace",
                    cAndAs: [
                        {
                            condition: "true",
                            actions: [
                                { method: StringToReplace[0], parameters: StringToReplaceWith[0] }
                            ]
                        }]
                }
            );
            var match = ko.utils.arrayFirst(self.columns(), function (item) {
                return "NoCollumnJustADictionaryWithStringsToReplace" === item.colName;
            })
            var i;
            for (i = 1; i < StringToReplace.length; i++) {
                self.addAction(match.cAndAs()[0], null, { method: StringToReplace[i], parameters: StringToReplaceWith[i] });
            };
        }
        else {
            var i;
            for (i = 0; i < StringToReplace.length; i++) {
                self.addAction(match.cAndAs()[0], null, { method: StringToReplace[i], parameters: StringToReplaceWith[i] });
            };
        }
    };

    self.uploadExcel = function () {
        //Reference the FileUpload element.
        var fileUpload = document.getElementById("fileUpload");

        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();

                //For Browsers other than IE.
                reader.onload = function (e) {
                    self.ProcessExcel(e.target.result);
                };
                reader.readAsBinaryString(fileUpload.files[0]);
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid Excel file.");
        }
    };

    self.ProcessExcel = function (data) {

        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_json(workbook.Sheets[firstSheet]);

        //Test if Column Names exist
        if (!(Object.keys(excelRows[0]).includes("StringToReplace"))) {
            alert("The column name StringToReplace does not exist in the excel file!")
        } else if (!(Object.keys(excelRows[0]).includes("StringToReplaceWith"))) {
            alert("The column name StringToReplaceWith does not exist in the excel file!")
        } else {
            var StringToReplace = [];
            var StringToReplaceWith = [];


            for (var i = 0; i < excelRows.length; i++) {
                StringToReplace.push(excelRows[i].StringToReplace);
                StringToReplaceWith.push([excelRows[i].StringToReplaceWith]);
            }

            self.addStringsToReplaceFromExcel(StringToReplace, StringToReplaceWith);
            self.closeModal();
        }
    };

    self.openModal = function () {
        var modal = document.getElementById("myModal");
        modal.style.display = "block"
    };

    self.closeModal = function () {
        var modal = document.getElementById("myModal");
        modal.style.display = "none";

        var fileUpload = document.getElementById("fileUpload");
        fileUpload.value = "";
    };

    self.addExampleData = function (iExampleData) {
  //      console.debug(iExampleData);
  
        self.exampleDataHeader(iExampleData);
    };

    self.addOPT = function (iOPT) {
       // console.log(iOPT);
        //F�gt f�r alle rm-Typen die feeder-Audit-Pfade ein + spezifische Pfade pro rm-Typ (die Korrektheit der Pfade muss hier noch gepr�ft werden)
		//Feeder-Audit auf Element-Ebene aktuell nicht m�glich (Better-Problem)
        var pathList = {};
	
		
        for (var i = 0, le = self.paths().length; i < le; i++) {
            var tmpPath = self.paths()[i];

			if(tmpPath.indexOf("magnitude") != -1){
				self.paths().push(tmpPath.replace("magnitude","magnitude_status"));
				
			}

			if (tmpPath.indexOf("ctx") != -1){
	
                continue;
				}


					
            
		 var components = tmpPath.split("/");

            //set templateId as default templateId for newContribution method
            actionsData.find(function (item) {
                return (item.method == "newContribution");
            }).parameters[1].defaultValue = iOPT.webTemplate.templateId;

            var currentObject = iOPT.webTemplate.tree;
			
		
        //   alert("iOPT.webTemplate.tree" + JSON.stringify(iOPT.webTemplate.tree));
		
            var tmpSubpath = components[0];
            for (var j = 1, le2 = components.length; j < le2; j++) { //bei 1 beginnen weil 0 das bereits gew�hlte Element ist
                for (var k = 0, le3 = currentObject.children.length; k < le3; k++) {
                    if (currentObject.children[k].id == components[j].replace(/:<<index>>/g, "")) {
                        currentObject = currentObject.children[k];
						
                        tmpSubpath += "/" + components[j]; 
				
					
					
                        break;
                    }
                    if ((currentObject.rmType == "OBSERVATION")) { 
                        pathList[tmpSubpath] = "OBSERVATION";
                    }
					if ((currentObject.rmType == "COMPOSITION")) {
                        pathList[tmpSubpath] = "COMPOSITION";
                    }
					if ((currentObject.rmType == "ADMIN_ENTRY")) { 
                        pathList[tmpSubpath] = "ADMIN_ENTRY";
                    }
					if ((currentObject.rmType == "INSTRUCTION")) { 
                        pathList[tmpSubpath] = "INSTRUCTION";
                    }
					if ((currentObject.rmType == "EVALUATION")) { 
                        pathList[tmpSubpath] = "EVALUATION";
                    }
						if ((currentObject.rmType == "CLUSTER")) { 
                        pathList[tmpSubpath] = "CLUSTER";
                    }
				
					
					
                }
            }
        }

        for (var key in pathList) {
            if (pathList[key] == "OBSERVATION" ) {
			// OBSERVATION-spezifische erforderliche RM Attribute
                self.paths.push(key + "/time");
                self.paths.push(key + "/history_origin");
				self.paths.push(key + "/_feeder_audit/original_content");
				self.paths.push(key + "/_feeder_audit/original_content|formalism");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|system_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|version_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|time");
            }
			if (pathList[key] == "COMPOSITION") {
                self.paths.push(key + "/_feeder_audit/original_content");
				self.paths.push(key + "/_feeder_audit/original_content|formalism");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|system_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|version_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|time");

            }
			if (pathList[key] == "ADMIN_ENTRY") {
               // ADMIN-spezifische erforderliche RM Attribute

				self.paths.push(key + "/_feeder_audit/original_content");
				self.paths.push(key + "/_feeder_audit/original_content|formalism");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|system_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|version_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|time");
            }
			if (pathList[key] == "INSTRUCTION") {
               // INSTRUCTION-spezifische erforderliche RM Attribute (Pfade pr�fen)
			   
				self.paths.push(key + "/narrative");
				self.paths.push(key + "/activities/timing");

				self.paths.push(key + "/_feeder_audit/original_content");
				self.paths.push(key + "/_feeder_audit/original_content|formalism");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|system_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|version_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|time");
            }
			if (pathList[key] == "EVALUATION") {
               // ACTION-spezifische erforderliche RM Attribute

				self.paths.push(key + "/_feeder_audit/original_content");
				self.paths.push(key + "/_feeder_audit/original_content|formalism");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|system_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|version_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|time");
            }
			if (pathList[key] == "CLUSTER") {
               // CLUSTER-spezifische erforderliche RM Attribute

				self.paths.push(key + "/_feeder_audit/original_content");
				self.paths.push(key + "/_feeder_audit/original_content|formalism");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|system_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|version_id");
				self.paths.push(key + "/_feeder_audit/originating_system_audit|time");
            }
		

        }

        //get atCodes and Values
        Utils.iterateNestedStructure(iOPT, self.getAtCodes);
    };

    self.getAtCodes = function (obj) {
        if ((obj !== null) && (obj.hasOwnProperty("value")) && (obj.value.indexOf("at") > -1) && (obj.hasOwnProperty("label"))) {
            self.atCodes.push({ method: obj.value, parameters: [obj.label] });
        }
    };

    self.addPaths = function (iPaths) {
        //console.log(iPaths);
        //falls aus Textdatei, dann in brauchbares JSON umwandeln und einlesen
        if (typeof iPaths === 'string') {
            iPaths = JSON.parse(iPaths);
        }
        //catch if it is actually just a webtemplate, because we work on ehrbase which does not provide examples yet
        if (iPaths.webTemplate) {
            var example = {};
            recursively_iterate_webtemplate(iPaths.webTemplate.tree, "", example, true);
            iPaths = example;

            /*	recursively iterates over structure, while keeping track of current path and executing given function for each node	*/
            function recursively_iterate_webtemplate(subtree, path, example, required) {
                if (subtree.id) {
                    var delimiter = (path.length == 0) ? "" : "/";
                    var index = (subtree.max == -1) ? ":0" : "";
                    required = (subtree.min == 1 && required) ? 1 : 0;
                    path += delimiter + subtree.id + index;
                    if (subtree.children) {
                        subtree.children.forEach((node) => {
                            recursively_iterate_webtemplate(node, path, example, required);
                        });
                    }
                    if (subtree.inputs) {
                        subtree.inputs.forEach((input) => {
                            var suffix = (input.suffix) ? '|' + input.suffix : "";
                            var validation = (input.validation) ? input.validation : {};
                            validation["type"] = input.type;
                            validation["required"] = required;
                            if (input.list) {
                                validation["code_list"] = input.list;
                            }
                            example[path + suffix] = validation;
                        });
                    }
                }
            }
        }
        //jetzt pfade aufbereiten und auslesen
        var tmpPaths = [];
        for (var key in iPaths) {
            var currentLine = key;
            currentLine = currentLine.replace(/:0/g, ":<<index>>"); // alle Indexe (Nullen) in Beispielpfaden durch index-Platzhalter ersetzen
            var components = currentLine.split("<<index>>");
            //erstmal stumpf alle Subpfade bilden
            var tmpStr = components[0];
            tmpPaths.push(tmpStr);
            for (var j = 1, le2 = components.length; j < le2; j++) {
                tmpStr += "<<index>>" + components[j];
                tmpPaths.push(tmpStr);
            }
        }
        tmpPaths = Utils.getUnique(tmpPaths);
        for (var i = 0, le = tmpPaths.length; i < le; i++) {
            self.paths.push(tmpPaths[i]);
        }
        //console.log(self.paths());
    };

    self.addInstructions = function (iColumns) {
        //console.log(iColumns);
        //create first suggestion if nothing is given
        if ((!iColumns) || (iColumns == null) || (iColumns.length == 0)) {
            for (var i = 0, le = self.exampleDataHeader().length; i < le; i++) {
                self.addColumn(null, null, { colName: self.exampleDataHeader()[i], cAndAs: [] }, true);
            }
        }
        else {
            for (var i = 0, le = iColumns.length; i < le; i++) {
                self.addColumn(null, null, iColumns[i], true);
            }
        }
        //self.updateCommit();
    };

    self.addColumn = function (sender, event, iColumn, push) {
        if (typeof push === 'undefined') {
            push = false;
        }
        ;
        if (typeof iColumn === 'undefined') {
            iColumn = {
                colName: "",
                cAndAs: []
            };
        }
        if (iColumn.colName == "NoCollumnJustAdditionalInformationAboutAtCodes") return;

        var col = { colName: iColumn.colName, cAndAs: ko.observableArray() };
        for (var i = 0, le = iColumn.cAndAs.length; i < le; i++) {
            self.addCondition(col, null, iColumn.cAndAs[i]);
        }

        if (push == "splice") { //find column entry with colName and replace with new one
            var index = self.columns().findIndex(function (elem) {
                return (elem.colName == col.colName);
            });
            self.columns.splice(index, 1, col);
        }
        else if (push) {
            self.columns.push(col);
        }
        else {
            self.columns.unshift(col);
        }
    };

    self.cloneColumn = function (sender, event) {
        var columnToClone = prompt("Please enter column to clone", "");

        var toClone = self.columns().find(function (elem) {
            return (elem.colName == columnToClone);
        });

        if (toClone) {
            var clone = ko.toJS(toClone);
            clone.colName = sender.colName;
            self.addColumn(null, null, clone, "splice");
        }
    };

    self.addCondition = function (column, event, iCondition) {
        if (typeof iCondition === 'undefined') {
            iCondition = {
                condition: "true",
                actions: [{ method: "", parameters: ko.observableArray() }]
            };
        }

        //condition now observable
        var cAndA = { condition: ko.observable(iCondition.condition), actions: ko.observableArray() };
        for (var i = 0, le = iCondition.actions.length; i < le; i++) {
            self.addAction(cAndA, null, iCondition.actions[i]);
        }

        column.cAndAs.push(cAndA);
    };


    self.addActionAtBegin = function (cAndA, event) {
        self.addAction(cAndA, event, undefined, true);
    }

    self.addAction = function (cAndA, event, iAction, iInsertAtBegin = false) {
        if (typeof iAction === 'undefined') {
            iAction = {
                method: "",
                parameters: []
            };
        }

        //action.method now observable
        var action = { method: ko.observable(iAction.method), parameters: ko.observableArray() };
        for (var i = 0, le = iAction.parameters.length; i < le; i++) {
            self.addParameter(action, null, iAction.parameters[i]);
        }

        if (iInsertAtBegin) {
            cAndA.actions.unshift(action);
        }
        else {
            cAndA.actions.push(action);
        }
    };

    self.addParameter = function (action, event, iParameter) {
        //get static parameter infos
        var parameterObject = { parameterName: "", parameter: ko.observable("") };
        var infos = self.getInfosForMethod(action.method());
        if ((infos != null) && (action.parameters().length < infos.parameters.length)) {
            parameterObject = jQuery.extend(true, {}, infos.parameters[action.parameters().length]);
            if (typeof parameterObject.defaultValue !== 'undefined') {
                parameterObject.parameter = ko.observable(parameterObject.defaultValue);
            }
            else {
                parameterObject.parameter = ko.observable("");
            }
        }
        //set value if one was given
        if (typeof iParameter !== 'undefined') {
            if (iParameter.parameterName) { //catch if iParameter is an object
                parameterObject.parameter = iParameter.parameter
            }
            else {
                parameterObject.parameter = ko.observable(iParameter);
            }
        }

        action.parameters.push(ko.observable(parameterObject));
    };

    self.deleteItem = function (parent, collection, deletingItem = this) { // this = $data, passed when binding from view
        if (collection == "parameters") {
            parent[collection].remove(function (item) {
                return item().parameter == deletingItem.parameter;
            });
        }
        else {
            parent[collection].remove(deletingItem);
        }
    };

    //wenn action ge�ndert wurde, schauen ob methode bekannt, falls ja, dann entsprechende parameterfelder setzen
    self.actionChanged = function (cAndA, method) {
        var action = cAndA;
        var infos = self.getInfosForMethod(method);

        if (infos == null)
            return;

        action.parameters.removeAll();

        for (var i = action.parameters().length, le = infos.parameters.length; i < le; i++) {
            self.addParameter(action, null);
        }

        if (method == "newContribution") {
            if (action.parameters()[2]().parameter() == "") {
                columnName = self.columns().find(function (col) {
                    var result = false;
                    col.cAndAs().forEach(tCandA => {
                        tCandA.actions().forEach(tAction => {
                            if (tAction == action) {
                                console.log("ok1");
                                result = true;
                            }
                        });
                    });
                    return result;
                    }
                ).colName;
                action.parameters()[2]().parameter(columnName);
            }
        }
    };

    self.getInfosForMethod = function (method) {
        for (var i = 0, le = actionsData.length; i < le; i++) {
            if (actionsData[i].method == method) {
                return actionsData[i];
            }
        }
        return null;
    };

    self.editParameter = function (action) {
        var parameter = this; // $data, passed when binding from view
        var tmpParams = [];

        self.clearProbableValues();

        if (parameter.parameterName == "path") {
            tmpParams = self.paths();
            if (action.method() == "newSubtree") {
                tmpParams = tmpParams.filter(self.isSubtreePath(true));
            }
            else {
                tmpParams = tmpParams.filter(self.isSubtreePath(false));
            }
        }
        else if (typeof parameter.exampleValues !== 'undefined') {
            tmpParams = parameter.exampleValues;
        }
        for (var i = 0, le = tmpParams.length; i < le; i++) {
            self.probableParams.push(tmpParams[i]);
        }

        self.currentParameter = parameter;
    };

    self.isSubtreePath = function (invert) {
        return function (path) {
            return invert == (path.substr(path.length - 1) == ':');
        };
    };

    self.toJSON = function () {
        //returnTheString nicht mehr von N�ten
        //if (returnTheString != true)
        //    returnTheString = false;
        //console.log("in toJSON");
        var tmp = ko.toJS(self.columns);
        var templateId = { templateId: "Export" };
        Utils.iterateNestedStructure(tmp, self.getTemplateId(templateId)); //ich bin ein computerhacker!
        Utils.iterateNestedStructure(tmp, self.cleanParams); //parameter objekte zu einem einzelnen string der nur den Wert des parameters enth�lt umwandeln
        tmp.push({ colName: "NoCollumnJustAdditionalInformationAboutAtCodes", cAndAs: [{ condition: "true", actions: self.atCodes }] }); //add atCodes
        //if (returnTheString) {
        self.myJSON(JSON.stringify(tmp, null, 2));
        self.newInstructions = self.myJSON();
        //return JSON.stringify(tmp, null, 2);
        //}
        //else {
        //    self.myJSON(JSON.stringify(tmp, null, 2));
        //    $(".taJSON").focus();
        //    $(".taJSON").select();
        //}
    };

    self.clearJSON = function () {
        self.myJSON(false);
    };

    self.getTemplateId = function (templateId) {
        return function (obj) {
            if ((obj.hasOwnProperty("parameterName")) && (obj.parameterName == "templateId")) {
                templateId.templateId = obj.parameter;
            }
        };
    };

    self.cleanParams = function (obj) {
        if (obj.hasOwnProperty("parameters")) {
            var myParameters = obj.parameters;
            for (var i = 0, le = obj.parameters.length; i < le; i++) {
                obj.parameters[i] = obj.parameters[i].parameter;
            }
        }
    };

    self.changeParameterValue = function () {
        if ((self.currentParameter.parameterName) && (self.currentParameter.parameterName == "cast") &&
            (self.currentParameter.parameter) && (self.currentParameter.parameter() != "")) {
            self.currentParameter.parameter(self.currentParameter.parameter() + "," + this);
        }
        else {
            self.currentParameter.parameter(this);
        }
        self.clearProbableValues();

        //everything that follows is just for "newSubtrees + newEntry", i.e. when path is set for this method, newSubtree and newEntry actions will be created
        //prepending subtrees can just be invoked here, if parameterName is path (notwendige aber nicht hinreichende Bedingung!)
        if ((self.currentParameter.parameterName) && (self.currentParameter.parameterName == "path")) {
            //get the right parent elements
            var result = false;
            self.columns().forEach(tCol => {
                tCol.cAndAs().forEach(tCandAs => {
                    tCandAs.actions().forEach(tAction => {
                        tAction.parameters().forEach(tParam => {
                            if (tParam() == self.currentParameter) {
                                result = { cAndAs: tCandAs, action: tAction };
                            }
                        });
                    });
                });
                return result;
            });
            //only if this condition is met set subtrees etc. else do nothing
            if (result.action.method() == "newSubtrees + newEntry") {
                var tmpPath = ""+this;
                //TODO delete action with "newSubtrees + newEntry"
                self.deleteItem(result.cAndAs, "actions", result.action);
                //add newSubtree and newEntry actions
                var components = tmpPath.split("<<index>>");
                var tmpStr = components[0];
                for (var i = 1, le = components.length; i < le; i++) {
                    //for each subtree in path: newSubtree
                    self.addAction(result.cAndAs, null, { method: "newSubtree", parameters: [tmpStr] });
                    tmpStr += "<<index>>" + components[i];
                }
                //for path add newEntry
                self.addAction(result.cAndAs, null, { method: "newEntry", parameters: [tmpStr] });
            }
        }
    };

    //self.clearProbableParams = function() {
    //    self.probableParams.removeAll();
    //};


    //Hier neue Sachen, um Datalist zu ersetzen (wird vom Cefsharp Chromium Browser *noch* nicht unterst�tzt)
    //Derzeitige Alternative ist die Darstellung einer Auswahlliste wie bei Parameters
    self.editCondition = function (cAndA) {
        var cAndA = this;
        self.clearProbableValues();

        for (var i = 0, le = conditionsData.length; i < le; i++) {
            self.probableConditions.push(conditionsData[i]);
        }

        self.currentConditions = cAndA;
    };

    self.changeConditionValue = function () {
        self.currentConditions.condition(this);
        self.clearProbableValues();
    };

    //self.clearProbableConditions = function () {
    //    self.probableConditions.removeAll();
    //};

    self.editAction = function (cAndA) {
        var cAndA = this;
        self.clearProbableValues();

        for (var i = 0, le = actionsData.length; i < le; i++) {
            self.probableActions.push(actionsData[i].method)
        }

        self.currentActions = cAndA;
    };

    self.changeActionValue = function () {
        self.currentActions.method(this);
        self.clearProbableValues();

        //actionChanged nun geschachtelt in changeActionValue, da nun observable und kein String mehr!
        self.actionChanged(self.currentActions, self.currentActions.method());
    };

    //self.clearProbableActions = function () {
    //    self.probableActions.removeAll();
    //};

    //One function to clear them all
    self.clearProbableValues = function () {
        self.probableActions.removeAll();
        self.probableConditions.removeAll();
        self.probableParams.removeAll();
    };
}
﻿namespace HaMSTR_ETLBuilder_Forms
{
    partial class frmInputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInputBox));
            this.labelInputDescription = new System.Windows.Forms.Label();
            this.btnInputDialogOK = new System.Windows.Forms.Button();
            this.btnInputDialogCancel = new System.Windows.Forms.Button();
            this.txtBoxInputDialog = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelInputDescription
            // 
            resources.ApplyResources(this.labelInputDescription, "labelInputDescription");
            this.labelInputDescription.Name = "labelInputDescription";
            // 
            // btnInputDialogOK
            // 
            this.btnInputDialogOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.btnInputDialogOK, "btnInputDialogOK");
            this.btnInputDialogOK.Name = "btnInputDialogOK";
            this.btnInputDialogOK.UseVisualStyleBackColor = true;
            // 
            // btnInputDialogCancel
            // 
            this.btnInputDialogCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btnInputDialogCancel, "btnInputDialogCancel");
            this.btnInputDialogCancel.Name = "btnInputDialogCancel";
            this.btnInputDialogCancel.UseVisualStyleBackColor = true;
            this.btnInputDialogCancel.Click += new System.EventHandler(this.btnInputDialogCancel_Click);
            // 
            // txtBoxInputDialog
            // 
            resources.ApplyResources(this.txtBoxInputDialog, "txtBoxInputDialog");
            this.txtBoxInputDialog.Name = "txtBoxInputDialog";
            // 
            // frmInputBox
            // 
            this.AcceptButton = this.btnInputDialogOK;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnInputDialogCancel;
            this.Controls.Add(this.txtBoxInputDialog);
            this.Controls.Add(this.btnInputDialogCancel);
            this.Controls.Add(this.btnInputDialogOK);
            this.Controls.Add(this.labelInputDescription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInputBox";
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelInputDescription;
        private System.Windows.Forms.Button btnInputDialogOK;
        private System.Windows.Forms.Button btnInputDialogCancel;
        private System.Windows.Forms.TextBox txtBoxInputDialog;
    }
}
﻿using System;

namespace HaMSTR_ETLBuilder_Forms
{
    partial class ETLBuilder_Initial
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ETLBuilder_Initial));
            this.gridRelData = new System.Windows.Forms.DataGridView();
            this.labelDbServer = new System.Windows.Forms.Label();
            this.btnOpenConnection = new System.Windows.Forms.Button();
            this.txtBoxConnString = new System.Windows.Forms.RichTextBox();
            this.tabControlEtlBuilder = new System.Windows.Forms.TabControl();
            this.tabPageDbConnection = new System.Windows.Forms.TabPage();
            this.btnSaveProject = new System.Windows.Forms.Button();
            this.btnLoadProject = new System.Windows.Forms.Button();
            this.grBoxErrorTableConnection = new System.Windows.Forms.GroupBox();
            this.cBoxErrorLog = new System.Windows.Forms.CheckBox();
            this.cBoxStoreLastWaitingContributions = new System.Windows.Forms.CheckBox();
            this.cBoxStoreProblematicRequests = new System.Windows.Forms.CheckBox();
            this.labelSaveErrorTablesHelp = new System.Windows.Forms.Label();
            this.cBoxErrorDbSqlServer = new System.Windows.Forms.CheckBox();
            this.cBoxErrorDbPg = new System.Windows.Forms.CheckBox();
            this.btnExecStatementErrors = new System.Windows.Forms.Button();
            this.btnCloseErrorDbConn = new System.Windows.Forms.Button();
            this.btnOpenErrorDbConn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxErrorSqlStatement = new System.Windows.Forms.RichTextBox();
            this.labelErrorTableInfo = new System.Windows.Forms.Label();
            this.txtBoxErrorConnString = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.labelErrorDbConnected = new System.Windows.Forms.Label();
            this.labelPreviewRows = new System.Windows.Forms.Label();
            this.txtBoxNumOfRows = new System.Windows.Forms.TextBox();
            this.grBoxDbConnection = new System.Windows.Forms.GroupBox();
            this.cBoxExcelfile = new System.Windows.Forms.CheckBox();
            this.labelHelpDbCon = new System.Windows.Forms.Label();
            this.cBoxPostgres = new System.Windows.Forms.CheckBox();
            this.cBoxSqlServer = new System.Windows.Forms.CheckBox();
            this.labelConnected = new System.Windows.Forms.Label();
            this.btnCloseConnection = new System.Windows.Forms.Button();
            this.labelPreview = new System.Windows.Forms.Label();
            this.btnExecQuery = new System.Windows.Forms.Button();
            this.labelSqlQuery = new System.Windows.Forms.Label();
            this.txtBoxSqlQuery = new System.Windows.Forms.RichTextBox();
            this.tabPageInstructions = new System.Windows.Forms.TabPage();
            this.btnSaveInstructions = new System.Windows.Forms.Button();
            this.grBoxTransmitCons = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.cBoxIncludeInstructionsJSON = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnLogInvalidPaths = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tBoxNrRequestStats = new System.Windows.Forms.TextBox();
            this.btnTransmitContributions = new System.Windows.Forms.Button();
            this.grBoxShowExampleContribution = new System.Windows.Forms.GroupBox();
            this.tBoxDummyEHRId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxExampleConNum = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnShowExampleContribution = new System.Windows.Forms.Button();
            this.labelInstructionsString = new System.Windows.Forms.Label();
            this.txtBoxInstructionsString = new System.Windows.Forms.RichTextBox();
            this.grBoxThinkPlatform = new System.Windows.Forms.GroupBox();
            this.txtBoxNamespaceTag = new System.Windows.Forms.TextBox();
            this.labelNamespaceTag = new System.Windows.Forms.Label();
            this.txtBoxThinkUser = new System.Windows.Forms.TextBox();
            this.txtBoxThinkPwd = new System.Windows.Forms.TextBox();
            this.labelThinkPwd = new System.Windows.Forms.Label();
            this.labelThinkUser = new System.Windows.Forms.Label();
            this.txtBoxTemplateId = new System.Windows.Forms.RichTextBox();
            this.btnAddTemplateId = new System.Windows.Forms.Button();
            this.labelThinkConnHTTP = new System.Windows.Forms.Label();
            this.btnRemoveTemplateId = new System.Windows.Forms.Button();
            this.cmbBoxTemplateId = new System.Windows.Forms.ComboBox();
            this.labelThinkTemplateID = new System.Windows.Forms.Label();
            this.btnAddThinkAddress = new System.Windows.Forms.Button();
            this.btnRemoveThinkAddress = new System.Windows.Forms.Button();
            this.cmbBoxThinkAddress = new System.Windows.Forms.ComboBox();
            this.txtBoxThinkAddress = new System.Windows.Forms.RichTextBox();
            this.btnLoadInstructions = new System.Windows.Forms.Button();
            this.labelShowExampleContributions = new System.Windows.Forms.Label();
            this.txtBoxDisplayExCon = new System.Windows.Forms.RichTextBox();
            this.tabPageMapping = new System.Windows.Forms.TabPage();
            this.tabPageDeleter = new System.Windows.Forms.TabPage();
            this.btnAQLForCompositions = new System.Windows.Forms.Button();
            this.btnAQLEHRs = new System.Windows.Forms.Button();
            this.btnRetrieveIds = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblDeleteType = new System.Windows.Forms.Label();
            this.txtBoxDeleteAmount = new System.Windows.Forms.TextBox();
            this.rTxtBoxDeleting = new System.Windows.Forms.RichTextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.openFileTxtInstructions = new System.Windows.Forms.OpenFileDialog();
            this.bsRelData = new System.Windows.Forms.BindingSource(this.components);
            this.saveInstructions = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gridRelData)).BeginInit();
            this.tabControlEtlBuilder.SuspendLayout();
            this.tabPageDbConnection.SuspendLayout();
            this.grBoxErrorTableConnection.SuspendLayout();
            this.grBoxDbConnection.SuspendLayout();
            this.tabPageInstructions.SuspendLayout();
            this.grBoxTransmitCons.SuspendLayout();
            this.grBoxShowExampleContribution.SuspendLayout();
            this.grBoxThinkPlatform.SuspendLayout();
            this.tabPageDeleter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsRelData)).BeginInit();
            this.SuspendLayout();
            // 
            // gridRelData
            // 
            this.gridRelData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridRelData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridRelData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRelData.Location = new System.Drawing.Point(11, 447);
            this.gridRelData.Name = "gridRelData";
            this.gridRelData.Size = new System.Drawing.Size(1317, 200);
            this.gridRelData.TabIndex = 0;
            // 
            // labelDbServer
            // 
            this.labelDbServer.AutoSize = true;
            this.labelDbServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDbServer.Location = new System.Drawing.Point(6, 34);
            this.labelDbServer.Name = "labelDbServer";
            this.labelDbServer.Size = new System.Drawing.Size(104, 15);
            this.labelDbServer.TabIndex = 9;
            this.labelDbServer.Text = "ConnectionString:";
            // 
            // btnOpenConnection
            // 
            this.btnOpenConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenConnection.Location = new System.Drawing.Point(383, 52);
            this.btnOpenConnection.Name = "btnOpenConnection";
            this.btnOpenConnection.Size = new System.Drawing.Size(123, 36);
            this.btnOpenConnection.TabIndex = 2;
            this.btnOpenConnection.Text = "Open Connection";
            this.btnOpenConnection.UseVisualStyleBackColor = true;
            this.btnOpenConnection.Click += new System.EventHandler(this.btnOpenConnection_Click);
            // 
            // txtBoxConnString
            // 
            this.txtBoxConnString.DetectUrls = false;
            this.txtBoxConnString.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxConnString.Location = new System.Drawing.Point(9, 52);
            this.txtBoxConnString.Name = "txtBoxConnString";
            this.txtBoxConnString.Size = new System.Drawing.Size(368, 93);
            this.txtBoxConnString.TabIndex = 1;
            this.txtBoxConnString.Text = "";
            // 
            // tabControlEtlBuilder
            // 
            this.tabControlEtlBuilder.Controls.Add(this.tabPageDbConnection);
            this.tabControlEtlBuilder.Controls.Add(this.tabPageInstructions);
            this.tabControlEtlBuilder.Controls.Add(this.tabPageMapping);
            this.tabControlEtlBuilder.Controls.Add(this.tabPageDeleter);
            this.tabControlEtlBuilder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlEtlBuilder.Location = new System.Drawing.Point(0, 0);
            this.tabControlEtlBuilder.Name = "tabControlEtlBuilder";
            this.tabControlEtlBuilder.SelectedIndex = 0;
            this.tabControlEtlBuilder.Size = new System.Drawing.Size(1344, 681);
            this.tabControlEtlBuilder.TabIndex = 0;
            this.tabControlEtlBuilder.SelectedIndexChanged += new System.EventHandler(this.tabControlEtlBuilder_SelectedIndexChanged);
            // 
            // tabPageDbConnection
            // 
            this.tabPageDbConnection.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageDbConnection.Controls.Add(this.btnSaveProject);
            this.tabPageDbConnection.Controls.Add(this.btnLoadProject);
            this.tabPageDbConnection.Controls.Add(this.grBoxErrorTableConnection);
            this.tabPageDbConnection.Controls.Add(this.labelPreviewRows);
            this.tabPageDbConnection.Controls.Add(this.txtBoxNumOfRows);
            this.tabPageDbConnection.Controls.Add(this.grBoxDbConnection);
            this.tabPageDbConnection.Controls.Add(this.labelPreview);
            this.tabPageDbConnection.Controls.Add(this.btnExecQuery);
            this.tabPageDbConnection.Controls.Add(this.labelSqlQuery);
            this.tabPageDbConnection.Controls.Add(this.txtBoxSqlQuery);
            this.tabPageDbConnection.Controls.Add(this.gridRelData);
            this.tabPageDbConnection.Location = new System.Drawing.Point(4, 22);
            this.tabPageDbConnection.Name = "tabPageDbConnection";
            this.tabPageDbConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDbConnection.Size = new System.Drawing.Size(1336, 655);
            this.tabPageDbConnection.TabIndex = 0;
            this.tabPageDbConnection.Text = "DB Connection";
            // 
            // btnSaveProject
            // 
            this.btnSaveProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveProject.Location = new System.Drawing.Point(540, 377);
            this.btnSaveProject.Name = "btnSaveProject";
            this.btnSaveProject.Size = new System.Drawing.Size(87, 34);
            this.btnSaveProject.TabIndex = 36;
            this.btnSaveProject.Text = "Save project";
            this.btnSaveProject.UseVisualStyleBackColor = true;
            this.btnSaveProject.Click += new System.EventHandler(this.btnSaveProject_Click);
            // 
            // btnLoadProject
            // 
            this.btnLoadProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadProject.Location = new System.Drawing.Point(540, 337);
            this.btnLoadProject.Name = "btnLoadProject";
            this.btnLoadProject.Size = new System.Drawing.Size(87, 34);
            this.btnLoadProject.TabIndex = 35;
            this.btnLoadProject.Text = "Load project";
            this.btnLoadProject.UseVisualStyleBackColor = true;
            this.btnLoadProject.Click += new System.EventHandler(this.button1_Click);
            // 
            // grBoxErrorTableConnection
            // 
            this.grBoxErrorTableConnection.Controls.Add(this.cBoxErrorLog);
            this.grBoxErrorTableConnection.Controls.Add(this.cBoxStoreLastWaitingContributions);
            this.grBoxErrorTableConnection.Controls.Add(this.cBoxStoreProblematicRequests);
            this.grBoxErrorTableConnection.Controls.Add(this.labelSaveErrorTablesHelp);
            this.grBoxErrorTableConnection.Controls.Add(this.cBoxErrorDbSqlServer);
            this.grBoxErrorTableConnection.Controls.Add(this.cBoxErrorDbPg);
            this.grBoxErrorTableConnection.Controls.Add(this.btnExecStatementErrors);
            this.grBoxErrorTableConnection.Controls.Add(this.btnCloseErrorDbConn);
            this.grBoxErrorTableConnection.Controls.Add(this.btnOpenErrorDbConn);
            this.grBoxErrorTableConnection.Controls.Add(this.label4);
            this.grBoxErrorTableConnection.Controls.Add(this.txtBoxErrorSqlStatement);
            this.grBoxErrorTableConnection.Controls.Add(this.labelErrorTableInfo);
            this.grBoxErrorTableConnection.Controls.Add(this.txtBoxErrorConnString);
            this.grBoxErrorTableConnection.Controls.Add(this.label6);
            this.grBoxErrorTableConnection.Controls.Add(this.labelErrorDbConnected);
            this.grBoxErrorTableConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxErrorTableConnection.Location = new System.Drawing.Point(6, 169);
            this.grBoxErrorTableConnection.Name = "grBoxErrorTableConnection";
            this.grBoxErrorTableConnection.Size = new System.Drawing.Size(528, 252);
            this.grBoxErrorTableConnection.TabIndex = 29;
            this.grBoxErrorTableConnection.TabStop = false;
            this.grBoxErrorTableConnection.Text = "Connection to DB for Error Tables";
            // 
            // cBoxErrorLog
            // 
            this.cBoxErrorLog.AutoSize = true;
            this.cBoxErrorLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxErrorLog.Location = new System.Drawing.Point(289, 9);
            this.cBoxErrorLog.Name = "cBoxErrorLog";
            this.cBoxErrorLog.Size = new System.Drawing.Size(44, 17);
            this.cBoxErrorLog.TabIndex = 36;
            this.cBoxErrorLog.Text = "Log";
            this.cBoxErrorLog.UseVisualStyleBackColor = true;
            // 
            // cBoxStoreLastWaitingContributions
            // 
            this.cBoxStoreLastWaitingContributions.AutoSize = true;
            this.cBoxStoreLastWaitingContributions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxStoreLastWaitingContributions.Location = new System.Drawing.Point(366, 178);
            this.cBoxStoreLastWaitingContributions.Name = "cBoxStoreLastWaitingContributions";
            this.cBoxStoreLastWaitingContributions.Size = new System.Drawing.Size(163, 17);
            this.cBoxStoreLastWaitingContributions.TabIndex = 35;
            this.cBoxStoreLastWaitingContributions.Text = "Store remaining Contributions";
            this.cBoxStoreLastWaitingContributions.UseVisualStyleBackColor = true;
            this.cBoxStoreLastWaitingContributions.Visible = false;
            // 
            // cBoxStoreProblematicRequests
            // 
            this.cBoxStoreProblematicRequests.AutoSize = true;
            this.cBoxStoreProblematicRequests.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxStoreProblematicRequests.Location = new System.Drawing.Point(366, 155);
            this.cBoxStoreProblematicRequests.Name = "cBoxStoreProblematicRequests";
            this.cBoxStoreProblematicRequests.Size = new System.Drawing.Size(157, 17);
            this.cBoxStoreProblematicRequests.TabIndex = 34;
            this.cBoxStoreProblematicRequests.Text = "Store Problematic Requests";
            this.cBoxStoreProblematicRequests.UseVisualStyleBackColor = true;
            this.cBoxStoreProblematicRequests.Visible = false;
            // 
            // labelSaveErrorTablesHelp
            // 
            this.labelSaveErrorTablesHelp.AutoSize = true;
            this.labelSaveErrorTablesHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSaveErrorTablesHelp.ForeColor = System.Drawing.Color.Firebrick;
            this.labelSaveErrorTablesHelp.Location = new System.Drawing.Point(327, 137);
            this.labelSaveErrorTablesHelp.Name = "labelSaveErrorTablesHelp";
            this.labelSaveErrorTablesHelp.Size = new System.Drawing.Size(33, 15);
            this.labelSaveErrorTablesHelp.TabIndex = 33;
            this.labelSaveErrorTablesHelp.Text = "Help";
            this.labelSaveErrorTablesHelp.Click += new System.EventHandler(this.labelSaveErrorTablesHelp_Click);
            // 
            // cBoxErrorDbSqlServer
            // 
            this.cBoxErrorDbSqlServer.AutoSize = true;
            this.cBoxErrorDbSqlServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxErrorDbSqlServer.Location = new System.Drawing.Point(422, 9);
            this.cBoxErrorDbSqlServer.Name = "cBoxErrorDbSqlServer";
            this.cBoxErrorDbSqlServer.Size = new System.Drawing.Size(100, 17);
            this.cBoxErrorDbSqlServer.TabIndex = 32;
            this.cBoxErrorDbSqlServer.Text = "MS SQL Server";
            this.cBoxErrorDbSqlServer.UseVisualStyleBackColor = true;
            // 
            // cBoxErrorDbPg
            // 
            this.cBoxErrorDbPg.AutoSize = true;
            this.cBoxErrorDbPg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxErrorDbPg.Location = new System.Drawing.Point(339, 9);
            this.cBoxErrorDbPg.Name = "cBoxErrorDbPg";
            this.cBoxErrorDbPg.Size = new System.Drawing.Size(83, 17);
            this.cBoxErrorDbPg.TabIndex = 32;
            this.cBoxErrorDbPg.Text = "PostgreSQL";
            this.cBoxErrorDbPg.UseVisualStyleBackColor = true;
            // 
            // btnExecStatementErrors
            // 
            this.btnExecStatementErrors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExecStatementErrors.Location = new System.Drawing.Point(383, 206);
            this.btnExecStatementErrors.Name = "btnExecStatementErrors";
            this.btnExecStatementErrors.Size = new System.Drawing.Size(123, 36);
            this.btnExecStatementErrors.TabIndex = 23;
            this.btnExecStatementErrors.Text = "Execute";
            this.btnExecStatementErrors.UseVisualStyleBackColor = true;
            this.btnExecStatementErrors.Click += new System.EventHandler(this.btnExecStatementErrors_Click);
            // 
            // btnCloseErrorDbConn
            // 
            this.btnCloseErrorDbConn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseErrorDbConn.Location = new System.Drawing.Point(383, 89);
            this.btnCloseErrorDbConn.Name = "btnCloseErrorDbConn";
            this.btnCloseErrorDbConn.Size = new System.Drawing.Size(123, 36);
            this.btnCloseErrorDbConn.TabIndex = 17;
            this.btnCloseErrorDbConn.Text = "Close Connection";
            this.btnCloseErrorDbConn.UseVisualStyleBackColor = true;
            this.btnCloseErrorDbConn.Click += new System.EventHandler(this.btnCloseErrorTableConn_Click);
            // 
            // btnOpenErrorDbConn
            // 
            this.btnOpenErrorDbConn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenErrorDbConn.Location = new System.Drawing.Point(383, 47);
            this.btnOpenErrorDbConn.Name = "btnOpenErrorDbConn";
            this.btnOpenErrorDbConn.Size = new System.Drawing.Size(123, 36);
            this.btnOpenErrorDbConn.TabIndex = 18;
            this.btnOpenErrorDbConn.Text = "Open Connection";
            this.btnOpenErrorDbConn.UseVisualStyleBackColor = true;
            this.btnOpenErrorDbConn.Click += new System.EventHandler(this.btnOpenErrorDbConn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 15);
            this.label4.TabIndex = 22;
            this.label4.Text = "SQL Statement";
            // 
            // txtBoxErrorSqlStatement
            // 
            this.txtBoxErrorSqlStatement.DetectUrls = false;
            this.txtBoxErrorSqlStatement.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtBoxErrorSqlStatement.Location = new System.Drawing.Point(9, 155);
            this.txtBoxErrorSqlStatement.Name = "txtBoxErrorSqlStatement";
            this.txtBoxErrorSqlStatement.Size = new System.Drawing.Size(351, 87);
            this.txtBoxErrorSqlStatement.TabIndex = 4;
            this.txtBoxErrorSqlStatement.Text = "INSERT INTO errorTable (composition, error_description) VALUES (@composition, @er" +
    "ror_desc);";
            // 
            // labelErrorTableInfo
            // 
            this.labelErrorTableInfo.AutoSize = true;
            this.labelErrorTableInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelErrorTableInfo.ForeColor = System.Drawing.Color.Firebrick;
            this.labelErrorTableInfo.Location = new System.Drawing.Point(327, 29);
            this.labelErrorTableInfo.Name = "labelErrorTableInfo";
            this.labelErrorTableInfo.Size = new System.Drawing.Size(33, 15);
            this.labelErrorTableInfo.TabIndex = 20;
            this.labelErrorTableInfo.Text = "Help";
            this.labelErrorTableInfo.Click += new System.EventHandler(this.labelErrorTableInfo_Click);
            // 
            // txtBoxErrorConnString
            // 
            this.txtBoxErrorConnString.DetectUrls = false;
            this.txtBoxErrorConnString.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtBoxErrorConnString.Location = new System.Drawing.Point(9, 47);
            this.txtBoxErrorConnString.Name = "txtBoxErrorConnString";
            this.txtBoxErrorConnString.Size = new System.Drawing.Size(351, 87);
            this.txtBoxErrorConnString.TabIndex = 1;
            this.txtBoxErrorConnString.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "ConnectionString:";
            // 
            // labelErrorDbConnected
            // 
            this.labelErrorDbConnected.AutoSize = true;
            this.labelErrorDbConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelErrorDbConnected.Location = new System.Drawing.Point(116, 29);
            this.labelErrorDbConnected.Name = "labelErrorDbConnected";
            this.labelErrorDbConnected.Size = new System.Drawing.Size(102, 15);
            this.labelErrorDbConnected.TabIndex = 16;
            this.labelErrorDbConnected.Text = "Connected / Error";
            this.labelErrorDbConnected.Visible = false;
            // 
            // labelPreviewRows
            // 
            this.labelPreviewRows.AutoSize = true;
            this.labelPreviewRows.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPreviewRows.Location = new System.Drawing.Point(180, 428);
            this.labelPreviewRows.Name = "labelPreviewRows";
            this.labelPreviewRows.Size = new System.Drawing.Size(46, 17);
            this.labelPreviewRows.TabIndex = 26;
            this.labelPreviewRows.Text = "Rows";
            // 
            // txtBoxNumOfRows
            // 
            this.txtBoxNumOfRows.Location = new System.Drawing.Point(130, 427);
            this.txtBoxNumOfRows.Name = "txtBoxNumOfRows";
            this.txtBoxNumOfRows.Size = new System.Drawing.Size(44, 20);
            this.txtBoxNumOfRows.TabIndex = 25;
            this.txtBoxNumOfRows.Text = "100";
            this.txtBoxNumOfRows.TextChanged += new System.EventHandler(this.txtBoxNumOfRows_TextChanged);
            // 
            // grBoxDbConnection
            // 
            this.grBoxDbConnection.Controls.Add(this.cBoxExcelfile);
            this.grBoxDbConnection.Controls.Add(this.labelHelpDbCon);
            this.grBoxDbConnection.Controls.Add(this.cBoxPostgres);
            this.grBoxDbConnection.Controls.Add(this.cBoxSqlServer);
            this.grBoxDbConnection.Controls.Add(this.txtBoxConnString);
            this.grBoxDbConnection.Controls.Add(this.labelDbServer);
            this.grBoxDbConnection.Controls.Add(this.btnOpenConnection);
            this.grBoxDbConnection.Controls.Add(this.labelConnected);
            this.grBoxDbConnection.Controls.Add(this.btnCloseConnection);
            this.grBoxDbConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxDbConnection.Location = new System.Drawing.Point(6, 3);
            this.grBoxDbConnection.Name = "grBoxDbConnection";
            this.grBoxDbConnection.Size = new System.Drawing.Size(528, 160);
            this.grBoxDbConnection.TabIndex = 24;
            this.grBoxDbConnection.TabStop = false;
            this.grBoxDbConnection.Text = "Database Connection";
            // 
            // cBoxExcelfile
            // 
            this.cBoxExcelfile.AutoSize = true;
            this.cBoxExcelfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxExcelfile.Location = new System.Drawing.Point(265, 9);
            this.cBoxExcelfile.Name = "cBoxExcelfile";
            this.cBoxExcelfile.Size = new System.Drawing.Size(68, 17);
            this.cBoxExcelfile.TabIndex = 34;
            this.cBoxExcelfile.Text = "Excel file";
            this.cBoxExcelfile.UseVisualStyleBackColor = true;
            // 
            // labelHelpDbCon
            // 
            this.labelHelpDbCon.AutoSize = true;
            this.labelHelpDbCon.BackColor = System.Drawing.Color.WhiteSmoke;
            this.labelHelpDbCon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHelpDbCon.ForeColor = System.Drawing.Color.Firebrick;
            this.labelHelpDbCon.Location = new System.Drawing.Point(344, 34);
            this.labelHelpDbCon.Name = "labelHelpDbCon";
            this.labelHelpDbCon.Size = new System.Drawing.Size(33, 15);
            this.labelHelpDbCon.TabIndex = 33;
            this.labelHelpDbCon.Text = "Help";
            this.labelHelpDbCon.Click += new System.EventHandler(this.labelHelpDbCon_Click);
            // 
            // cBoxPostgres
            // 
            this.cBoxPostgres.AutoSize = true;
            this.cBoxPostgres.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxPostgres.Location = new System.Drawing.Point(339, 9);
            this.cBoxPostgres.Name = "cBoxPostgres";
            this.cBoxPostgres.Size = new System.Drawing.Size(83, 17);
            this.cBoxPostgres.TabIndex = 30;
            this.cBoxPostgres.Text = "PostgreSQL";
            this.cBoxPostgres.UseVisualStyleBackColor = true;
            // 
            // cBoxSqlServer
            // 
            this.cBoxSqlServer.AutoSize = true;
            this.cBoxSqlServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxSqlServer.Location = new System.Drawing.Point(422, 9);
            this.cBoxSqlServer.Name = "cBoxSqlServer";
            this.cBoxSqlServer.Size = new System.Drawing.Size(100, 17);
            this.cBoxSqlServer.TabIndex = 31;
            this.cBoxSqlServer.Text = "MS SQL Server";
            this.cBoxSqlServer.UseVisualStyleBackColor = true;
            // 
            // labelConnected
            // 
            this.labelConnected.AutoSize = true;
            this.labelConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.Location = new System.Drawing.Point(116, 34);
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.Size = new System.Drawing.Size(102, 15);
            this.labelConnected.TabIndex = 16;
            this.labelConnected.Text = "Connected / Error";
            this.labelConnected.Visible = false;
            // 
            // btnCloseConnection
            // 
            this.btnCloseConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseConnection.Location = new System.Drawing.Point(383, 101);
            this.btnCloseConnection.Name = "btnCloseConnection";
            this.btnCloseConnection.Size = new System.Drawing.Size(123, 36);
            this.btnCloseConnection.TabIndex = 3;
            this.btnCloseConnection.Text = "Close Connection";
            this.btnCloseConnection.UseVisualStyleBackColor = true;
            this.btnCloseConnection.Click += new System.EventHandler(this.btnCloseConnection_Click);
            // 
            // labelPreview
            // 
            this.labelPreview.AutoSize = true;
            this.labelPreview.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPreview.Location = new System.Drawing.Point(8, 427);
            this.labelPreview.Name = "labelPreview";
            this.labelPreview.Size = new System.Drawing.Size(116, 17);
            this.labelPreview.TabIndex = 23;
            this.labelPreview.Text = "Preview of first";
            // 
            // btnExecQuery
            // 
            this.btnExecQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExecQuery.Location = new System.Drawing.Point(540, 29);
            this.btnExecQuery.Name = "btnExecQuery";
            this.btnExecQuery.Size = new System.Drawing.Size(93, 29);
            this.btnExecQuery.TabIndex = 5;
            this.btnExecQuery.Text = "Execute";
            this.btnExecQuery.UseVisualStyleBackColor = true;
            this.btnExecQuery.Click += new System.EventHandler(this.btnExecQuery_Click);
            // 
            // labelSqlQuery
            // 
            this.labelSqlQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSqlQuery.AutoSize = true;
            this.labelSqlQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSqlQuery.Location = new System.Drawing.Point(537, 8);
            this.labelSqlQuery.Name = "labelSqlQuery";
            this.labelSqlQuery.Size = new System.Drawing.Size(93, 17);
            this.labelSqlQuery.TabIndex = 21;
            this.labelSqlQuery.Text = "SQL Query:";
            // 
            // txtBoxSqlQuery
            // 
            this.txtBoxSqlQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxSqlQuery.DetectUrls = false;
            this.txtBoxSqlQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxSqlQuery.Location = new System.Drawing.Point(636, 8);
            this.txtBoxSqlQuery.Name = "txtBoxSqlQuery";
            this.txtBoxSqlQuery.Size = new System.Drawing.Size(692, 433);
            this.txtBoxSqlQuery.TabIndex = 4;
            this.txtBoxSqlQuery.Text = "";
            // 
            // tabPageInstructions
            // 
            this.tabPageInstructions.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageInstructions.Controls.Add(this.btnSaveInstructions);
            this.tabPageInstructions.Controls.Add(this.grBoxTransmitCons);
            this.tabPageInstructions.Controls.Add(this.grBoxShowExampleContribution);
            this.tabPageInstructions.Controls.Add(this.labelInstructionsString);
            this.tabPageInstructions.Controls.Add(this.txtBoxInstructionsString);
            this.tabPageInstructions.Controls.Add(this.grBoxThinkPlatform);
            this.tabPageInstructions.Controls.Add(this.btnLoadInstructions);
            this.tabPageInstructions.Controls.Add(this.labelShowExampleContributions);
            this.tabPageInstructions.Controls.Add(this.txtBoxDisplayExCon);
            this.tabPageInstructions.Location = new System.Drawing.Point(4, 22);
            this.tabPageInstructions.Name = "tabPageInstructions";
            this.tabPageInstructions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInstructions.Size = new System.Drawing.Size(1336, 655);
            this.tabPageInstructions.TabIndex = 1;
            this.tabPageInstructions.Text = "Instructions & Contributions";
            // 
            // btnSaveInstructions
            // 
            this.btnSaveInstructions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveInstructions.Location = new System.Drawing.Point(1023, 321);
            this.btnSaveInstructions.Name = "btnSaveInstructions";
            this.btnSaveInstructions.Size = new System.Drawing.Size(148, 25);
            this.btnSaveInstructions.TabIndex = 51;
            this.btnSaveInstructions.Text = "Save Instructions";
            this.btnSaveInstructions.UseVisualStyleBackColor = true;
            this.btnSaveInstructions.Click += new System.EventHandler(this.btnSaveInstructions_Click);
            // 
            // grBoxTransmitCons
            // 
            this.grBoxTransmitCons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grBoxTransmitCons.AutoSize = true;
            this.grBoxTransmitCons.Controls.Add(this.button2);
            this.grBoxTransmitCons.Controls.Add(this.cBoxIncludeInstructionsJSON);
            this.grBoxTransmitCons.Controls.Add(this.button1);
            this.grBoxTransmitCons.Controls.Add(this.btnLogInvalidPaths);
            this.grBoxTransmitCons.Controls.Add(this.label1);
            this.grBoxTransmitCons.Controls.Add(this.tBoxNrRequestStats);
            this.grBoxTransmitCons.Controls.Add(this.btnTransmitContributions);
            this.grBoxTransmitCons.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxTransmitCons.Location = new System.Drawing.Point(732, 186);
            this.grBoxTransmitCons.Name = "grBoxTransmitCons";
            this.grBoxTransmitCons.Size = new System.Drawing.Size(596, 127);
            this.grBoxTransmitCons.TabIndex = 50;
            this.grBoxTransmitCons.TabStop = false;
            this.grBoxTransmitCons.Text = "Load to Think!";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.button2.Location = new System.Drawing.Point(409, 43);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(181, 31);
            this.button2.TabIndex = 53;
            this.button2.Text = "Log request-stats now";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cBoxIncludeInstructionsJSON
            // 
            this.cBoxIncludeInstructionsJSON.AutoSize = true;
            this.cBoxIncludeInstructionsJSON.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxIncludeInstructionsJSON.Location = new System.Drawing.Point(31, 90);
            this.cBoxIncludeInstructionsJSON.Name = "cBoxIncludeInstructionsJSON";
            this.cBoxIncludeInstructionsJSON.Size = new System.Drawing.Size(230, 17);
            this.cBoxIncludeInstructionsJSON.TabIndex = 52;
            this.cBoxIncludeInstructionsJSON.Text = "Include mapping instructions in composition";
            this.cBoxIncludeInstructionsJSON.UseVisualStyleBackColor = true;
            this.cBoxIncludeInstructionsJSON.CheckedChanged += new System.EventHandler(this.cBoxIncludeInstructionsJSON_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.button1.Location = new System.Drawing.Point(215, 53);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 31);
            this.button1.TabIndex = 51;
            this.button1.Text = "Log open requests etc.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnLogInvalidPaths
            // 
            this.btnLogInvalidPaths.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogInvalidPaths.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnLogInvalidPaths.Location = new System.Drawing.Point(215, 16);
            this.btnLogInvalidPaths.Name = "btnLogInvalidPaths";
            this.btnLogInvalidPaths.Size = new System.Drawing.Size(141, 31);
            this.btnLogInvalidPaths.TabIndex = 50;
            this.btnLogInvalidPaths.Text = "Log invalid paths";
            this.btnLogInvalidPaths.UseVisualStyleBackColor = true;
            this.btnLogInvalidPaths.Click += new System.EventHandler(this.btnLogInvalidPaths_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(406, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Intermediate request-stats/min:";
            // 
            // tBoxNrRequestStats
            // 
            this.tBoxNrRequestStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxNrRequestStats.Location = new System.Drawing.Point(564, 16);
            this.tBoxNrRequestStats.Name = "tBoxNrRequestStats";
            this.tBoxNrRequestStats.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tBoxNrRequestStats.Size = new System.Drawing.Size(26, 21);
            this.tBoxNrRequestStats.TabIndex = 35;
            this.tBoxNrRequestStats.Text = "4";
            // 
            // btnTransmitContributions
            // 
            this.btnTransmitContributions.BackColor = System.Drawing.Color.LimeGreen;
            this.btnTransmitContributions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransmitContributions.Location = new System.Drawing.Point(6, 16);
            this.btnTransmitContributions.Name = "btnTransmitContributions";
            this.btnTransmitContributions.Size = new System.Drawing.Size(183, 68);
            this.btnTransmitContributions.TabIndex = 49;
            this.btnTransmitContributions.Text = "Transmit Contributions";
            this.btnTransmitContributions.UseVisualStyleBackColor = false;
            this.btnTransmitContributions.Click += new System.EventHandler(this.btnTransmitContributions_Click);
            // 
            // grBoxShowExampleContribution
            // 
            this.grBoxShowExampleContribution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grBoxShowExampleContribution.AutoSize = true;
            this.grBoxShowExampleContribution.Controls.Add(this.tBoxDummyEHRId);
            this.grBoxShowExampleContribution.Controls.Add(this.label3);
            this.grBoxShowExampleContribution.Controls.Add(this.txtBoxExampleConNum);
            this.grBoxShowExampleContribution.Controls.Add(this.label2);
            this.grBoxShowExampleContribution.Controls.Add(this.btnShowExampleContribution);
            this.grBoxShowExampleContribution.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxShowExampleContribution.Location = new System.Drawing.Point(516, 186);
            this.grBoxShowExampleContribution.Name = "grBoxShowExampleContribution";
            this.grBoxShowExampleContribution.Size = new System.Drawing.Size(210, 121);
            this.grBoxShowExampleContribution.TabIndex = 47;
            this.grBoxShowExampleContribution.TabStop = false;
            this.grBoxShowExampleContribution.Text = "Example Contributions";
            // 
            // tBoxDummyEHRId
            // 
            this.tBoxDummyEHRId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBoxDummyEHRId.Location = new System.Drawing.Point(10, 43);
            this.tBoxDummyEHRId.Name = "tBoxDummyEHRId";
            this.tBoxDummyEHRId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tBoxDummyEHRId.Size = new System.Drawing.Size(190, 21);
            this.tBoxDummyEHRId.TabIndex = 35;
            this.tBoxDummyEHRId.Text = "Dummy EHR Id";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(158, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Rows";
            // 
            // txtBoxExampleConNum
            // 
            this.txtBoxExampleConNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxExampleConNum.Location = new System.Drawing.Point(107, 21);
            this.txtBoxExampleConNum.Name = "txtBoxExampleConNum";
            this.txtBoxExampleConNum.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBoxExampleConNum.Size = new System.Drawing.Size(45, 21);
            this.txtBoxExampleConNum.TabIndex = 33;
            this.txtBoxExampleConNum.Text = "10";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Apply instructions to";
            // 
            // btnShowExampleContribution
            // 
            this.btnShowExampleContribution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowExampleContribution.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnShowExampleContribution.Location = new System.Drawing.Point(106, 70);
            this.btnShowExampleContribution.Name = "btnShowExampleContribution";
            this.btnShowExampleContribution.Size = new System.Drawing.Size(98, 31);
            this.btnShowExampleContribution.TabIndex = 30;
            this.btnShowExampleContribution.Text = "Show Example";
            this.btnShowExampleContribution.UseVisualStyleBackColor = true;
            this.btnShowExampleContribution.Click += new System.EventHandler(this.btnShowExampleContribution_Click);
            // 
            // labelInstructionsString
            // 
            this.labelInstructionsString.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelInstructionsString.AutoSize = true;
            this.labelInstructionsString.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInstructionsString.Location = new System.Drawing.Point(523, 331);
            this.labelInstructionsString.Name = "labelInstructionsString";
            this.labelInstructionsString.Size = new System.Drawing.Size(140, 17);
            this.labelInstructionsString.TabIndex = 46;
            this.labelInstructionsString.Text = "Instructions String";
            // 
            // txtBoxInstructionsString
            // 
            this.txtBoxInstructionsString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxInstructionsString.Location = new System.Drawing.Point(526, 351);
            this.txtBoxInstructionsString.Name = "txtBoxInstructionsString";
            this.txtBoxInstructionsString.Size = new System.Drawing.Size(807, 298);
            this.txtBoxInstructionsString.TabIndex = 30;
            this.txtBoxInstructionsString.Text = "";
            // 
            // grBoxThinkPlatform
            // 
            this.grBoxThinkPlatform.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grBoxThinkPlatform.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grBoxThinkPlatform.Controls.Add(this.txtBoxNamespaceTag);
            this.grBoxThinkPlatform.Controls.Add(this.labelNamespaceTag);
            this.grBoxThinkPlatform.Controls.Add(this.txtBoxThinkUser);
            this.grBoxThinkPlatform.Controls.Add(this.txtBoxThinkPwd);
            this.grBoxThinkPlatform.Controls.Add(this.labelThinkPwd);
            this.grBoxThinkPlatform.Controls.Add(this.labelThinkUser);
            this.grBoxThinkPlatform.Controls.Add(this.txtBoxTemplateId);
            this.grBoxThinkPlatform.Controls.Add(this.btnAddTemplateId);
            this.grBoxThinkPlatform.Controls.Add(this.labelThinkConnHTTP);
            this.grBoxThinkPlatform.Controls.Add(this.btnRemoveTemplateId);
            this.grBoxThinkPlatform.Controls.Add(this.cmbBoxTemplateId);
            this.grBoxThinkPlatform.Controls.Add(this.labelThinkTemplateID);
            this.grBoxThinkPlatform.Controls.Add(this.btnAddThinkAddress);
            this.grBoxThinkPlatform.Controls.Add(this.btnRemoveThinkAddress);
            this.grBoxThinkPlatform.Controls.Add(this.cmbBoxThinkAddress);
            this.grBoxThinkPlatform.Controls.Add(this.txtBoxThinkAddress);
            this.grBoxThinkPlatform.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBoxThinkPlatform.Location = new System.Drawing.Point(526, 6);
            this.grBoxThinkPlatform.Name = "grBoxThinkPlatform";
            this.grBoxThinkPlatform.Size = new System.Drawing.Size(802, 174);
            this.grBoxThinkPlatform.TabIndex = 43;
            this.grBoxThinkPlatform.TabStop = false;
            this.grBoxThinkPlatform.Text = "openEHR repository";
            // 
            // txtBoxNamespaceTag
            // 
            this.txtBoxNamespaceTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxNamespaceTag.Location = new System.Drawing.Point(472, 130);
            this.txtBoxNamespaceTag.Name = "txtBoxNamespaceTag";
            this.txtBoxNamespaceTag.Size = new System.Drawing.Size(230, 20);
            this.txtBoxNamespaceTag.TabIndex = 51;
            // 
            // labelNamespaceTag
            // 
            this.labelNamespaceTag.AutoSize = true;
            this.labelNamespaceTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNamespaceTag.Location = new System.Drawing.Point(385, 135);
            this.labelNamespaceTag.Name = "labelNamespaceTag";
            this.labelNamespaceTag.Size = new System.Drawing.Size(77, 15);
            this.labelNamespaceTag.TabIndex = 50;
            this.labelNamespaceTag.Text = "Namespace:";
            // 
            // txtBoxThinkUser
            // 
            this.txtBoxThinkUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxThinkUser.Location = new System.Drawing.Point(129, 100);
            this.txtBoxThinkUser.Name = "txtBoxThinkUser";
            this.txtBoxThinkUser.Size = new System.Drawing.Size(237, 20);
            this.txtBoxThinkUser.TabIndex = 28;
            // 
            // txtBoxThinkPwd
            // 
            this.txtBoxThinkPwd.Location = new System.Drawing.Point(472, 97);
            this.txtBoxThinkPwd.Name = "txtBoxThinkPwd";
            this.txtBoxThinkPwd.PasswordChar = '*';
            this.txtBoxThinkPwd.Size = new System.Drawing.Size(230, 23);
            this.txtBoxThinkPwd.TabIndex = 29;
            // 
            // labelThinkPwd
            // 
            this.labelThinkPwd.AutoSize = true;
            this.labelThinkPwd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelThinkPwd.Location = new System.Drawing.Point(398, 99);
            this.labelThinkPwd.Name = "labelThinkPwd";
            this.labelThinkPwd.Size = new System.Drawing.Size(64, 15);
            this.labelThinkPwd.TabIndex = 49;
            this.labelThinkPwd.Text = "Password:";
            // 
            // labelThinkUser
            // 
            this.labelThinkUser.AutoSize = true;
            this.labelThinkUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelThinkUser.Location = new System.Drawing.Point(27, 100);
            this.labelThinkUser.Name = "labelThinkUser";
            this.labelThinkUser.Size = new System.Drawing.Size(51, 15);
            this.labelThinkUser.TabIndex = 45;
            this.labelThinkUser.Text = "User ID:";
            // 
            // txtBoxTemplateId
            // 
            this.txtBoxTemplateId.DetectUrls = false;
            this.txtBoxTemplateId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxTemplateId.Location = new System.Drawing.Point(472, 62);
            this.txtBoxTemplateId.Multiline = false;
            this.txtBoxTemplateId.Name = "txtBoxTemplateId";
            this.txtBoxTemplateId.Size = new System.Drawing.Size(230, 27);
            this.txtBoxTemplateId.TabIndex = 26;
            this.txtBoxTemplateId.Text = "";
            // 
            // btnAddTemplateId
            // 
            this.btnAddTemplateId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnAddTemplateId.Location = new System.Drawing.Point(708, 60);
            this.btnAddTemplateId.Name = "btnAddTemplateId";
            this.btnAddTemplateId.Size = new System.Drawing.Size(51, 23);
            this.btnAddTemplateId.TabIndex = 27;
            this.btnAddTemplateId.Text = "Add";
            this.btnAddTemplateId.UseVisualStyleBackColor = true;
            this.btnAddTemplateId.Click += new System.EventHandler(this.btnAddTemplateId_Click);
            // 
            // labelThinkConnHTTP
            // 
            this.labelThinkConnHTTP.AutoSize = true;
            this.labelThinkConnHTTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelThinkConnHTTP.Location = new System.Drawing.Point(27, 32);
            this.labelThinkConnHTTP.Name = "labelThinkConnHTTP";
            this.labelThinkConnHTTP.Size = new System.Drawing.Size(96, 15);
            this.labelThinkConnHTTP.TabIndex = 1;
            this.labelThinkConnHTTP.Text = "REST API base :";
            // 
            // btnRemoveTemplateId
            // 
            this.btnRemoveTemplateId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnRemoveTemplateId.Location = new System.Drawing.Point(372, 62);
            this.btnRemoveTemplateId.Name = "btnRemoveTemplateId";
            this.btnRemoveTemplateId.Size = new System.Drawing.Size(79, 23);
            this.btnRemoveTemplateId.TabIndex = 25;
            this.btnRemoveTemplateId.Text = "Remove Item";
            this.btnRemoveTemplateId.UseVisualStyleBackColor = true;
            this.btnRemoveTemplateId.Click += new System.EventHandler(this.btnRemoveTemplateId_Click);
            // 
            // cmbBoxTemplateId
            // 
            this.cmbBoxTemplateId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxTemplateId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxTemplateId.FormattingEnabled = true;
            this.cmbBoxTemplateId.Location = new System.Drawing.Point(129, 62);
            this.cmbBoxTemplateId.MaxDropDownItems = 20;
            this.cmbBoxTemplateId.Name = "cmbBoxTemplateId";
            this.cmbBoxTemplateId.Size = new System.Drawing.Size(237, 21);
            this.cmbBoxTemplateId.Sorted = true;
            this.cmbBoxTemplateId.TabIndex = 24;
            this.cmbBoxTemplateId.SelectedIndexChanged += new System.EventHandler(this.cmbBoxTemplateId_SelectedIndexChanged);
            this.cmbBoxTemplateId.SelectionChangeCommitted += new System.EventHandler(this.cmbBoxTemplateId_SelectionChangeCommitted);
            this.cmbBoxTemplateId.TextUpdate += new System.EventHandler(this.cmbBoxTemplateId_SelectionChangeCommitted);
            // 
            // labelThinkTemplateID
            // 
            this.labelThinkTemplateID.AutoSize = true;
            this.labelThinkTemplateID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelThinkTemplateID.Location = new System.Drawing.Point(27, 63);
            this.labelThinkTemplateID.Name = "labelThinkTemplateID";
            this.labelThinkTemplateID.Size = new System.Drawing.Size(77, 15);
            this.labelThinkTemplateID.TabIndex = 40;
            this.labelThinkTemplateID.Text = "Template ID:";
            // 
            // btnAddThinkAddress
            // 
            this.btnAddThinkAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnAddThinkAddress.Location = new System.Drawing.Point(708, 29);
            this.btnAddThinkAddress.Name = "btnAddThinkAddress";
            this.btnAddThinkAddress.Size = new System.Drawing.Size(51, 23);
            this.btnAddThinkAddress.TabIndex = 10;
            this.btnAddThinkAddress.Text = "Add";
            this.btnAddThinkAddress.UseVisualStyleBackColor = true;
            this.btnAddThinkAddress.Click += new System.EventHandler(this.btnAddThinkAddress_Click);
            // 
            // btnRemoveThinkAddress
            // 
            this.btnRemoveThinkAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnRemoveThinkAddress.Location = new System.Drawing.Point(372, 31);
            this.btnRemoveThinkAddress.Name = "btnRemoveThinkAddress";
            this.btnRemoveThinkAddress.Size = new System.Drawing.Size(79, 23);
            this.btnRemoveThinkAddress.TabIndex = 8;
            this.btnRemoveThinkAddress.Text = "Remove Item";
            this.btnRemoveThinkAddress.UseVisualStyleBackColor = true;
            this.btnRemoveThinkAddress.Click += new System.EventHandler(this.btnRemoveThinkAddress_Click);
            // 
            // cmbBoxThinkAddress
            // 
            this.cmbBoxThinkAddress.AccessibleName = "";
            this.cmbBoxThinkAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxThinkAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxThinkAddress.FormattingEnabled = true;
            this.cmbBoxThinkAddress.Location = new System.Drawing.Point(129, 31);
            this.cmbBoxThinkAddress.MaxDropDownItems = 20;
            this.cmbBoxThinkAddress.Name = "cmbBoxThinkAddress";
            this.cmbBoxThinkAddress.Size = new System.Drawing.Size(237, 21);
            this.cmbBoxThinkAddress.Sorted = true;
            this.cmbBoxThinkAddress.TabIndex = 7;
            this.cmbBoxThinkAddress.SelectedIndexChanged += new System.EventHandler(this.cmbBoxThinkAddress_SelectedIndexChanged);
            this.cmbBoxThinkAddress.SelectionChangeCommitted += new System.EventHandler(this.cmbBoxThinkAddress_SelectionChangeCommitted);
            this.cmbBoxThinkAddress.TextUpdate += new System.EventHandler(this.cmbBoxThinkAddress_SelectionChangeCommitted);
            // 
            // txtBoxThinkAddress
            // 
            this.txtBoxThinkAddress.DetectUrls = false;
            this.txtBoxThinkAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxThinkAddress.Location = new System.Drawing.Point(472, 31);
            this.txtBoxThinkAddress.Multiline = false;
            this.txtBoxThinkAddress.Name = "txtBoxThinkAddress";
            this.txtBoxThinkAddress.Size = new System.Drawing.Size(230, 27);
            this.txtBoxThinkAddress.TabIndex = 9;
            this.txtBoxThinkAddress.Text = "e.g.:  http://134.169.38.57:8081/rest/v1/";
            // 
            // btnLoadInstructions
            // 
            this.btnLoadInstructions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadInstructions.Location = new System.Drawing.Point(1177, 320);
            this.btnLoadInstructions.Name = "btnLoadInstructions";
            this.btnLoadInstructions.Size = new System.Drawing.Size(153, 26);
            this.btnLoadInstructions.TabIndex = 32;
            this.btnLoadInstructions.Text = "Load Instructions from File";
            this.btnLoadInstructions.UseVisualStyleBackColor = true;
            this.btnLoadInstructions.Click += new System.EventHandler(this.btnLoadInstructions_Click);
            // 
            // labelShowExampleContributions
            // 
            this.labelShowExampleContributions.AutoSize = true;
            this.labelShowExampleContributions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.labelShowExampleContributions.Location = new System.Drawing.Point(8, 3);
            this.labelShowExampleContributions.Name = "labelShowExampleContributions";
            this.labelShowExampleContributions.Size = new System.Drawing.Size(174, 17);
            this.labelShowExampleContributions.TabIndex = 1;
            this.labelShowExampleContributions.Text = "Example Contributions:";
            // 
            // txtBoxDisplayExCon
            // 
            this.txtBoxDisplayExCon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxDisplayExCon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxDisplayExCon.Location = new System.Drawing.Point(3, 23);
            this.txtBoxDisplayExCon.Name = "txtBoxDisplayExCon";
            this.txtBoxDisplayExCon.Size = new System.Drawing.Size(485, 626);
            this.txtBoxDisplayExCon.TabIndex = 31;
            this.txtBoxDisplayExCon.Text = "";
            // 
            // tabPageMapping
            // 
            this.tabPageMapping.Location = new System.Drawing.Point(4, 22);
            this.tabPageMapping.Name = "tabPageMapping";
            this.tabPageMapping.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMapping.Size = new System.Drawing.Size(1336, 655);
            this.tabPageMapping.TabIndex = 4;
            this.tabPageMapping.Text = "Mapping Options";
            this.tabPageMapping.UseVisualStyleBackColor = true;
            this.tabPageMapping.Click += new System.EventHandler(this.tabPageMapping_Click);
            // 
            // tabPageDeleter
            // 
            this.tabPageDeleter.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageDeleter.Controls.Add(this.btnAQLForCompositions);
            this.tabPageDeleter.Controls.Add(this.btnAQLEHRs);
            this.tabPageDeleter.Controls.Add(this.btnRetrieveIds);
            this.tabPageDeleter.Controls.Add(this.label7);
            this.tabPageDeleter.Controls.Add(this.lblDeleteType);
            this.tabPageDeleter.Controls.Add(this.txtBoxDeleteAmount);
            this.tabPageDeleter.Controls.Add(this.rTxtBoxDeleting);
            this.tabPageDeleter.Controls.Add(this.btnDelete);
            this.tabPageDeleter.Location = new System.Drawing.Point(4, 22);
            this.tabPageDeleter.Name = "tabPageDeleter";
            this.tabPageDeleter.Size = new System.Drawing.Size(1336, 655);
            this.tabPageDeleter.TabIndex = 5;
            this.tabPageDeleter.Text = "Deleting";
            // 
            // btnAQLForCompositions
            // 
            this.btnAQLForCompositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnAQLForCompositions.Location = new System.Drawing.Point(8, 14);
            this.btnAQLForCompositions.Name = "btnAQLForCompositions";
            this.btnAQLForCompositions.Size = new System.Drawing.Size(120, 31);
            this.btnAQLForCompositions.TabIndex = 57;
            this.btnAQLForCompositions.Text = "AQL for compositions";
            this.btnAQLForCompositions.UseVisualStyleBackColor = true;
            this.btnAQLForCompositions.Click += new System.EventHandler(this.btnAQLForCompositions_Click);
            // 
            // btnAQLEHRs
            // 
            this.btnAQLEHRs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnAQLEHRs.Location = new System.Drawing.Point(133, 14);
            this.btnAQLEHRs.Name = "btnAQLEHRs";
            this.btnAQLEHRs.Size = new System.Drawing.Size(88, 31);
            this.btnAQLEHRs.TabIndex = 56;
            this.btnAQLEHRs.Text = "AQL for EHRs";
            this.btnAQLEHRs.UseVisualStyleBackColor = true;
            this.btnAQLEHRs.Click += new System.EventHandler(this.btnAQLEHRs_Click);
            // 
            // btnRetrieveIds
            // 
            this.btnRetrieveIds.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnRetrieveIds.Location = new System.Drawing.Point(249, 14);
            this.btnRetrieveIds.Name = "btnRetrieveIds";
            this.btnRetrieveIds.Size = new System.Drawing.Size(141, 31);
            this.btnRetrieveIds.TabIndex = 55;
            this.btnRetrieveIds.Text = "Retrieve Ids by AQL query";
            this.btnRetrieveIds.UseVisualStyleBackColor = true;
            this.btnRetrieveIds.Click += new System.EventHandler(this.btnRetrieveIds_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(405, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(213, 13);
            this.label7.TabIndex = 54;
            this.label7.Text = "If you press delete, you will physically delete";
            // 
            // lblDeleteType
            // 
            this.lblDeleteType.AutoSize = true;
            this.lblDeleteType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteType.Location = new System.Drawing.Point(667, 32);
            this.lblDeleteType.Name = "lblDeleteType";
            this.lblDeleteType.Size = new System.Drawing.Size(35, 13);
            this.lblDeleteType.TabIndex = 53;
            this.lblDeleteType.Text = "EHRs";
            // 
            // txtBoxDeleteAmount
            // 
            this.txtBoxDeleteAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxDeleteAmount.Location = new System.Drawing.Point(624, 24);
            this.txtBoxDeleteAmount.Name = "txtBoxDeleteAmount";
            this.txtBoxDeleteAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBoxDeleteAmount.Size = new System.Drawing.Size(37, 21);
            this.txtBoxDeleteAmount.TabIndex = 52;
            this.txtBoxDeleteAmount.Text = "0";
            // 
            // rTxtBoxDeleting
            // 
            this.rTxtBoxDeleting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rTxtBoxDeleting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rTxtBoxDeleting.Location = new System.Drawing.Point(8, 51);
            this.rTxtBoxDeleting.Name = "rTxtBoxDeleting";
            this.rTxtBoxDeleting.Size = new System.Drawing.Size(897, 535);
            this.rTxtBoxDeleting.TabIndex = 51;
            this.rTxtBoxDeleting.Text = "select e/ehr_id/value as ehrid from EHR e";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Red;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(770, 14);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(135, 34);
            this.btnDelete.TabIndex = 50;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // openFileTxtInstructions
            // 
            this.openFileTxtInstructions.FileName = "instructions.txt";
            // 
            // saveInstructions
            // 
            this.saveInstructions.FileName = "Instructions.txt";
            // 
            // ETLBuilder_Initial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 681);
            this.Controls.Add(this.tabControlEtlBuilder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ETLBuilder_Initial";
            this.Text = "HaMSTR ETL Builder";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ETLBuilder_Initial_FormClosing);
            this.Load += new System.EventHandler(this.ETLBuilder_Initial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridRelData)).EndInit();
            this.tabControlEtlBuilder.ResumeLayout(false);
            this.tabPageDbConnection.ResumeLayout(false);
            this.tabPageDbConnection.PerformLayout();
            this.grBoxErrorTableConnection.ResumeLayout(false);
            this.grBoxErrorTableConnection.PerformLayout();
            this.grBoxDbConnection.ResumeLayout(false);
            this.grBoxDbConnection.PerformLayout();
            this.tabPageInstructions.ResumeLayout(false);
            this.tabPageInstructions.PerformLayout();
            this.grBoxTransmitCons.ResumeLayout(false);
            this.grBoxTransmitCons.PerformLayout();
            this.grBoxShowExampleContribution.ResumeLayout(false);
            this.grBoxShowExampleContribution.PerformLayout();
            this.grBoxThinkPlatform.ResumeLayout(false);
            this.grBoxThinkPlatform.PerformLayout();
            this.tabPageDeleter.ResumeLayout(false);
            this.tabPageDeleter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsRelData)).EndInit();
            this.ResumeLayout(false);

        }

        private void tabPageMapping_Click(object sender, EventArgs e)
        {
            
        }

        private void cmbBoxThinkAddress_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cmbBoxTemplateId_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        #endregion

        private System.Windows.Forms.DataGridView gridRelData;
        private System.Windows.Forms.Label labelDbServer;
        public System.Windows.Forms.Button btnOpenConnection;
        private System.Windows.Forms.RichTextBox txtBoxConnString;
        private System.Windows.Forms.TabControl tabControlEtlBuilder;
        private System.Windows.Forms.TabPage tabPageDbConnection;
        private System.Windows.Forms.TabPage tabPageInstructions;
        private System.Windows.Forms.Label labelConnected;
        public System.Windows.Forms.Button btnCloseConnection;
        private System.Windows.Forms.Label labelPreview;
        public System.Windows.Forms.Button btnExecQuery;
        private System.Windows.Forms.Label labelSqlQuery;
        private System.Windows.Forms.RichTextBox txtBoxSqlQuery;
        private System.Windows.Forms.BindingSource bsRelData;
        private System.Windows.Forms.GroupBox grBoxDbConnection;
        private System.Windows.Forms.Label labelThinkConnHTTP;
        private System.Windows.Forms.RichTextBox txtBoxThinkAddress;
        private System.Windows.Forms.Button btnAddThinkAddress;
        private System.Windows.Forms.ComboBox cmbBoxThinkAddress;
        private System.Windows.Forms.Button btnRemoveThinkAddress;
        private System.Windows.Forms.RichTextBox txtBoxInstructionsString;
        private System.Windows.Forms.Button btnLoadInstructions;
        private System.Windows.Forms.OpenFileDialog openFileTxtInstructions;
        private System.Windows.Forms.Button btnRemoveTemplateId;
        private System.Windows.Forms.ComboBox cmbBoxTemplateId;
        private System.Windows.Forms.Label labelThinkTemplateID;
        private System.Windows.Forms.GroupBox grBoxThinkPlatform;
        private System.Windows.Forms.RichTextBox txtBoxTemplateId;
        private System.Windows.Forms.Button btnAddTemplateId;
        private System.Windows.Forms.Label labelThinkUser;
        private System.Windows.Forms.Label labelThinkPwd;
        private System.Windows.Forms.TextBox txtBoxThinkPwd;
        private System.Windows.Forms.TextBox txtBoxThinkUser;
        private System.Windows.Forms.TabPage tabPageMapping;
        private System.Windows.Forms.Label labelInstructionsString;
        private System.Windows.Forms.Label labelPreviewRows;
        private System.Windows.Forms.TextBox txtBoxNumOfRows;
        private System.Windows.Forms.Label labelShowExampleContributions;
        private System.Windows.Forms.RichTextBox txtBoxDisplayExCon;
        private System.Windows.Forms.GroupBox grBoxShowExampleContribution;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxExampleConNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnShowExampleContribution;
        private System.Windows.Forms.GroupBox grBoxTransmitCons;
        private System.Windows.Forms.Button btnTransmitContributions;
        private System.Windows.Forms.GroupBox grBoxErrorTableConnection;
        public System.Windows.Forms.Button btnExecStatementErrors;
        public System.Windows.Forms.Button btnCloseErrorDbConn;
        public System.Windows.Forms.Button btnOpenErrorDbConn;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.RichTextBox txtBoxErrorSqlStatement;
        private System.Windows.Forms.Label labelErrorTableInfo;
        private System.Windows.Forms.RichTextBox txtBoxErrorConnString;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelErrorDbConnected;
        private System.Windows.Forms.TextBox txtBoxNamespaceTag;
        private System.Windows.Forms.Label labelNamespaceTag;
        private System.Windows.Forms.CheckBox cBoxErrorDbSqlServer;
        private System.Windows.Forms.CheckBox cBoxErrorDbPg;
        private System.Windows.Forms.Label labelHelpDbCon;
        private System.Windows.Forms.CheckBox cBoxPostgres;
        private System.Windows.Forms.CheckBox cBoxSqlServer;
        private System.Windows.Forms.Label labelSaveErrorTablesHelp;
        private System.Windows.Forms.Button btnSaveInstructions;
        private System.Windows.Forms.SaveFileDialog saveInstructions;
        private System.Windows.Forms.CheckBox cBoxStoreLastWaitingContributions;
        private System.Windows.Forms.CheckBox cBoxStoreProblematicRequests;
        private System.Windows.Forms.CheckBox cBoxExcelfile;
        private System.Windows.Forms.CheckBox cBoxErrorLog;
        public System.Windows.Forms.Button btnLoadProject;
        public System.Windows.Forms.Button btnSaveProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tBoxNrRequestStats;
        private System.Windows.Forms.Button btnLogInvalidPaths;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPageDeleter;
        private System.Windows.Forms.Label lblDeleteType;
        private System.Windows.Forms.TextBox txtBoxDeleteAmount;
        private System.Windows.Forms.RichTextBox rTxtBoxDeleting;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRetrieveIds;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAQLForCompositions;
        private System.Windows.Forms.Button btnAQLEHRs;
        private System.Windows.Forms.TextBox tBoxDummyEHRId;
        private System.Windows.Forms.CheckBox cBoxIncludeInstructionsJSON;
        private System.Windows.Forms.Button button2;
    }
}


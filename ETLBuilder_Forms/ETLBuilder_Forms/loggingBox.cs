﻿using System.Windows.Forms;

namespace HaMSTR_ETLBuilder_Forms
{
    public partial class loggingBox : Form
    {
        public loggingBox(string startText)
        {
            InitializeComponent();
            this.inputText = startText;
        }

        /// <summary>
        /// Legt die Benutzereingabe fest oder gibt diese zurück
        /// </summary>
        public string inputText
        {
            get { return txtLogInfo.Text; }
            set { txtLogInfo.AppendText(value); }
        }
    }
}

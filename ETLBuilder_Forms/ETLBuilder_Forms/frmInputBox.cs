﻿using System.Windows.Forms;

namespace HaMSTR_ETLBuilder_Forms
{
    public partial class frmInputBox : Form
    {
        public frmInputBox(string description, string windowTitle, string defaultInput, bool showTextBox)
        {
            InitializeComponent();
            this.description = description;
            this.windowTitle = windowTitle;
            txtBoxInputDialog.Text = defaultInput;
            if (!showTextBox) { txtBoxInputDialog.Visible = false; }
        }
        /// <summary>
        /// Legt den Fenstertext fest oder gibt diesen zurück.
        /// </summary>
        public string windowTitle
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        /// <summary>
        /// Legt die Beschreibung, die über dem Eingabefeld erscheint, fest oder gibt diese zurück.
        /// </summary>
        public string description
        {
            get { return labelInputDescription.Text; }
            set { labelInputDescription.Text = value; }
        }

        /// <summary>
        /// Legt die Benutzereingabe fest oder gibt diese zurück
        /// </summary>
        public string inputText
        {
            get { return txtBoxInputDialog.Text; }
            set { txtBoxInputDialog.Text = value; }
        }

        private void btnInputDialogCancel_Click(object sender, System.EventArgs e)
        {
            txtBoxInputDialog.Text = "";
        }
    }
}

﻿#region Namespaces
using CefSharp;
using CefSharp.WinForms;
using Devart.Data.PostgreSql;
using HaMSTR_ETLBuilder_Forms.Properties;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Forms;
#endregion

namespace HaMSTR_ETLBuilder_Forms
{
    public partial class ETLBuilder_Initial : Form
    {

        #region Initialize, Load, Close Form
        public ETLBuilder_Initial()
        {
            //
            // Erforderlich für die Windows Form-Designerunterstützung
            InitializeComponent();
            initializeBrowser();
        }

        //Verhalten beim Laden der Form
        private void ETLBuilder_Initial_Load(object sender, EventArgs e)
        {
            //Hiermit kann ohne vorheriges Anklicken des Tabs ein Zugriff auf die dort enthaltenen Controls ermöglicht werden
            tabPageInstructions.Show();
            tabPageMapping.Show();

            //User Settings zu ComboBox hinzufügen bei jedem Laden des Tools
            addItemsToComboBox(cmbBoxThinkAddress, Settings.Default.thinkAddress);
            addItemsToComboBox(cmbBoxTemplateId, Settings.Default.thinkTemplateId);

            //Dictionary für die Einstellungen bzgl Think-Plattform vorbefüllen
            initializeThinkParams();

            //just some lazy dev stuff 
            /*Thread.Sleep(1000);
            load_project_file(Directory.GetCurrentDirectory()+ "\\project.json", Directory.GetCurrentDirectory());
            Thread.Sleep(1000);
            btnExecQuery_Click(null, null);*/
            //myBrowser.ShowDevTools();
        }

        //Verhalten beim Schließen der Form (tritt ein, bevor die Form tatsächlich geschlossen wird)
        private void ETLBuilder_Initial_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Hier: Cefsharp Browser schließen, bevor die Applikation geschlossen wird
            Cef.Shutdown();
            //Alle Änderungen durch den Benutzer der ComboBox-Items sichern
            Settings.Default.Save();            
        }

        #region allgemeine Methoden 

        //Kurze Methode um UserSettings zu ComboBox hinzuzufügen
        public void addItemsToComboBox(ComboBox cmbBox, StringCollection strCol)
        {
            try
            {
                if (strCol != null)
                {
                    foreach (object tmp in strCol)
                    {
                        cmbBox.Items.Add(tmp.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to add Items to the Combo Box: " + ex.Message);
            }
        }

        //Methode um hinzugefügte ComboBoxItems zu sichern
        public void saveNewComboBoxItems(ComboBox cmbBox, StringCollection strCol, string newItem)
        {
            try
            {
                cmbBox.Items.Add(newItem);
                strCol.Add(newItem);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while trying to save new ComboBox Item: " + ex.Message);
            }
        }

        //Methode um entfernte ComboBoxItems persistent zu entfernen
        public void removeComboBoxItems(ComboBox cmbBox, StringCollection strCol, object selectedItem)
        {
            try
            {
                cmbBox.Items.Remove(selectedItem);
                strCol.Remove(selectedItem.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while trying to remove ComboBox Item: " + ex.Message);
            }
        }

        //Methode, um den Browser bei Initialisierung der Form ebenfalls zu initialisieren
        public void initializeBrowser()
        {
            try
            {
                //IsInitialized benötigt, um sicherzugehen, dass auch nur einmal initialisiert wird
                //Sollte es fehlen, wird eine Exception ausgelöst
                if (!Cef.IsInitialized)
                {
                    Cef.Initialize();
                    Thread.Sleep(500);
                    string curDir = Directory.GetCurrentDirectory();
                    ChromiumWebBrowser myBrowser = new ChromiumWebBrowser(String.Format("file:///{0}/HTML/index.html", curDir));
                    Thread.Sleep(500);
                    tabPageMapping.Controls.Add(myBrowser);
                }
                else if (!tabPageMapping.Contains(myBrowser))
                {
                    tabPageMapping.Controls.Add(myBrowser);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fehler beim Aufruf: " + ex.Message, "Cefsharp Browser");
            }
        }

        //Methode zum Hinzufügen von Parametern der Think-Plattform
        public void initializeThinkParams()
        {
            thinkParams.TryAdd("thinkAddress", "");
            thinkParams.TryAdd("thinkTemplateId", "");
        }
        #endregion

        #endregion


        #region initialize classes

        dbConnect dbConnection = new dbConnect();
        dbConnect conStoreErrors = new dbConnect();

        ServerResponseHandling responseHandle = null;
        RepoHandle repoHandle = null;
        ContributionsHandler contributionsHandler = null;

        ChromiumWebBrowser myBrowser = new ChromiumWebBrowser(String.Format("file:///{0}/HTML/index.html", Directory.GetCurrentDirectory()));

        public ConcurrentDictionary<string, string> problematicRequests = new ConcurrentDictionary<string, string>();
        public ConcurrentDictionary<Contribution, string> notSentContributions = new ConcurrentDictionary<Contribution, string>();

        //Dictionary für Einstellungen bzgl. der Think-Plattform (bleibt bei Änderungen stets up-to-date
        ConcurrentDictionary<string, string> thinkParams = new ConcurrentDictionary<string, string>();

        #endregion


        #region anything connected to DB Connections

        // Button to open connection to DB
        private void btnOpenConnection_Click(object sender, EventArgs e)
        {
            try
            {
                if (cBoxPostgres.Checked)
                {
                    dbConnection.openConnection(txtBoxConnString.Text, true);
                    if (dbConnection.pgIsOpen)
                    {
                        labelConnected.Text = "Connected";
                        labelConnected.ForeColor = Color.Green;
                        labelConnected.Show();
                    }
                    else
                    {
                        labelConnected.Text = "Something went wrong. No connection";
                        labelConnected.ForeColor = Color.Red;
                        labelConnected.Show();
                    }
                }
                else if (cBoxSqlServer.Checked)
                {
                    dbConnection.openConnection(txtBoxConnString.Text, false);
                    if (dbConnection.sqlServerIsOpen)
                    {
                        labelConnected.Text = "Connected";
                        labelConnected.ForeColor = Color.Green;
                        labelConnected.Show();
                    }
                    else
                    {
                        labelConnected.Text = "Something went wrong. No connection";
                        labelConnected.ForeColor = Color.Red;
                        labelConnected.Show();
                    }
                }
                else if (cBoxExcelfile.Checked) //if flatfile, then simply check if file exists
                {
                    if (File.Exists(txtBoxConnString.Text))
                    {
                        labelConnected.Text = "File exists";
                        labelConnected.ForeColor = Color.Green;
                        labelConnected.Show();
                    }
                    else
                    {
                        labelConnected.Text = "Something went wrong. File doesn't exist.";
                        labelConnected.ForeColor = Color.Red;
                        labelConnected.Show();
                    }
                }
            } catch (Exception ex)
            {
                MessageBox.Show("Error while trying to open a connection to the DB: " + ex.Message);
            }

        }
        // Button to Close Connection to DB
        private void btnCloseConnection_Click(object sender, EventArgs e)
        {
            try
            {
                dbConnection.closeConnection();
                labelConnected.Text = "Closed connection";
                labelConnected.ForeColor = Color.Gray;
                labelConnected.Show();
            } catch (Exception ex)
            {
                MessageBox.Show("Error while trying to close the connection to the DB: " + ex.Message);
            }
        }

        // Button to execute SQL Query and extract relational Data
        private void btnExecQuery_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cBoxExcelfile.Checked)
                {
                    if (!dbConnection.pgIsOpen && !dbConnection.sqlServerIsOpen) { MessageBox.Show("No DB connection is open!"); return; }

                    dbConnection.executeQuery(txtBoxSqlQuery.Text);

                    int tmp = int.Parse(txtBoxNumOfRows.Text);

                    bsRelData.DataSource = dbConnection.previewDataTable(dbConnection.dt, tmp);
                    gridRelData.Refresh();
                    gridRelData.DataSource = bsRelData;
                }
                //if flatfile, then load preview from file
                else
                {
                    String constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + txtBoxConnString.Text + ";Extended Properties='Excel 12.0 XML;HDR=YES;';";
                    String queryStr = txtBoxSqlQuery.Text;
                    if (txtBoxConnString.Text.IndexOf(".csv") > -1)
                    {
                        string path = Path.GetDirectoryName(txtBoxConnString.Text);
                        string filename = Path.GetFileName(txtBoxConnString.Text);

                        constructSchema(path,filename);

                        constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='text;HDR=YES;';";
                        queryStr = "SELECT * FROM [" + filename + "]";
                    }
                    OleDbConnection con = new OleDbConnection(constr);
                    OleDbCommand oconn = new OleDbCommand(queryStr, con);
                    con.Open();
                    OleDbDataAdapter dataAdapter = new OleDbDataAdapter(oconn);
                    dbConnection.dt = new DataTable();
                    dataAdapter.Fill(dbConnection.dt);
                    txtBoxNumOfRows.Text = dbConnection.dt.Rows.Count.ToString(); //set the amount of rows to flatfiles length //TODO maybe catch too big files some day?
                    gridRelData.DataSource = dbConnection.dt;
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to execute Query or display Data" + Environment.NewLine + "Error: " + ex.Message);
            }


            void constructSchema(string path, string filename)
            {
                StringBuilder schema = new StringBuilder();
                DataTable data = loadCSV(path, filename);
                schema.AppendLine("[" + filename + "]");
                schema.AppendLine("ColNameHeader=True");
                schema.AppendLine("Format = Delimited(;)");
                for (int i = 0; i < data.Columns.Count; i++)
                {
                    schema.AppendLine("col" + (i + 1).ToString() + "=\"" + data.Columns[i].ColumnName + "\" Text");
                }
                string schemaFileName = path + @"\Schema.ini";
                TextWriter tw = new StreamWriter(schemaFileName);
                tw.WriteLine(schema.ToString());
                tw.Close();
            }

            DataTable loadCSV(string path, string filename)
            {
                File.AppendAllText(path + "/schema.ini", "[" + filename + "]" + Environment.NewLine + " Format = Delimited(;)" + Environment.NewLine);

                string sqlString = "Select * FROM [" + filename + "]";
                string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='text;HDR=YES;';";
                DataTable theCSV = new DataTable();

                using (OleDbConnection conn = new OleDbConnection(constr))
                {
                    using (OleDbCommand comm = new OleDbCommand(sqlString, conn))
                    {
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(comm))
                        {
                            adapter.Fill(theCSV);
                        }
                    }
                }
                return theCSV;
            }
        }

        //Check if numOfRows changes
        private void txtBoxNumOfRows_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dbConnection.dt.Rows.Count > 0)
                {
                    gridRelData.Refresh();
                    int tmp;

                    if (txtBoxNumOfRows.Text == "")
                    {
                        tmp = 0;
                    }
                    else
                    {
                        tmp = int.Parse(txtBoxNumOfRows.Text);
                    }
                    bsRelData.DataSource = dbConnection.previewDataTable(dbConnection.dt, tmp);
                    gridRelData.DataSource = bsRelData;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
        }
        //Open DB Connection for response and error handling
        private void btnOpenErrorDbConn_Click(object sender, EventArgs e)
        {
            try
            {
                if (cBoxErrorDbPg.Checked && cBoxErrorDbSqlServer.Checked) { MessageBox.Show("Please choose either Postgres or SQL Server - not both!"); return; }

                if (cBoxErrorDbPg.Checked)
                {
                    conStoreErrors.openConnection(txtBoxErrorConnString.Text, true);
                    if (conStoreErrors.pgIsOpen)
                    {
                        labelErrorDbConnected.Text = "Connected";
                        labelErrorDbConnected.ForeColor = Color.Green;
                        labelErrorDbConnected.Show();
                    }
                    else
                    {
                        labelErrorDbConnected.Text = "Something went wrong. No connection";
                        labelErrorDbConnected.ForeColor = Color.Red;
                        labelErrorDbConnected.Show();
                    }
                }

                if (cBoxErrorDbSqlServer.Checked)
                {
                    conStoreErrors.openConnection(txtBoxErrorConnString.Text, false);
                    if (conStoreErrors.sqlServerIsOpen)
                    {
                        labelErrorDbConnected.Text = "Connected";
                        labelErrorDbConnected.ForeColor = Color.Green;
                        labelErrorDbConnected.Show();
                    }
                    else
                    {
                        labelErrorDbConnected.Text = "Something went wrong. No connection";
                        labelErrorDbConnected.ForeColor = Color.Red;
                        labelErrorDbConnected.Show();
                    }
                }

                if (cBoxErrorLog.Checked)
                {
                    if (Directory.Exists(Directory.GetCurrentDirectory() + "/Log"))
                    {
                        labelErrorDbConnected.Text = "Folder \"Log\" exists";
                        labelErrorDbConnected.ForeColor = Color.Green;
                        labelErrorDbConnected.Show();
                    }
                    else
                    {
                        labelErrorDbConnected.Text = "Something went wrong. Folder Log doesn't exist.";
                        labelErrorDbConnected.ForeColor = Color.Red;
                        labelErrorDbConnected.Show();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while trying to open a connection to the DB for storing errors: " + ex.Message);
            }
        }

        //Close DB Connection for response and error handling
        private void btnCloseErrorTableConn_Click(object sender, EventArgs e)
        {
            try
            {
                conStoreErrors.closeConnection();
                labelErrorDbConnected.Text = "Closed connection";
                labelErrorDbConnected.ForeColor = Color.Gray;
                labelErrorDbConnected.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while trying to close the connection to the DB for storing errors: " + ex.Message);
            }
        }

        //Button to execute the Query to store problematic requests in DB
        private void btnExecStatementErrors_Click(object sender, EventArgs e)
        {
            try
            {
                if ((cBoxStoreProblematicRequests.Checked) && (cBoxStoreLastWaitingContributions.Checked))
                {
                    MessageBox.Show("Please select only one and store one after another.");
                    return;
                }

                if (conStoreErrors.pgIsOpen || conStoreErrors.sqlServerIsOpen || dbConnection.pgIsOpen || dbConnection.sqlServerIsOpen)
                {
                    if (cBoxStoreProblematicRequests.Checked)
                    {
                        foreach (KeyValuePair<string, string> tmp in problematicRequests)
                        {
                            if (cBoxErrorLog.Checked == true) 
                            {
                                File.AppendAllText(Directory.GetCurrentDirectory() + "/Log/Log_bad_comp_" + tmp.Key.GetHashCode() + "_" + Thread.CurrentThread.ManagedThreadId + ".txt", "composition: " + tmp.Key + Environment.NewLine + "value: " + tmp.Value);
                            }
                            else
                            {
                                if (conStoreErrors.pgIsOpen)
                                {
                                    PgSqlCommand sql = new PgSqlCommand(txtBoxErrorSqlStatement.Text, conStoreErrors.pgConnection);

                                    sql.Parameters.Add("@composition", PgSqlType.Text);
                                    sql.Parameters.Add("@error_desc", PgSqlType.VarChar);
                                    sql.Parameters["@composition"].Value = tmp.Key;
                                    sql.Parameters["@error_desc"].Value = tmp.Value;
                                    sql.ExecuteNonQuery();
                                }
                                else if (conStoreErrors.sqlServerIsOpen)
                                {
                                    SqlCommand sql = new SqlCommand(txtBoxErrorSqlStatement.Text, conStoreErrors.sqlServerConnection);
                                    sql.Parameters.AddWithValue("@composition", PgSqlType.Text);
                                    sql.Parameters.AddWithValue("@error_desc", PgSqlType.VarChar);
                                    sql.Parameters["@composition"].Value = tmp.Key;
                                    sql.Parameters["@error_desc"].Value = tmp.Value;
                                    sql.ExecuteNonQuery();
                                }
                                else if (dbConnection.pgIsOpen)
                                {
                                    PgSqlCommand sql = new PgSqlCommand(txtBoxErrorSqlStatement.Text, dbConnection.pgConnection);

                                    sql.Parameters.Add("@composition", PgSqlType.Text);
                                    sql.Parameters.Add("@error_desc", PgSqlType.VarChar);
                                    sql.Parameters["@composition"].Value = tmp.Key;
                                    sql.Parameters["@error_desc"].Value = tmp.Value;
                                    sql.ExecuteNonQuery();
                                }
                                else if (dbConnection.sqlServerIsOpen)
                                {
                                    SqlCommand sql = new SqlCommand(txtBoxErrorSqlStatement.Text, conStoreErrors.sqlServerConnection);
                                    sql.Parameters.AddWithValue("@composition", PgSqlType.Text);
                                    sql.Parameters.AddWithValue("@error_desc", PgSqlType.VarChar);
                                    sql.Parameters["@composition"].Value = tmp.Key;
                                    sql.Parameters["@error_desc"].Value = tmp.Value;
                                    sql.ExecuteNonQuery();
                                }
                            }
                        }
                        return;
                    }
                    
                    if (cBoxStoreLastWaitingContributions.Checked)
                    {
                        foreach (KeyValuePair<Contribution, string> tmp in notSentContributions)
                        {
                            if (conStoreErrors.pgIsOpen)
                            {
                                PgSqlCommand sql = new PgSqlCommand(txtBoxErrorSqlStatement.Text, conStoreErrors.pgConnection);

                                sql.Parameters.Add("@composition", PgSqlType.Text);
                                sql.Parameters.Add("@error_desc", PgSqlType.VarChar);
                                sql.Parameters["@composition"].Value = tmp.Key;
                                sql.Parameters["@error_desc"].Value = tmp.Value;
                                sql.ExecuteNonQuery();
                                MessageBox.Show("Stored!", "Not sent Contributions");
                            }
                            else if (conStoreErrors.sqlServerIsOpen)
                            {
                                SqlCommand sql = new SqlCommand(txtBoxErrorSqlStatement.Text, conStoreErrors.sqlServerConnection);
                                sql.Parameters.AddWithValue("@composition", PgSqlType.Text);
                                sql.Parameters.AddWithValue("@error_desc", PgSqlType.VarChar);
                                sql.Parameters["@composition"].Value = tmp.Key;
                                sql.Parameters["@error_desc"].Value = tmp.Value;
                                sql.ExecuteNonQuery();
                                MessageBox.Show("Stored!", "Not sent Contributions");
                            }
                            else if (dbConnection.pgIsOpen)
                            {
                                PgSqlCommand sql = new PgSqlCommand(txtBoxErrorSqlStatement.Text, dbConnection.pgConnection);

                                sql.Parameters.Add("@composition", PgSqlType.Text);
                                sql.Parameters.Add("@error_desc", PgSqlType.VarChar);
                                sql.Parameters["@composition"].Value = tmp.Key;
                                sql.Parameters["@error_desc"].Value = tmp.Value;
                                sql.ExecuteNonQuery();
                                MessageBox.Show("Stored!", "Not sent Contributions");
                            }
                            else if (dbConnection.sqlServerIsOpen)
                            {
                                SqlCommand sql = new SqlCommand(txtBoxErrorSqlStatement.Text, conStoreErrors.sqlServerConnection);
                                sql.Parameters.AddWithValue("@composition", PgSqlType.Text);
                                sql.Parameters.AddWithValue("@error_desc", PgSqlType.VarChar);
                                sql.Parameters["@composition"].Value = tmp.Key;
                                sql.Parameters["@error_desc"].Value = tmp.Value;
                                sql.ExecuteNonQuery();
                                MessageBox.Show("Stored!", "Not sent Contributions");
                            }

                        }
                        return;
                    }

                }
                else
                {
                    MessageBox.Show("No connection to a DB while trying to store Problematic Requests");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while trying to save Problematic Requests: " + ex.Message, "Store Problematic Requests");
            }

        }
        //Click Events for "Help"-Labels
        private void labelErrorTableInfo_Click(object sender, EventArgs e)
        {
            loggingBox helpBox = new loggingBox("You only need to connect to another DB if you're either not already connected to one or this db differs from the other one. Select your DBMS."
                + Environment.NewLine
                + Environment.NewLine
                + "Example ConString for Postgres: " +
                "\"Host=134.169.38.58;Port=5432;Database=postgres;User ID=<user>;Password=<pwd>;\""
                + Environment.NewLine
                + Environment.NewLine
                + "Example ConString for MS SQL Server: " +
                "\"Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password = myPassword;\"");
            helpBox.Show();
        }

        private void labelHelpDbCon_Click(object sender, EventArgs e)
        {
            loggingBox helpBox = new loggingBox("Select your DBMS. After connecting, you can execute a SQL query to receive data."
                + Environment.NewLine
                + Environment.NewLine
                + "Example ConString for Postgres: "
                + "\"Host=134.169.38.58;Port=5432;Database=postgres;User ID=<user>;Password=<pwd>;\""
                + Environment.NewLine
                + Environment.NewLine
                + "Example ConString for MS SQL Server: "
                + "\"Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password = myPassword;\""
                + Environment.NewLine
                + Environment.NewLine
                + "ConString for flat files (e.g. Excel) is the filepath");
            helpBox.Show();
        }
        
        private void labelSaveErrorTablesHelp_Click(object sender, EventArgs e)
        {
            loggingBox helpBox = new loggingBox("If there is something to store after trying to transmit contributions, there will appear checkBoxes on the right.");
            helpBox.Show();
        }
        #endregion


        #region Buttons and change events for think!-platform connection

        // Add Think Addresses to list of ComboBox "Think!Address"
        private void btnAddThinkAddress_Click(object sender, EventArgs e)
        {
            saveNewComboBoxItems(cmbBoxThinkAddress, Settings.Default.thinkAddress, txtBoxThinkAddress.Text);
        }

        // Remove think adresses to list of ComboBox"Think!Address"
        private void btnRemoveThinkAddress_Click(object sender, EventArgs e)
        {
            removeComboBoxItems(cmbBoxThinkAddress, Settings.Default.thinkAddress, cmbBoxThinkAddress.SelectedItem);
        }

        //Add and Remove Template IDs
        private void btnAddTemplateId_Click(object sender, EventArgs e)
        {
            saveNewComboBoxItems(cmbBoxTemplateId, Settings.Default.thinkTemplateId, txtBoxTemplateId.Text);
        }
        private void btnRemoveTemplateId_Click(object sender, EventArgs e)
        {
            removeComboBoxItems(cmbBoxTemplateId, Settings.Default.thinkTemplateId, cmbBoxTemplateId.SelectedItem);
        }

        //Selection Change of Think!Address ComboBox
        private void cmbBoxThinkAddress_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbBoxThinkAddress.SelectedItem != null)
            {
                thinkParams["thinkAddress"] = cmbBoxThinkAddress.SelectedItem.ToString();
            }
            else { thinkParams["thinkAddress"] = ""; }
        }

        //Selection Change of TemplateID ComboBox
        private void cmbBoxTemplateId_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbBoxTemplateId.SelectedItem != null)
            {
                thinkParams["thinkTemplateId"] = cmbBoxTemplateId.SelectedItem.ToString();
            }
            else { thinkParams["thinkTemplateId"] = ""; }
        }
        #endregion

        // Button to transmit according to generated instructions and so on
        private void btnTransmitContributions_Click(object sender, EventArgs e)
        {
            try
            {
                #region check if needed input is as expected
                if (txtBoxNamespaceTag.Text == null)
                {
                    MessageBox.Show("Namespace for subjectIDs without ehrID has to be set.");
                    return;
                }

                //check if there is data to transmit
                if (dbConnection.dt.Rows.Count == 0)
                {
                    MessageBox.Show("PLEASE VERIFY: record set for storage was empty.");
                    return;
                }
                #endregion

                #region initialize everything for transmission
                //Show a logging form
                loggingBox logEvents = new loggingBox(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + " Transmit contributions started..." + Environment.NewLine);
                logEvents.Show(); //Show the log

                //clear cache of not sended contributions and problematic requests
                problematicRequests = new ConcurrentDictionary<string, string>();
                notSentContributions = new ConcurrentDictionary<Contribution, string>();

                //have to be a global variables, so they will not be destroyed when method is done but requests are pending
                responseHandle = new ServerResponseHandling(int.Parse(tBoxNrRequestStats.Text));
                repoHandle = new RepoHandle(cmbBoxThinkAddress.SelectedItem.ToString(), txtBoxThinkUser.Text, txtBoxThinkPwd.Text, logEvents, responseHandle);
                responseHandle.repoHandle = repoHandle;
                
                contributionsHandler = new ContributionsHandler(repoHandle);
                responseHandle.contributionsHandler = contributionsHandler;

                contributionsHandler.namespaceTag = txtBoxNamespaceTag.Text.ToString();
                #endregion

                //try to retrieve existing ehrIds in namespace with one query (to avoid the two queries for each single existing ehrId)
                string aql = "SELECT e/ehr_id/value AS ehrId, e/ehr_status/subject/external_ref/id/value AS subjectId FROM EHR e " +
                                "WHERE e/ehr_status/subject/external_ref/namespace = '"+ txtBoxNamespaceTag.Text +"'";
                dynamic res = JsonConvert.DeserializeObject<ExpandoObject>(repoHandle.httpRequest("query?aql=" + aql, false, null, null), new ExpandoObjectConverter());
                if (res != null)
                {
                    foreach (dynamic row in res.resultSet)
                    {
                        contributionsHandler.subjectIdsToEhrIds[row.subjectId] = row.ehrId;
                    }
                }
                //create contributions from data
                createContributions(contributionsHandler, dbConnection.dt);
                logEvents.inputText = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") +
                    " All relational data converted to compositions (async requests pending)" + Environment.NewLine + 
                    "   - Dataset rows: " + dbConnection.dt.Rows.Count + Environment.NewLine +
                    "   - Created contributions: " + contributionsHandler.contributionsCounter +
                    " (" + +contributionsHandler.entryCounter + " entries)"  + Environment.NewLine;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Severe error while trying to transmit data: " + ex.Message);
            }
        }

        //where relational data is converted to contributions
        private void createContributions(ContributionsHandler contributionsHandler, DataTable data)
        {
            try
            {
                contributionsHandler.skipInstructionsJSON = (!cBoxIncludeInstructionsJSON.Checked);

                //set handlers
                InstructionsHandler instructionsHandler = new InstructionsHandler(txtBoxInstructionsString.Text, contributionsHandler);
                
                //forward at codes to contributionsHandler .ContainsKey(key)
                if (instructionsHandler.columnInstructionsKeyToIndex.ContainsKey("NoCollumnJustAdditionalInformationAboutAtCodes"))
                {
                    contributionsHandler.setAtCodes(instructionsHandler.columnInstructions[instructionsHandler.columnInstructionsKeyToIndex["NoCollumnJustAdditionalInformationAboutAtCodes"]]);
                }
                if (instructionsHandler.columnInstructionsKeyToIndex.ContainsKey("NoCollumnJustADictionaryWithStringsToReplace"))
                {
                    contributionsHandler.setStringsToReplace(instructionsHandler.columnInstructions[instructionsHandler.columnInstructionsKeyToIndex["NoCollumnJustADictionaryWithStringsToReplace"]]);
                }

                #region process data to contributions - create body as JSON-Array of contributions - compositions are built in FLAT Format
                //API-details: www.ehrscape.com/api-explorer.html and www.ehrscape.com/reference.html#_composition
                foreach (DataRow row in data.Rows)
                {
                    foreach (DataColumn col in data.Columns)
                    {
                        instructionsHandler.runOn(row, col);
                    }
                }
                instructionsHandler.runOn(null, null);
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        //create new example contributions
        private void cBoxIncludeInstructionsJSON_CheckedChanged(object sender, EventArgs e)
        {
            btnShowExampleContribution_Click(null, null);
        }

        //logs summary information about problems in contributions the repository returned
        private void btnLogInvalidPaths_Click(object sender, EventArgs e)
        {
            if (responseHandle != null) responseHandle.logInvalidCompositionsPaths();
        }

        //logs open requests, waiting contributions etc.
        private void button1_Click_1(object sender, EventArgs e)
        {
            //string tmpStr = "";
            foreach (KeyValuePair<string, bool> tmp in repoHandle.openRequests)
            {
                problematicRequests.TryAdd(tmp.Key, "no response");
            }
            repoHandle.openRequests = new ConcurrentDictionary<string, bool>();

            foreach (KeyValuePair<string, string> tmp in responseHandle.tmpProblematicRequests)
            {
                problematicRequests.TryAdd(tmp.Key, tmp.Value);
            }

            if (!problematicRequests.IsEmpty)
            {
                MessageBox.Show(DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " ProblematicRequests not empty and can be stored");
                File.AppendAllText(Directory.GetCurrentDirectory() + "/Log/Log_ProblematicRequests.txt", JsonConvert.SerializeObject(problematicRequests, Formatting.Indented));
            }
            else
            {
                MessageBox.Show(DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " No problematic requests available to store" + Environment.NewLine);
            }

            foreach (KeyValuePair<Contribution, bool> tmp in contributionsHandler.waitingForCommit)
            {
                //store waiting contributions -> available to store in db
                notSentContributions.TryAdd(tmp.Key, "was waiting for commit but job stopped");
            }
            if (!notSentContributions.IsEmpty)
            {
                cBoxStoreLastWaitingContributions.Visible = true;
                MessageBox.Show((DateTime.Now.ToString("dd/MM/yyyy h:mm:ss tt") + " there are still waiting contributions, that were not send due to stopping this job" + Environment.NewLine));
                File.AppendAllText(Directory.GetCurrentDirectory() + "/Log/Log_WaitingContributions.txt", JsonConvert.SerializeObject(notSentContributions, Formatting.Indented));
            }
            contributionsHandler.waitingForCommit = new ConcurrentDictionary<Contribution, bool>();
        }

        private void btnShowExampleContribution_Click(object sender, EventArgs e)
        {
            try
            {
                //Textbox leeren
                txtBoxDisplayExCon.Text = "";
                txtBoxDisplayExCon.Refresh();

                //create contributions from data (limited set)
                ContributionsHandler tmpContributionsHandler = new ContributionsHandler(txtBoxDisplayExCon);
                tmpContributionsHandler.defaultEHRId = tBoxDummyEHRId.Text;
                createContributions(tmpContributionsHandler, dbConnection.previewDataTable(dbConnection.dt, int.Parse(txtBoxExampleConNum.Text)));

                if (txtBoxDisplayExCon.Text.IndexOf(":-1") > -1) {
                    MessageBox.Show("Example contributions contain index of -1. You might want to add a subtree here and there ");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        //Change Event: strings hin und her tauschen und Anzeigen, wenn man zwischen Mapping-Optionen und "Instructions & Contributions" wechselt
        private void tabControlEtlBuilder_SelectedIndexChanged(object sender, EventArgs e)
        {
            //always set new InstructionsString when changing tab
            string returnedInstructions = "";
            myBrowser.ExecuteScriptAsync("iVM.toJSON();");
            var task = myBrowser.EvaluateScriptAsync("(function() {return iVM.newInstructions;})();");

            var complete = task.ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    var response = t.Result;
                    if (response.Success && response.Result != null)
                    {
                        returnedInstructions = response.Result.ToString();
                    }
                }
            });
            complete.Wait();
            returnedInstructions = returnedInstructions.Replace("\\n", "");
            if (returnedInstructions != "")
            {
                txtBoxInstructionsString.Text = returnedInstructions;
            }

            if (tabControlEtlBuilder.SelectedIndex == 1) //1: Index von "Instructions & Contributions" - Beim Wechsel zu diesem Tab Live Mapping aktualisieren, sofern Instructions vorhanden
            {
                btnShowExampleContribution_Click(null, null);
            }

            if (tabControlEtlBuilder.SelectedIndex == 2) //2: Index von "Mapping Options"
            {
                //Checken ob alle nötigen Eingaben zum Repository vorhanden sind
                if(cmbBoxThinkAddress.SelectedItem == null || 
                    cmbBoxTemplateId.SelectedItem == null || 
                    string.IsNullOrWhiteSpace(txtBoxThinkUser.Text) || 
                    string.IsNullOrWhiteSpace(txtBoxThinkPwd.Text) ||
                    string.IsNullOrWhiteSpace(cmbBoxThinkAddress.SelectedItem.ToString())
                   
                    )
                {
                    MessageBox.Show("Enter/Select all of Think-Address, Template, UserID, Pwd!");
                    tabControlEtlBuilder.SelectedIndex = 1;
                    return;
                }
                //Prüfen ob Datensatz im Cache
                if (dbConnection.dt.Rows.Count == 0) { MessageBox.Show("Dataset is empty."); tabControlEtlBuilder.SelectedIndex = 0; return; }

                try
                {
                    if (txtBoxInstructionsString.Text == null) { txtBoxInstructionsString.Text = ""; }

                    RepoHandle repoHandle = new RepoHandle(cmbBoxThinkAddress.SelectedItem.ToString(), txtBoxThinkUser.Text, txtBoxThinkPwd.Text, null, null);//responseHande is just needed for async usage
                    
                    if (string.IsNullOrWhiteSpace(cmbBoxTemplateId.SelectedItem.ToString())) {
                        MessageBox.Show("Please select TemplateId.");
                        return;
                    }

                    //assemble infos for ETL-Builder-Tool and log them as JSON-String
                    EtlBuilderInfo etlBuilderInfo = new EtlBuilderInfo(dbConnection.previewDataTable(dbConnection.dt, 100), txtBoxInstructionsString.Text, cmbBoxTemplateId.SelectedItem.ToString(), repoHandle);

                    myBrowser.Reload(true);
                    Thread.Sleep(500);
                    myBrowser.ExecuteScriptAsync("var iVM = new InstructionsVM(); ko.applyBindings(iVM);");
                    Thread.Sleep(300);
                    myBrowser.ExecuteScriptAsync("iVM.addExampleData(" + etlBuilderInfo.exampleDataJSON + ");");
                    myBrowser.ExecuteScriptAsync("iVM.addInstructions("+ etlBuilderInfo.columnInstructionsJSON.Replace("\n", " ") + ");");
                    myBrowser.ExecuteScriptAsync("iVM.addPaths(" + etlBuilderInfo.templatePathesJSON.Replace("\n", " ") + ");");
                    myBrowser.ExecuteScriptAsync("iVM.addOPT(" + etlBuilderInfo.templateOPTJSON.Replace("\n", " ") + ");");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        #region loading and saving project/instructions to/from file

        //loads values for GUI-elements from JSON file (just the ones not depending on the data)
        private void button1_Click(object sender, EventArgs e)
        {
            string curDir = Directory.GetCurrentDirectory();
            openFileTxtInstructions.Title = "Open project File";
            openFileTxtInstructions.InitialDirectory = curDir;
            openFileTxtInstructions.FileName = "project.json";

            if (openFileTxtInstructions.ShowDialog() == DialogResult.OK)
            {
                load_project_file(openFileTxtInstructions.FileName, curDir);
            }
        }

        private void load_project_file(string filename, string curDir)
        {
            var converter = new ExpandoObjectConverter();
            StreamReader stream = new StreamReader(filename);
            dynamic project = JsonConvert.DeserializeObject<ExpandoObject>(stream.ReadToEnd(), converter);
            stream.Close();

            cBoxExcelfile.Checked = project.excel;
            cBoxPostgres.Checked = project.postgresDB;
            cBoxSqlServer.Checked = project.msSQL;
            txtBoxConnString.Text = project.connString;
            txtBoxSqlQuery.Text = project.sqlQuery;
            cBoxErrorLog.Checked = project.logErr;
            cBoxErrorDbPg.Checked = project.postgresErr;
            cBoxErrorDbSqlServer.Checked = project.mssqlErr;
            txtBoxErrorConnString.Text = project.connStrErr;
            txtBoxErrorSqlStatement.Text = project.sqlErr;
            try { cmbBoxThinkAddress.Text = project.thinkAddress; } catch (RuntimeBinderException) { cmbBoxThinkAddress.SelectedIndex = 0; }
            try { cmbBoxTemplateId.Text = project.templateId; } catch (RuntimeBinderException) { cmbBoxTemplateId.SelectedIndex = 0; }
            txtBoxThinkUser.Text = project.repoUser;
            try { txtBoxThinkPwd.Text = project.repoPwd; } catch (RuntimeBinderException) { } //option for manually set password in testsettings
            txtBoxNamespaceTag.Text = project.repoNamespace;
            txtBoxInstructionsString.Text = project.instructions;
            try { tBoxNrRequestStats.Text = project.nrRequestStats; } catch (RuntimeBinderException) { tBoxNrRequestStats.Text = "4"; }
            try { tBoxDummyEHRId.Text = project.dummyEHRId; } catch (RuntimeBinderException) { tBoxDummyEHRId.Text = "null"; }
            try { cBoxIncludeInstructionsJSON.Checked = project.includeInstructionsJSON; } catch (RuntimeBinderException) { cBoxIncludeInstructionsJSON.Checked = false; }
            cBoxStoreProblematicRequests.Checked = project.storeProblematic;
            cBoxStoreLastWaitingContributions.Checked = project.storeRemainingContributions;

            if (txtBoxThinkPwd.Text == "") //just a dev convenience thing, try to load pw from another file
            {
                try
                {
                    stream = new StreamReader(curDir + "\\project.json");
                    project = JsonConvert.DeserializeObject<ExpandoObject>(stream.ReadToEnd(), converter);
                    stream.Close();
                    txtBoxThinkPwd.Text = project.repoPwd;
                }
                catch
                {

                }
            }
        }

        //saves values for GUI-elements to JSON file (just the ones not depending on the data)
        private void btnSaveProject_Click(object sender, EventArgs e)
        {
            dynamic project = new {
                excel = cBoxExcelfile.Checked,
                postgresDB = cBoxPostgres.Checked,
                msSQL = cBoxSqlServer.Checked,
                connString = txtBoxConnString.Text,
                sqlQuery = txtBoxSqlQuery.Text,
                logErr = cBoxErrorLog.Checked,
                postgresErr = cBoxErrorDbPg.Checked,
                mssqlErr = cBoxErrorDbSqlServer.Checked,
                connStrErr= txtBoxErrorConnString.Text,
                sqlErr = txtBoxErrorSqlStatement.Text,
                thinkAddress = cmbBoxThinkAddress.Text,
                templateId = cmbBoxTemplateId.Text,
                repoUser = txtBoxThinkUser.Text,
                repoNamespace = txtBoxNamespaceTag.Text,
                instructions = txtBoxInstructionsString.Text,
                nrRequestStats = tBoxNrRequestStats.Text,
                storeProblematic = cBoxStoreProblematicRequests.Checked,
                dummyEHRId = tBoxDummyEHRId.Text,
                includeInstructionsJSON = cBoxIncludeInstructionsJSON.Checked,
                storeRemainingContributions = cBoxStoreLastWaitingContributions.Checked
            };

            string curDir = Directory.GetCurrentDirectory();
            saveInstructions.Title = "Save project";
            saveInstructions.InitialDirectory = Directory.GetCurrentDirectory();
            saveInstructions.FileName = "project.json";
            if (saveInstructions.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(saveInstructions.FileName);
                sw.Write(JsonConvert.SerializeObject(project, Formatting.Indented));
                sw.Close();
            }
        }

        //Button to load existing Instructions from .txt
        private void btnLoadInstructions_Click(object sender, EventArgs e)
        {
            string curDir = Directory.GetCurrentDirectory();
            openFileTxtInstructions.Title = "Open Text File";
            openFileTxtInstructions.InitialDirectory = Directory.GetCurrentDirectory();

            if (openFileTxtInstructions.ShowDialog() == DialogResult.OK)
            {
                StreamReader stream = new StreamReader(openFileTxtInstructions.FileName);
                txtBoxInstructionsString.Text = stream.ReadToEnd();
                stream.Close();
            }
        }
        //Button to save existing Instructions to txt
        private void btnSaveInstructions_Click(object sender, EventArgs e)
        {
            string curDir = Directory.GetCurrentDirectory();
            saveInstructions.Title = "Save Instructions";
            saveInstructions.InitialDirectory = Directory.GetCurrentDirectory();
            if (saveInstructions.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(saveInstructions.FileName);
                sw.Write(txtBoxInstructionsString.Text);
                sw.Close();
            }
        }

        #endregion

        #region Deleting EHRs and compositions
        private void btnAQLEHRs_Click(object sender, EventArgs e)
        {
            rTxtBoxDeleting.Text = "select e/ehr_id/value as ehrId from EHR e";
        }

        private void btnAQLForCompositions_Click(object sender, EventArgs e)
        {
            if ((cmbBoxThinkAddress.SelectedItem.ToString()).IndexOf("/ehrbase/")>-1)
            {
                rTxtBoxDeleting.Text = "select a/uid/value as compositionId, "+ Environment.NewLine +
                " e/ehr_id/value as ehrId" + Environment.NewLine +
                "from EHR e" + Environment.NewLine +
                "contains COMPOSITION a" + Environment.NewLine +
                "WHERE a/ archetype_details / template_id / value = '" + cmbBoxTemplateId.SelectedItem.ToString() + "'" + Environment.NewLine;
            }
            else
            {
                rTxtBoxDeleting.Text = "select a/uid/value as compositionId" + Environment.NewLine +
                "from EHR e" + Environment.NewLine +
                "contains COMPOSITION a" + Environment.NewLine +
                "WHERE a/ archetype_details / template_id / value = '" + cmbBoxTemplateId.SelectedItem.ToString() + "'" + Environment.NewLine;
            }
        }

        private void btnRetrieveIds_Click(object sender, EventArgs e)
        {
            try
            {
                repoHandle = new RepoHandle(cmbBoxThinkAddress.SelectedItem.ToString(), txtBoxThinkUser.Text, txtBoxThinkPwd.Text, null, null);
                dynamic res = JsonConvert.DeserializeObject<ExpandoObject>(repoHandle.httpRequest("query?aql=" + rTxtBoxDeleting.Text.Replace('\n', ' '), false, null, null), new ExpandoObjectConverter());
                rTxtBoxDeleting.Text = JsonConvert.SerializeObject(res.resultSet, Formatting.Indented);
                txtBoxDeleteAmount.Text = res.resultSet.Count.ToString();
                if (rTxtBoxDeleting.Text.IndexOf("ehrId") != -1)
                {
                    lblDeleteType.Text = "EHRs";
                }
                if (rTxtBoxDeleting.Text.IndexOf("compositionId") != -1)
                {
                    lblDeleteType.Text = "compositions";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("(Maybe there is just nothing to retrieve? Or did you forget to enter repository address, username, etc.?) Error on attempt to retrieve stuff using AQL: " + ex.Message);
            }
        }

        //for whatever reason i don't manage to parse the JSON without these classes...i don't care
        class compositionItem
        {
            public string compositionId { get; set; }
            public string ehrId { get; set; }
        }
        class ehrItem
        {
            public string ehrId { get; set; }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                repoHandle = new RepoHandle(cmbBoxThinkAddress.SelectedItem.ToString(), txtBoxThinkUser.Text, txtBoxThinkPwd.Text, null, null);
                loggingBox logEvents = new loggingBox(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + " Deleting " + lblDeleteType.Text + " started..." + Environment.NewLine);
                logEvents.Show(); //Show the log
                repoHandle.log = logEvents;
                //depending on type of deletion parse JSON and iterate through list sending DELETE-request for each element
                if (lblDeleteType.Text == "EHRs")
                {
                    List<ehrItem> res = JsonConvert.DeserializeObject<List<ehrItem>>(rTxtBoxDeleting.Text);
                    foreach (ehrItem entry in res)
                    {
                        repoHandle.httpRequest("ehrs/" + entry.ehrId, false, "", "DELETE-ADMIN");
                    }
                }
                else
                {
                    List<compositionItem> res = JsonConvert.DeserializeObject<List<compositionItem>>(rTxtBoxDeleting.Text);
                    foreach (compositionItem entry in res)
                    {
                        if (repoHandle.isEhrbase)
                        {
                            repoHandle.httpRequest("compositions/" + entry.compositionId, false, entry.ehrId, "DELETE-ADMIN");
                        }
                        else
                        {
                            repoHandle.httpRequest("compositions/" + entry.compositionId, false, "", "DELETE-ADMIN");
                        }       
                    }
                }

                logEvents.inputText = "Done deleting.";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error on attempt to delete stuff: " + ex.Message);
            }
        }
        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            if (responseHandle != null)
            {
                responseHandle.maybeLog(true);
            }
        }
    }
}